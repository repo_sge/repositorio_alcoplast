﻿using System;
using SIGEFA.Formularios;
using SIGEFA.Reportes.clsReportes;

namespace SIGEFA.Reportes
{
	public partial class frmParamRankingxCliente : DevComponents.DotNetBar.Office2007Form
    {
        clsReporteRankingxCliente ds = new clsReporteRankingxCliente();

        public frmParamRankingxCliente()
        {
            InitializeComponent();
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            CRRankingCliente rpt = new CRRankingCliente();
            frmRptRankingCliente frm = new frmRptRankingCliente();
            rpt.SetDataSource(ds.Reporte_Ranking(dtpFecha1.Value, dtpFecha2.Value, frmLogin.iCodAlmacen).Tables[0]);
            frm.crvRankingCliente.ReportSource = rpt;
            frm.Show();
        }
    }
}
