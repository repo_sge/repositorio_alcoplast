﻿using System;
using System.Windows.Forms;

namespace SIGEFA.Reportes
{
	public partial class frmTransferenciaDirecta : Form
    {
        public frmTransferenciaDirecta()
        {
            InitializeComponent();
        }

        private void frmTransferenciaDirecta_Load(object sender, EventArgs e)
        {

            this.crvTransferenciaPendiente.RefreshReport();
        }
    }
}
