﻿using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using SIGEFA.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.InterMySql
{
    class MysqlParametro : IParametro
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        public string obtenerParametroPorNombre(string nombreParametro)
        {
            String valor = "";
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("ObtenerParametroPorNombre", con.conector);
                cmd.Parameters.AddWithValue("nombre_parametro", nombreParametro);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        valor = dr.GetString(0);
                    }
                }
                return valor;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
