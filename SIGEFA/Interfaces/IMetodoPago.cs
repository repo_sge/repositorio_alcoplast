﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface IMetodoPago
    {
        Boolean Insert(clsMetodoPago NuevoMetodoPago);
        Boolean Update(clsMetodoPago MetodoPago);
        Boolean Delete(Int32 Codigo);

        clsMetodoPago CargaMetodoPago(Int32 Codigo);
        clsMetodoPago BuscaMetodoPago(String Codigo);
        DataTable ListaMetodoPagos();
        DataTable CargaMetodoPagos();
    }
}
