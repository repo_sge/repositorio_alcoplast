﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface ITipoPrecio
    {
      
       Boolean insert(clsTipoPrecios tp);
       Boolean Update(clsTipoPrecios tp);
       Boolean eliminar(Int32 codTipoPrecio);
     DataTable ListaPrecios();

    }
}
