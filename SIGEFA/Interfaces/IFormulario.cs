﻿using System.Data;

namespace SIGEFA.Interfaces
{
	interface IFormulario
    {
        DataTable MuestraFormularios();
    }
}
