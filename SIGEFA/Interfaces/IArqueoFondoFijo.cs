﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface IArqueoFondoFijo
    {
        Boolean Insert(clsArqueoFondoFijo NuevoArqueo);

        Boolean insertDetalle(clsDetalleArqueFondoFijo det);

        DataTable ListaDinero(Int32 tipo);

        Decimal TraeValor(Int32 codigo);
    }
}
