﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface ITipoDocumento
    {
        Boolean Insert(clsTipoDocumento NuevoTipoDocumento);
        Boolean Update(clsTipoDocumento TipoDocumento);
        Boolean Delete(Int32 Codigo);

        clsTipoDocumento CargaTipoDocumento(Int32 Codigo);
        clsTipoDocumento BuscaTipoDocumento(String Sigla);
        DataTable ListaTipoDocumentos();
        DataTable ListaTipoDocumentosDeCaja();
        DataTable CargaTipoDocumentos();
        DataTable ListaDocumentoNota();
		DataTable ListaTipoDocumentosElectronicos();
	}
}
