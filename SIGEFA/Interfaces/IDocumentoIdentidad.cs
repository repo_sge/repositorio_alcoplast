﻿using SIGEFA.Entidades;
using System;
using System.Data;

namespace SIGEFA.Interfaces
{
	interface IDocumentoIdentidad
	{
		/**
		 * codigoTipoDocumento: codigo de Documento de Facturacion (BOLETA O FACTURA)
		 */
		DataTable ListaDocumentoIdentidad(Int32 codigoTipoDocumento);
		clsDocumentoIdentidad MuestraDocumentoIdentidad(Int32 codigoDocumentoIdentidad);
		clsDocumentoIdentidad ObtenerDocumentoIdentidadDeVenta(Int32 codigoFacturaVenta);
	}
}
