﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface ITipoArticulo
    {
        Boolean Insert(clsTipoArticulo NuevaTipoArticulo);
        Boolean Update(clsTipoArticulo TipoArticulo);
        Boolean Delete(Int32 Codigo);

        clsTipoArticulo CargaTipoArticulo(Int32 Codigo);
        DataTable ListaTipoArticulos();
    }
}
