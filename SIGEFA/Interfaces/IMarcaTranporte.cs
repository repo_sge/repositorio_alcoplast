﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface IMarcaTransporte
    {
        Boolean Insert(clsMarcaTransporte NuevaMarcaTransporte);
        Boolean Update(clsMarcaTransporte MarcaTransporte);
        Boolean Delete(Int32 Codigo);

        clsMarcaTransporte CargaMarcaTransporte(Int32 Codigo);
        DataTable ListaMarcaTransportes();
    }
}
