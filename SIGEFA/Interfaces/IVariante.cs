﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface IVariante
    {
        Boolean Insert(clsVariante NuevoVariante);
        Boolean Update(clsVariante Variante);
        Boolean Delete(Int32 Codigo);

        clsVariante CargaVariante(Int32 Codigo);
        DataTable ListaVariantes(Int32 codCaracteristica);
    }
}
