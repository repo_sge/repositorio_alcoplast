﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface ITipoPagoCaja
    {
        Boolean Insert(clsTipoPagoCaja NuevoTPcaja);
        Boolean Update(clsTipoPagoCaja TPcaja);
        Boolean Delete(Int32 Codigo);
                
        DataTable ListaTipoPagoCaja();
    }
}
