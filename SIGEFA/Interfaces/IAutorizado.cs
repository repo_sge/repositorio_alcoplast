﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface IAutorizado
    {
        Boolean Insert(clsAutorizado NuevoAutorizado);
        Boolean Update(clsAutorizado Autorizado);
        Boolean Delete(Int32 Codigo);

        clsAutorizado CargaAutorizado(Int32 Codigo);
        DataTable ListaAutorizados();
    }
}
