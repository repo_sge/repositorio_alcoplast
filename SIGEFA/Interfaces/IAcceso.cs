﻿using System;
using System.Collections.Generic;
using SIGEFA.Entidades;


namespace SIGEFA.Interfaces
{
	interface IAcceso
    {
        Boolean Insert(clsAccesos NuevoAcceso);
        Boolean LimpiarAccesos(Int32 CodUsuario, Int32 CodAlmacen);
        List<Int32> MuestraAccesos(Int32 CodUsuario, Int32 CodAlmacen);
        Boolean InsertAccesoEmp(Int32 CodUsuario,Int32 CodEmpresa, Int32 codUser);
    }
}
