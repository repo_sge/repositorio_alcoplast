﻿using SIGEFA.Entidades;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System;
using System.Windows.Forms;

namespace SIGEFA.Administradores
{
	class clsAdmNotaIngresoIgv
	{
		INotaIngresoIgv MNotaIngresoIgv = new MysqlNotaIngresoIgv();

		public Boolean insert(clsNotaIngresoIgv notaIngresoIgv)
		{
			try
			{
				return MNotaIngresoIgv.insert(notaIngresoIgv);
			}
			catch (Exception ex)
			{
				if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: Documento Duplicado", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return false;
			}
		}
	}
}
