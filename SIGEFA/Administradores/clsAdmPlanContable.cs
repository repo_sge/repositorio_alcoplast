﻿using System;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System.Data;
using System.Windows.Forms;

namespace SIGEFA.Administradores
{
	class clsAdmPlanContable
    {

        IPlanContable Mban = new MysqlPlanContable();

        public DataTable ListaPlanContableArbol()
        {
            try
            {
                return Mban.ListaPlanContableArbol();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }
    }
}
