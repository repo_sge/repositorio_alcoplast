﻿using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGEFA.Administradores
{
    class clsAdmParametro
    {
        IParametro MParametro = new MysqlParametro();

        public String ObtenerParametroPorNombre(String nombreParametro)
        {
            try
            {
                return MParametro.obtenerParametroPorNombre(nombreParametro);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message,
                                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return "";
            }
        }

    }
}
