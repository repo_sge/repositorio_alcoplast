﻿using System;

namespace SIGEFA.Entidades
{
	public class clsNotaIngresoIgv
	{
		private Int32 iCodNotaIngresoIgv;
		private Int32 iCodNotaIngreso;
		private Boolean bIncluyeIgv;

		public Int32 CodNotaIngresoIgv
		{
			get { return iCodNotaIngresoIgv; }
			set { iCodNotaIngresoIgv = value; }
		}

		public Int32 CodNotaIngreso
		{
			get { return iCodNotaIngreso; }
			set { iCodNotaIngreso = value; }
		}

		public Boolean IncluyeIgv
		{
			get { return bIncluyeIgv; }
			set { bIncluyeIgv = value; }
		}

	}
}
