﻿namespace SIGEFA.Formularios
{
    partial class frmGeneraVenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGeneraVenta));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvDetalle = new System.Windows.Forms.DataGridView();
            this.coddetalle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codproducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codunidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preciounit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montodscto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.igv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valoreal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioreal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioconigv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorpromedio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipoarticulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipoimpuesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codalmacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.almacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodigoEmpresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcion_venta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serie_motor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nrochasis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Marca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.color = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codFamilia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtDelOV = new System.Windows.Forms.TextBox();
            this.txtAddOV = new System.Windows.Forms.TextBox();
            this.btnDeleteItem = new System.Windows.Forms.Button();
            this.btnAddOV = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkFactura = new System.Windows.Forms.RadioButton();
            this.chkBoleta = new System.Windows.Forms.RadioButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cmbDocumentoIdentidad = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtNumDoc = new System.Windows.Forms.TextBox();
            this.rbtnPendiente = new System.Windows.Forms.RadioButton();
            this.label24 = new System.Windows.Forms.Label();
            this.cbovendedor = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtComentario = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txttasa = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.lbLineaCredito = new System.Windows.Forms.Label();
            this.txtLineaCredito = new System.Windows.Forms.TextBox();
            this.txtLineaCreditoUso = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtLineaCreditoDisponible = new System.Windows.Forms.TextBox();
            this.txtPDescuento = new System.Windows.Forms.TextBox();
            this.txtBruto = new System.Windows.Forms.TextBox();
            this.txtDscto = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.dtpFechaPago = new System.Windows.Forms.DateTimePicker();
            this.cmbFormaPago = new System.Windows.Forms.ComboBox();
            this.txtSunat_Capchat = new System.Windows.Forms.TextBox();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.pbCapchatS = new System.Windows.Forms.PictureBox();
            this.txtNombreCliente = new System.Windows.Forms.TextBox();
            this.txtCodCliente = new System.Windows.Forms.TextBox();
            this.chkVentaDsctoGlobal = new System.Windows.Forms.CheckBox();
            this.chkVentaGratuita = new System.Windows.Forms.CheckBox();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtinafectas = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtexoneradas = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtgratuitas = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtgravadas = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtPrecioVenta = new System.Windows.Forms.TextBox();
            this.txtIGV = new System.Windows.Forms.TextBox();
            this.txtValorVenta = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDocRef = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSerie = new System.Windows.Forms.TextBox();
            this.txtPedido = new System.Windows.Forms.TextBox();
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.superValidator1 = new DevComponents.DotNetBar.Validator.SuperValidator();
            this.customValidator6 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.customValidator2 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.customValidator1 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.customValidator4 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.txtCodigoBarras = new System.Windows.Forms.TextBox();
            this.lbNombreTransaccion = new System.Windows.Forms.Label();
            this.txtTransaccion = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.chkVentaEspecial = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnInicioOV = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnGuardaVenta = new System.Windows.Forms.Button();
            this.lblCantidadProductos = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalle)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCapchatS)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvDetalle);
            this.groupBox1.Location = new System.Drawing.Point(14, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(901, 336);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalle Venta";
            // 
            // dgvDetalle
            // 
            this.dgvDetalle.AllowUserToAddRows = false;
            this.dgvDetalle.AllowUserToDeleteRows = false;
            this.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.coddetalle,
            this.codproducto,
            this.referencia,
            this.descripcion,
            this.codunidad,
            this.unidad,
            this.cantidad,
            this.preciounit,
            this.importe,
            this.dscto1,
            this.dscto2,
            this.dscto3,
            this.montodscto,
            this.valorventa,
            this.igv,
            this.precioventa,
            this.valoreal,
            this.precioreal,
            this.precioconigv,
            this.valorpromedio,
            this.Tipoarticulo,
            this.Tipoimpuesto,
            this.codalmacen,
            this.almacen,
            this.TipoUnidad,
            this.CodigoEmpresa,
            this.descripcion_venta,
            this.serie_motor,
            this.nrochasis,
            this.modelo,
            this.Marca,
            this.color,
            this.codFamilia});
            this.dgvDetalle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDetalle.Location = new System.Drawing.Point(3, 18);
            this.dgvDetalle.Name = "dgvDetalle";
            this.dgvDetalle.ReadOnly = true;
            this.dgvDetalle.RowHeadersVisible = false;
            this.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetalle.Size = new System.Drawing.Size(895, 315);
            this.dgvDetalle.TabIndex = 2;
            this.dgvDetalle.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvDetalle_RowsAdded);
            this.dgvDetalle.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvDetalle_RowsRemoved);
            // 
            // coddetalle
            // 
            this.coddetalle.DataPropertyName = "codDetalle";
            this.coddetalle.HeaderText = "CodDetalle";
            this.coddetalle.Name = "coddetalle";
            this.coddetalle.ReadOnly = true;
            this.coddetalle.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.coddetalle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.coddetalle.Visible = false;
            this.coddetalle.Width = 80;
            // 
            // codproducto
            // 
            this.codproducto.DataPropertyName = "codProducto";
            this.codproducto.HeaderText = "Código Producto_";
            this.codproducto.Name = "codproducto";
            this.codproducto.ReadOnly = true;
            this.codproducto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.codproducto.Visible = false;
            this.codproducto.Width = 70;
            // 
            // referencia
            // 
            this.referencia.DataPropertyName = "referencia";
            this.referencia.HeaderText = "Código Producto";
            this.referencia.Name = "referencia";
            this.referencia.ReadOnly = true;
            this.referencia.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.referencia.Width = 70;
            // 
            // descripcion
            // 
            this.descripcion.DataPropertyName = "producto";
            this.descripcion.HeaderText = "Descripcion";
            this.descripcion.Name = "descripcion";
            this.descripcion.ReadOnly = true;
            this.descripcion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.descripcion.Width = 250;
            // 
            // codunidad
            // 
            this.codunidad.DataPropertyName = "codUnidadMedida";
            this.codunidad.HeaderText = "Cod. Unidad";
            this.codunidad.Name = "codunidad";
            this.codunidad.ReadOnly = true;
            this.codunidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.codunidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.codunidad.Visible = false;
            // 
            // unidad
            // 
            this.unidad.DataPropertyName = "unidad";
            this.unidad.HeaderText = "Unidad";
            this.unidad.Name = "unidad";
            this.unidad.ReadOnly = true;
            this.unidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.unidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.unidad.Width = 80;
            // 
            // cantidad
            // 
            this.cantidad.DataPropertyName = "cantidad";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.cantidad.DefaultCellStyle = dataGridViewCellStyle1;
            this.cantidad.HeaderText = "Cantidad";
            this.cantidad.Name = "cantidad";
            this.cantidad.ReadOnly = true;
            this.cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cantidad.Width = 80;
            // 
            // preciounit
            // 
            this.preciounit.DataPropertyName = "preciounitario";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.preciounit.DefaultCellStyle = dataGridViewCellStyle2;
            this.preciounit.HeaderText = "P. Unit.";
            this.preciounit.Name = "preciounit";
            this.preciounit.ReadOnly = true;
            this.preciounit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.preciounit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.preciounit.Width = 80;
            // 
            // importe
            // 
            this.importe.DataPropertyName = "subtotal";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.importe.DefaultCellStyle = dataGridViewCellStyle3;
            this.importe.HeaderText = "Importe";
            this.importe.Name = "importe";
            this.importe.ReadOnly = true;
            this.importe.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.importe.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.importe.Visible = false;
            // 
            // dscto1
            // 
            this.dscto1.DataPropertyName = "descuento1";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.dscto1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dscto1.HeaderText = "% Dscto1";
            this.dscto1.Name = "dscto1";
            this.dscto1.ReadOnly = true;
            this.dscto1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto1.Visible = false;
            // 
            // dscto2
            // 
            this.dscto2.DataPropertyName = "descuento2";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            this.dscto2.DefaultCellStyle = dataGridViewCellStyle5;
            this.dscto2.HeaderText = "% Dscto2";
            this.dscto2.Name = "dscto2";
            this.dscto2.ReadOnly = true;
            this.dscto2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto2.Visible = false;
            // 
            // dscto3
            // 
            this.dscto3.DataPropertyName = "descuento3";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            this.dscto3.DefaultCellStyle = dataGridViewCellStyle6;
            this.dscto3.HeaderText = "% Dscto3";
            this.dscto3.Name = "dscto3";
            this.dscto3.ReadOnly = true;
            this.dscto3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto3.Visible = false;
            // 
            // montodscto
            // 
            this.montodscto.DataPropertyName = "montodscto";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = null;
            this.montodscto.DefaultCellStyle = dataGridViewCellStyle7;
            this.montodscto.HeaderText = "Monto Dscto";
            this.montodscto.Name = "montodscto";
            this.montodscto.ReadOnly = true;
            this.montodscto.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.montodscto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.montodscto.Visible = false;
            // 
            // valorventa
            // 
            this.valorventa.DataPropertyName = "valorventa";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            dataGridViewCellStyle8.NullValue = null;
            this.valorventa.DefaultCellStyle = dataGridViewCellStyle8;
            this.valorventa.HeaderText = "V. Venta";
            this.valorventa.Name = "valorventa";
            this.valorventa.ReadOnly = true;
            this.valorventa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valorventa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.valorventa.Visible = false;
            // 
            // igv
            // 
            this.igv.DataPropertyName = "igv";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N2";
            this.igv.DefaultCellStyle = dataGridViewCellStyle9;
            this.igv.HeaderText = "IGV";
            this.igv.Name = "igv";
            this.igv.ReadOnly = true;
            this.igv.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.igv.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.igv.Visible = false;
            // 
            // precioventa
            // 
            this.precioventa.DataPropertyName = "importe";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N2";
            this.precioventa.DefaultCellStyle = dataGridViewCellStyle10;
            this.precioventa.HeaderText = "P. Venta";
            this.precioventa.Name = "precioventa";
            this.precioventa.ReadOnly = true;
            this.precioventa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.precioventa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // valoreal
            // 
            this.valoreal.DataPropertyName = "valoreal";
            dataGridViewCellStyle11.Format = "N2";
            dataGridViewCellStyle11.NullValue = null;
            this.valoreal.DefaultCellStyle = dataGridViewCellStyle11;
            this.valoreal.HeaderText = "V. real";
            this.valoreal.Name = "valoreal";
            this.valoreal.ReadOnly = true;
            this.valoreal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valoreal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.valoreal.Visible = false;
            // 
            // precioreal
            // 
            this.precioreal.DataPropertyName = "precioreal";
            dataGridViewCellStyle12.Format = "N2";
            dataGridViewCellStyle12.NullValue = null;
            this.precioreal.DefaultCellStyle = dataGridViewCellStyle12;
            this.precioreal.HeaderText = "P. real";
            this.precioreal.Name = "precioreal";
            this.precioreal.ReadOnly = true;
            this.precioreal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.precioreal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.precioreal.Visible = false;
            // 
            // precioconigv
            // 
            this.precioconigv.DataPropertyName = "precioigv";
            this.precioconigv.HeaderText = "precioconigv";
            this.precioconigv.Name = "precioconigv";
            this.precioconigv.ReadOnly = true;
            this.precioconigv.Visible = false;
            // 
            // valorpromedio
            // 
            this.valorpromedio.DataPropertyName = "valorpromedio";
            this.valorpromedio.HeaderText = "valorpromedio";
            this.valorpromedio.Name = "valorpromedio";
            this.valorpromedio.ReadOnly = true;
            this.valorpromedio.Visible = false;
            // 
            // Tipoarticulo
            // 
            this.Tipoarticulo.DataPropertyName = "Tipoarticulo";
            this.Tipoarticulo.HeaderText = "Tipoarticulo";
            this.Tipoarticulo.Name = "Tipoarticulo";
            this.Tipoarticulo.ReadOnly = true;
            this.Tipoarticulo.Visible = false;
            // 
            // Tipoimpuesto
            // 
            this.Tipoimpuesto.DataPropertyName = "Tipoimpuesto";
            this.Tipoimpuesto.HeaderText = "Tipoimpuesto";
            this.Tipoimpuesto.Name = "Tipoimpuesto";
            this.Tipoimpuesto.ReadOnly = true;
            this.Tipoimpuesto.Visible = false;
            // 
            // codalmacen
            // 
            this.codalmacen.DataPropertyName = "codalmacen";
            this.codalmacen.HeaderText = "codalmacen";
            this.codalmacen.Name = "codalmacen";
            this.codalmacen.ReadOnly = true;
            this.codalmacen.Visible = false;
            // 
            // almacen
            // 
            this.almacen.DataPropertyName = "almacen";
            this.almacen.HeaderText = "Almacen";
            this.almacen.Name = "almacen";
            this.almacen.ReadOnly = true;
            this.almacen.Visible = false;
            // 
            // TipoUnidad
            // 
            this.TipoUnidad.DataPropertyName = "TipoUnidad";
            this.TipoUnidad.HeaderText = "T. Und";
            this.TipoUnidad.Name = "TipoUnidad";
            this.TipoUnidad.ReadOnly = true;
            this.TipoUnidad.Visible = false;
            // 
            // CodigoEmpresa
            // 
            this.CodigoEmpresa.DataPropertyName = "CodigoEmpresa";
            this.CodigoEmpresa.HeaderText = "CodigoEmpresa";
            this.CodigoEmpresa.Name = "CodigoEmpresa";
            this.CodigoEmpresa.ReadOnly = true;
            this.CodigoEmpresa.Visible = false;
            // 
            // descripcion_venta
            // 
            this.descripcion_venta.DataPropertyName = "descripcion_venta";
            this.descripcion_venta.HeaderText = "descripcion_venta";
            this.descripcion_venta.Name = "descripcion_venta";
            this.descripcion_venta.ReadOnly = true;
            this.descripcion_venta.Visible = false;
            // 
            // serie_motor
            // 
            this.serie_motor.DataPropertyName = "serie_motor";
            this.serie_motor.HeaderText = "Serie de Motor";
            this.serie_motor.Name = "serie_motor";
            this.serie_motor.ReadOnly = true;
            // 
            // nrochasis
            // 
            this.nrochasis.DataPropertyName = "nrochasis";
            this.nrochasis.HeaderText = "Nº de Chasis";
            this.nrochasis.Name = "nrochasis";
            this.nrochasis.ReadOnly = true;
            // 
            // modelo
            // 
            this.modelo.DataPropertyName = "modelo";
            this.modelo.HeaderText = "Modelo";
            this.modelo.Name = "modelo";
            this.modelo.ReadOnly = true;
            // 
            // Marca
            // 
            this.Marca.DataPropertyName = "marca";
            this.Marca.HeaderText = "Marca";
            this.Marca.Name = "Marca";
            this.Marca.ReadOnly = true;
            // 
            // color
            // 
            this.color.DataPropertyName = "color";
            this.color.HeaderText = "Color";
            this.color.Name = "color";
            this.color.ReadOnly = true;
            // 
            // codFamilia
            // 
            this.codFamilia.DataPropertyName = "codFamilia";
            this.codFamilia.HeaderText = "codFamilia";
            this.codFamilia.Name = "codFamilia";
            this.codFamilia.ReadOnly = true;
            this.codFamilia.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtDelOV);
            this.panel1.Controls.Add(this.txtAddOV);
            this.panel1.Controls.Add(this.btnDeleteItem);
            this.panel1.Controls.Add(this.btnAddOV);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(921, 75);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 186);
            this.panel1.TabIndex = 2;
            // 
            // txtDelOV
            // 
            this.txtDelOV.Location = new System.Drawing.Point(17, 41);
            this.txtDelOV.Name = "txtDelOV";
            this.txtDelOV.Size = new System.Drawing.Size(109, 22);
            this.txtDelOV.TabIndex = 131;
            this.txtDelOV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAddOV
            // 
            this.txtAddOV.Location = new System.Drawing.Point(17, 13);
            this.txtAddOV.Name = "txtAddOV";
            this.txtAddOV.Size = new System.Drawing.Size(109, 22);
            this.txtAddOV.TabIndex = 130;
            this.txtAddOV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAddOV.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAddOV_KeyPress);
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.Enabled = false;
            this.btnDeleteItem.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnDeleteItem.FlatAppearance.BorderSize = 2;
            this.btnDeleteItem.Location = new System.Drawing.Point(133, 41);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(94, 22);
            this.btnDeleteItem.TabIndex = 129;
            this.btnDeleteItem.Text = "F2 - DEL OV";
            this.btnDeleteItem.UseVisualStyleBackColor = true;
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click_1);
            // 
            // btnAddOV
            // 
            this.btnAddOV.Enabled = false;
            this.btnAddOV.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnAddOV.FlatAppearance.BorderSize = 2;
            this.btnAddOV.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnAddOV.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnAddOV.Location = new System.Drawing.Point(133, 13);
            this.btnAddOV.Name = "btnAddOV";
            this.btnAddOV.Size = new System.Drawing.Size(94, 22);
            this.btnAddOV.TabIndex = 128;
            this.btnAddOV.Text = "F3 - ADD OV";
            this.btnAddOV.UseVisualStyleBackColor = true;
            this.btnAddOV.Click += new System.EventHandler(this.btnAddOV_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkFactura);
            this.groupBox2.Controls.Add(this.chkBoleta);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(5, 142);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(238, 35);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // chkFactura
            // 
            this.chkFactura.AutoSize = true;
            this.chkFactura.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.chkFactura.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkFactura.Location = new System.Drawing.Point(130, 11);
            this.chkFactura.Name = "chkFactura";
            this.chkFactura.Size = new System.Drawing.Size(80, 18);
            this.chkFactura.TabIndex = 1;
            this.chkFactura.Text = "FACTURA";
            this.chkFactura.UseVisualStyleBackColor = true;
            this.chkFactura.Click += new System.EventHandler(this.chkFactura_Click);
            // 
            // chkBoleta
            // 
            this.chkBoleta.AutoSize = true;
            this.chkBoleta.Checked = true;
            this.chkBoleta.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.chkBoleta.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkBoleta.Location = new System.Drawing.Point(44, 11);
            this.chkBoleta.Name = "chkBoleta";
            this.chkBoleta.Size = new System.Drawing.Size(71, 18);
            this.chkBoleta.TabIndex = 0;
            this.chkBoleta.TabStop = true;
            this.chkBoleta.Text = "BOLETA";
            this.chkBoleta.UseVisualStyleBackColor = true;
            this.chkBoleta.Click += new System.EventHandler(this.chkBoleta_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Write Document.png");
            this.imageList1.Images.SetKeyName(1, "New Document.png");
            this.imageList1.Images.SetKeyName(2, "Remove Document.png");
            this.imageList1.Images.SetKeyName(3, "document-print.png");
            this.imageList1.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
            this.imageList1.Images.SetKeyName(5, "exit.png");
            this.imageList1.Images.SetKeyName(6, "boton-inicio.png");
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cmbDocumentoIdentidad);
            this.groupBox4.Controls.Add(this.txtNumDoc);
            this.groupBox4.Controls.Add(this.rbtnPendiente);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.cbovendedor);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.txtComentario);
            this.groupBox4.Controls.Add(this.groupBox3);
            this.groupBox4.Controls.Add(this.txtPDescuento);
            this.groupBox4.Controls.Add(this.txtBruto);
            this.groupBox4.Controls.Add(this.txtDscto);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.dtpFechaPago);
            this.groupBox4.Controls.Add(this.cmbFormaPago);
            this.groupBox4.Controls.Add(this.txtSunat_Capchat);
            this.groupBox4.Controls.Add(this.txtDireccion);
            this.groupBox4.Controls.Add(this.pbCapchatS);
            this.groupBox4.Controls.Add(this.txtNombreCliente);
            this.groupBox4.Controls.Add(this.txtCodCliente);
            this.groupBox4.Controls.Add(this.chkVentaDsctoGlobal);
            this.groupBox4.Controls.Add(this.chkVentaGratuita);
            this.groupBox4.Controls.Add(this.dtpFecha);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(17, 414);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(898, 183);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Datos del Cliente";
            // 
            // cmbDocumentoIdentidad
            // 
            this.cmbDocumentoIdentidad.DisplayMember = "Text";
            this.cmbDocumentoIdentidad.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbDocumentoIdentidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDocumentoIdentidad.FormattingEnabled = true;
            this.cmbDocumentoIdentidad.ItemHeight = 16;
            this.cmbDocumentoIdentidad.Location = new System.Drawing.Point(325, 21);
            this.cmbDocumentoIdentidad.Name = "cmbDocumentoIdentidad";
            this.cmbDocumentoIdentidad.Size = new System.Drawing.Size(240, 22);
            this.cmbDocumentoIdentidad.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbDocumentoIdentidad.TabIndex = 137;
            // 
            // txtNumDoc
            // 
            this.txtNumDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumDoc.Enabled = false;
            this.txtNumDoc.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumDoc.Location = new System.Drawing.Point(227, 21);
            this.txtNumDoc.Name = "txtNumDoc";
            this.txtNumDoc.ReadOnly = true;
            this.txtNumDoc.Size = new System.Drawing.Size(92, 22);
            this.txtNumDoc.TabIndex = 136;
            // 
            // rbtnPendiente
            // 
            this.rbtnPendiente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbtnPendiente.AutoSize = true;
            this.rbtnPendiente.ForeColor = System.Drawing.Color.OliveDrab;
            this.rbtnPendiente.Location = new System.Drawing.Point(325, 24);
            this.rbtnPendiente.Name = "rbtnPendiente";
            this.rbtnPendiente.Size = new System.Drawing.Size(148, 17);
            this.rbtnPendiente.TabIndex = 112;
            this.rbtnPendiente.Text = "Pendiente de Despacho";
            this.rbtnPendiente.UseVisualStyleBackColor = true;
            this.rbtnPendiente.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(14, 157);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(95, 15);
            this.label24.TabIndex = 135;
            this.label24.Text = "VENDEDOR       :";
            // 
            // cbovendedor
            // 
            this.cbovendedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbovendedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbovendedor.FormattingEnabled = true;
            this.cbovendedor.Items.AddRange(new object[] {
            "Sin Vendedor"});
            this.cbovendedor.Location = new System.Drawing.Point(115, 155);
            this.cbovendedor.Name = "cbovendedor";
            this.cbovendedor.Size = new System.Drawing.Size(279, 20);
            this.cbovendedor.TabIndex = 134;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(629, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 133;
            this.label14.Text = "Notas :";
            // 
            // txtComentario
            // 
            this.txtComentario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComentario.Location = new System.Drawing.Point(683, 15);
            this.txtComentario.Multiline = true;
            this.txtComentario.Name = "txtComentario";
            this.txtComentario.Size = new System.Drawing.Size(206, 41);
            this.txtComentario.TabIndex = 132;
            this.txtComentario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.groupBox3.Controls.Add(this.txttasa);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.lbLineaCredito);
            this.groupBox3.Controls.Add(this.txtLineaCredito);
            this.groupBox3.Controls.Add(this.txtLineaCreditoUso);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.txtLineaCreditoDisponible);
            this.groupBox3.Enabled = false;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(658, 62);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(232, 115);
            this.groupBox3.TabIndex = 131;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Condiciones de Crédito:";
            // 
            // txttasa
            // 
            this.txttasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttasa.Location = new System.Drawing.Point(126, 82);
            this.txttasa.Name = "txttasa";
            this.txttasa.ReadOnly = true;
            this.txttasa.Size = new System.Drawing.Size(92, 18);
            this.txttasa.TabIndex = 106;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(40, 85);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 12);
            this.label30.TabIndex = 105;
            this.label30.Text = "Tasa de Interés:";
            // 
            // lbLineaCredito
            // 
            this.lbLineaCredito.AutoSize = true;
            this.lbLineaCredito.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLineaCredito.Location = new System.Drawing.Point(13, 20);
            this.lbLineaCredito.Name = "lbLineaCredito";
            this.lbLineaCredito.Size = new System.Drawing.Size(94, 12);
            this.lbLineaCredito.TabIndex = 85;
            this.lbLineaCredito.Text = "Línea de Crédito (S/.):";
            // 
            // txtLineaCredito
            // 
            this.txtLineaCredito.Enabled = false;
            this.txtLineaCredito.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineaCredito.Location = new System.Drawing.Point(126, 17);
            this.txtLineaCredito.Name = "txtLineaCredito";
            this.txtLineaCredito.ReadOnly = true;
            this.txtLineaCredito.Size = new System.Drawing.Size(92, 18);
            this.txtLineaCredito.TabIndex = 84;
            // 
            // txtLineaCreditoUso
            // 
            this.txtLineaCreditoUso.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineaCreditoUso.Location = new System.Drawing.Point(126, 61);
            this.txtLineaCreditoUso.Name = "txtLineaCreditoUso";
            this.txtLineaCreditoUso.ReadOnly = true;
            this.txtLineaCreditoUso.Size = new System.Drawing.Size(92, 18);
            this.txtLineaCreditoUso.TabIndex = 98;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(12, 40);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 12);
            this.label23.TabIndex = 95;
            this.label23.Text = "Línea Disponible (S/.):";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(14, 64);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(93, 12);
            this.label25.TabIndex = 97;
            this.label25.Text = "Línea C. en Uso (S/.):";
            // 
            // txtLineaCreditoDisponible
            // 
            this.txtLineaCreditoDisponible.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineaCreditoDisponible.Location = new System.Drawing.Point(126, 40);
            this.txtLineaCreditoDisponible.Name = "txtLineaCreditoDisponible";
            this.txtLineaCreditoDisponible.ReadOnly = true;
            this.txtLineaCreditoDisponible.Size = new System.Drawing.Size(92, 18);
            this.txtLineaCreditoDisponible.TabIndex = 96;
            // 
            // txtPDescuento
            // 
            this.txtPDescuento.Location = new System.Drawing.Point(398, 155);
            this.txtPDescuento.Name = "txtPDescuento";
            this.txtPDescuento.Size = new System.Drawing.Size(88, 22);
            this.txtPDescuento.TabIndex = 126;
            this.txtPDescuento.Tag = "7";
            this.txtPDescuento.Visible = false;
            // 
            // txtBruto
            // 
            this.txtBruto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBruto.Location = new System.Drawing.Point(492, 116);
            this.txtBruto.Name = "txtBruto";
            this.txtBruto.ReadOnly = true;
            this.txtBruto.Size = new System.Drawing.Size(90, 22);
            this.txtBruto.TabIndex = 122;
            this.txtBruto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.superValidator1.SetValidator1(this.txtBruto, this.customValidator6);
            // 
            // txtDscto
            // 
            this.txtDscto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtDscto.Location = new System.Drawing.Point(492, 155);
            this.txtDscto.Name = "txtDscto";
            this.txtDscto.ReadOnly = true;
            this.txtDscto.Size = new System.Drawing.Size(87, 22);
            this.txtDscto.TabIndex = 123;
            this.txtDscto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(489, 139);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 125;
            this.label2.Text = "Descuento :";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(489, 100);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 124;
            this.label13.Text = "Bruto :";
            // 
            // dtpFechaPago
            // 
            this.dtpFechaPago.Enabled = false;
            this.dtpFechaPago.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaPago.Location = new System.Drawing.Point(300, 129);
            this.dtpFechaPago.Name = "dtpFechaPago";
            this.dtpFechaPago.Size = new System.Drawing.Size(94, 22);
            this.dtpFechaPago.TabIndex = 119;
            this.dtpFechaPago.Tag = "16";
            this.dtpFechaPago.Visible = false;
            // 
            // cmbFormaPago
            // 
            this.cmbFormaPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFormaPago.FormattingEnabled = true;
            this.cmbFormaPago.Location = new System.Drawing.Point(115, 129);
            this.cmbFormaPago.Name = "cmbFormaPago";
            this.cmbFormaPago.Size = new System.Drawing.Size(175, 20);
            this.cmbFormaPago.TabIndex = 118;
            this.cmbFormaPago.Tag = "16";
            this.cmbFormaPago.Visible = false;
            this.cmbFormaPago.SelectionChangeCommitted += new System.EventHandler(this.cmbFormaPago_SelectionChangeCommitted);
            // 
            // txtSunat_Capchat
            // 
            this.txtSunat_Capchat.Location = new System.Drawing.Point(300, 154);
            this.txtSunat_Capchat.Name = "txtSunat_Capchat";
            this.txtSunat_Capchat.Size = new System.Drawing.Size(84, 22);
            this.txtSunat_Capchat.TabIndex = 120;
            this.txtSunat_Capchat.Visible = false;
            // 
            // txtDireccion
            // 
            this.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDireccion.Enabled = false;
            this.txtDireccion.Location = new System.Drawing.Point(116, 75);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.ReadOnly = true;
            this.txtDireccion.Size = new System.Drawing.Size(370, 22);
            this.txtDireccion.TabIndex = 117;
            this.txtDireccion.Tag = "21";
            // 
            // pbCapchatS
            // 
            this.pbCapchatS.Location = new System.Drawing.Point(400, 106);
            this.pbCapchatS.Name = "pbCapchatS";
            this.pbCapchatS.Size = new System.Drawing.Size(86, 38);
            this.pbCapchatS.TabIndex = 121;
            this.pbCapchatS.TabStop = false;
            this.pbCapchatS.Visible = false;
            // 
            // txtNombreCliente
            // 
            this.txtNombreCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreCliente.Enabled = false;
            this.txtNombreCliente.Location = new System.Drawing.Point(115, 48);
            this.txtNombreCliente.Name = "txtNombreCliente";
            this.txtNombreCliente.Size = new System.Drawing.Size(371, 22);
            this.txtNombreCliente.TabIndex = 116;
            this.txtNombreCliente.Tag = "3";
            this.superValidator1.SetValidator1(this.txtNombreCliente, this.customValidator2);
            // 
            // txtCodCliente
            // 
            this.txtCodCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.txtCodCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodCliente.Location = new System.Drawing.Point(116, 21);
            this.txtCodCliente.Name = "txtCodCliente";
            this.txtCodCliente.Size = new System.Drawing.Size(105, 22);
            this.txtCodCliente.TabIndex = 115;
            this.txtCodCliente.Tag = "5";
            this.txtCodCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.superValidator1.SetValidator1(this.txtCodCliente, this.customValidator1);
            this.txtCodCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodCliente_KeyDown);
            this.txtCodCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodCliente_KeyPress);
            this.txtCodCliente.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCodCliente_KeyUp);
            // 
            // chkVentaDsctoGlobal
            // 
            this.chkVentaDsctoGlobal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkVentaDsctoGlobal.AutoSize = true;
            this.chkVentaDsctoGlobal.Location = new System.Drawing.Point(492, 77);
            this.chkVentaDsctoGlobal.Name = "chkVentaDsctoGlobal";
            this.chkVentaDsctoGlobal.Size = new System.Drawing.Size(104, 17);
            this.chkVentaDsctoGlobal.TabIndex = 114;
            this.chkVentaDsctoGlobal.Text = "Vta. Descuento";
            this.chkVentaDsctoGlobal.UseVisualStyleBackColor = true;
            this.chkVentaDsctoGlobal.Visible = false;
            // 
            // chkVentaGratuita
            // 
            this.chkVentaGratuita.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkVentaGratuita.AutoSize = true;
            this.chkVentaGratuita.Location = new System.Drawing.Point(492, 50);
            this.chkVentaGratuita.Name = "chkVentaGratuita";
            this.chkVentaGratuita.Size = new System.Drawing.Size(91, 17);
            this.chkVentaGratuita.TabIndex = 111;
            this.chkVentaGratuita.Text = "Vta. Gratuita";
            this.chkVentaGratuita.UseVisualStyleBackColor = true;
            this.chkVentaGratuita.Visible = false;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Location = new System.Drawing.Point(116, 103);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(278, 22);
            this.dtpFecha.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 15);
            this.label7.TabIndex = 4;
            this.label7.Text = "FORMA PAGO    :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(14, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 15);
            this.label6.TabIndex = 3;
            this.label6.Text = "FECHA DOC.      :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 15);
            this.label5.TabIndex = 2;
            this.label5.Text = "Nº DOCUM. (F1) :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "DIRECCION       :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "CLIENTE           :";
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "Write Document.png");
            this.imageList2.Images.SetKeyName(1, "New Document.png");
            this.imageList2.Images.SetKeyName(2, "Remove Document.png");
            this.imageList2.Images.SetKeyName(3, "document-print.png");
            this.imageList2.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
            this.imageList2.Images.SetKeyName(5, "exit.png");
            this.imageList2.Images.SetKeyName(6, "barcode-29485.jpg");
            this.imageList2.Images.SetKeyName(7, "images.png");
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtinafectas);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.txtexoneradas);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.txtgratuitas);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.txtgravadas);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.txtPrecioVenta);
            this.panel2.Controls.Add(this.txtIGV);
            this.panel2.Controls.Add(this.txtValorVenta);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Enabled = false;
            this.panel2.Location = new System.Drawing.Point(921, 267);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(250, 244);
            this.panel2.TabIndex = 8;
            // 
            // txtinafectas
            // 
            this.txtinafectas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtinafectas.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtinafectas.Location = new System.Drawing.Point(128, 74);
            this.txtinafectas.Name = "txtinafectas";
            this.txtinafectas.ReadOnly = true;
            this.txtinafectas.Size = new System.Drawing.Size(99, 25);
            this.txtinafectas.TabIndex = 46;
            this.txtinafectas.Tag = "7";
            this.txtinafectas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(50, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 17);
            this.label26.TabIndex = 44;
            this.label26.Tag = "7";
            this.label26.Text = "Inafectas :";
            // 
            // txtexoneradas
            // 
            this.txtexoneradas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtexoneradas.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexoneradas.Location = new System.Drawing.Point(128, 43);
            this.txtexoneradas.Name = "txtexoneradas";
            this.txtexoneradas.ReadOnly = true;
            this.txtexoneradas.Size = new System.Drawing.Size(99, 25);
            this.txtexoneradas.TabIndex = 45;
            this.txtexoneradas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(35, 46);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(86, 17);
            this.label32.TabIndex = 43;
            this.label32.Text = "Exoneradas :";
            // 
            // txtgratuitas
            // 
            this.txtgratuitas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtgratuitas.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgratuitas.Location = new System.Drawing.Point(128, 103);
            this.txtgratuitas.Name = "txtgratuitas";
            this.txtgratuitas.ReadOnly = true;
            this.txtgratuitas.Size = new System.Drawing.Size(99, 25);
            this.txtgratuitas.TabIndex = 42;
            this.txtgratuitas.Tag = "7";
            this.txtgratuitas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(50, 106);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 17);
            this.label19.TabIndex = 40;
            this.label19.Tag = "7";
            this.label19.Text = "Gratuitas :";
            // 
            // txtgravadas
            // 
            this.txtgravadas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtgravadas.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgravadas.Location = new System.Drawing.Point(128, 14);
            this.txtgravadas.Name = "txtgravadas";
            this.txtgravadas.ReadOnly = true;
            this.txtgravadas.Size = new System.Drawing.Size(99, 25);
            this.txtgravadas.TabIndex = 41;
            this.txtgravadas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(49, 17);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 17);
            this.label22.TabIndex = 39;
            this.label22.Text = "Gravadas :";
            // 
            // txtPrecioVenta
            // 
            this.txtPrecioVenta.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtPrecioVenta.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecioVenta.Location = new System.Drawing.Point(116, 209);
            this.txtPrecioVenta.Name = "txtPrecioVenta";
            this.txtPrecioVenta.ReadOnly = true;
            this.txtPrecioVenta.Size = new System.Drawing.Size(112, 27);
            this.txtPrecioVenta.TabIndex = 8;
            this.txtPrecioVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtIGV
            // 
            this.txtIGV.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtIGV.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIGV.Location = new System.Drawing.Point(116, 175);
            this.txtIGV.Name = "txtIGV";
            this.txtIGV.ReadOnly = true;
            this.txtIGV.Size = new System.Drawing.Size(112, 27);
            this.txtIGV.TabIndex = 7;
            this.txtIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.superValidator1.SetValidator1(this.txtIGV, this.customValidator4);
            // 
            // txtValorVenta
            // 
            this.txtValorVenta.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtValorVenta.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorVenta.Location = new System.Drawing.Point(116, 141);
            this.txtValorVenta.Name = "txtValorVenta";
            this.txtValorVenta.ReadOnly = true;
            this.txtValorVenta.Size = new System.Drawing.Size(112, 27);
            this.txtValorVenta.TabIndex = 6;
            this.txtValorVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(48, 211);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 20);
            this.label10.TabIndex = 2;
            this.label10.Text = "TOTAL :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(72, 177);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 20);
            this.label9.TabIndex = 1;
            this.label9.Text = "IGV :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "SUBTOTAL :";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label11);
            this.panel3.Location = new System.Drawing.Point(921, 517);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(250, 76);
            this.panel3.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 42);
            this.label11.TabIndex = 10;
            this.label11.Text = "00.00";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDocRef
            // 
            this.txtDocRef.BackColor = System.Drawing.Color.PeachPuff;
            this.txtDocRef.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDocRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocRef.Location = new System.Drawing.Point(804, 9);
            this.txtDocRef.Multiline = true;
            this.txtDocRef.Name = "txtDocRef";
            this.txtDocRef.Size = new System.Drawing.Size(59, 50);
            this.txtDocRef.TabIndex = 40;
            this.txtDocRef.Tag = "10";
            this.txtDocRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDocRef.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocRef_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(939, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 42);
            this.label1.TabIndex = 43;
            this.label1.Tag = "22";
            this.label1.Text = "-";
            this.label1.Visible = false;
            // 
            // txtSerie
            // 
            this.txtSerie.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSerie.ForeColor = System.Drawing.Color.Red;
            this.txtSerie.Location = new System.Drawing.Point(867, 14);
            this.txtSerie.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtSerie.Multiline = true;
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.ReadOnly = true;
            this.txtSerie.Size = new System.Drawing.Size(72, 50);
            this.txtSerie.TabIndex = 45;
            this.txtSerie.Text = "000";
            this.txtSerie.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSerie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSerie_KeyDown);
            this.txtSerie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSerie_KeyPress);
            // 
            // txtPedido
            // 
            this.txtPedido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPedido.ForeColor = System.Drawing.Color.Red;
            this.txtPedido.Location = new System.Drawing.Point(969, 14);
            this.txtPedido.Multiline = true;
            this.txtPedido.Name = "txtPedido";
            this.txtPedido.ReadOnly = true;
            this.txtPedido.Size = new System.Drawing.Size(202, 50);
            this.txtPedido.TabIndex = 46;
            this.txtPedido.Text = "00000000";
            this.txtPedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // superValidator1
            // 
            this.superValidator1.ContainerControl = this;
            this.superValidator1.ErrorProvider = this.errorProvider1;
            this.superValidator1.Highlighter = this.highlighter1;
            // 
            // customValidator6
            // 
            this.customValidator6.ErrorMessage = "Your error message here.";
            this.customValidator6.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator6.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator6_ValidateValue);
            // 
            // customValidator2
            // 
            this.customValidator2.ErrorMessage = "Datos Cliente";
            this.customValidator2.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator2.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator2_ValidateValue);
            // 
            // customValidator1
            // 
            this.customValidator1.ErrorMessage = "Ingrese numero RUC o DNI";
            this.customValidator1.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator1.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator1_ValidateValue);
            // 
            // customValidator4
            // 
            this.customValidator4.ErrorMessage = "Your error message here.";
            this.customValidator4.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator4.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator4_ValidateValue);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
            // 
            // txtCodigoBarras
            // 
            this.txtCodigoBarras.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoBarras.Location = new System.Drawing.Point(691, 9);
            this.txtCodigoBarras.Multiline = true;
            this.txtCodigoBarras.Name = "txtCodigoBarras";
            this.txtCodigoBarras.ReadOnly = true;
            this.txtCodigoBarras.Size = new System.Drawing.Size(108, 50);
            this.txtCodigoBarras.TabIndex = 129;
            this.txtCodigoBarras.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbNombreTransaccion
            // 
            this.lbNombreTransaccion.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombreTransaccion.Location = new System.Drawing.Point(98, 16);
            this.lbNombreTransaccion.Name = "lbNombreTransaccion";
            this.lbNombreTransaccion.Size = new System.Drawing.Size(501, 43);
            this.lbNombreTransaccion.TabIndex = 137;
            this.lbNombreTransaccion.Text = "NombreTransaccion";
            // 
            // txtTransaccion
            // 
            this.txtTransaccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTransaccion.Enabled = false;
            this.txtTransaccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTransaccion.Location = new System.Drawing.Point(15, 18);
            this.txtTransaccion.Multiline = true;
            this.txtTransaccion.Name = "txtTransaccion";
            this.txtTransaccion.ReadOnly = true;
            this.txtTransaccion.Size = new System.Drawing.Size(75, 41);
            this.txtTransaccion.TabIndex = 138;
            this.txtTransaccion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTransaccion_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(13, 3);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 12);
            this.label15.TabIndex = 136;
            this.label15.Text = "Transacción";
            // 
            // chkVentaEspecial
            // 
            this.chkVentaEspecial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkVentaEspecial.AutoSize = true;
            this.chkVentaEspecial.Location = new System.Drawing.Point(605, 22);
            this.chkVentaEspecial.Name = "chkVentaEspecial";
            this.chkVentaEspecial.Size = new System.Drawing.Size(70, 17);
            this.chkVentaEspecial.TabIndex = 132;
            this.chkVentaEspecial.Text = "Vta. Esp.";
            this.chkVentaEspecial.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ImageIndex = 3;
            this.button1.ImageList = this.imageList1;
            this.button1.Location = new System.Drawing.Point(759, 604);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(196, 35);
            this.button1.TabIndex = 141;
            this.button1.Text = "IMPRIMIR DESPACHO";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnInicioOV
            // 
            this.btnInicioOV.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInicioOV.Image = ((System.Drawing.Image)(resources.GetObject("btnInicioOV.Image")));
            this.btnInicioOV.Location = new System.Drawing.Point(14, 603);
            this.btnInicioOV.Name = "btnInicioOV";
            this.btnInicioOV.Size = new System.Drawing.Size(176, 36);
            this.btnInicioOV.TabIndex = 140;
            this.btnInicioOV.Text = "F6 INICIAR VENTA";
            this.btnInicioOV.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInicioOV.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnInicioOV.UseVisualStyleBackColor = true;
            this.btnInicioOV.Click += new System.EventHandler(this.btnInicioOV_Click_1);
            // 
            // btnImprimir
            // 
            this.btnImprimir.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.Image = ((System.Drawing.Image)(resources.GetObject("btnImprimir.Image")));
            this.btnImprimir.Location = new System.Drawing.Point(562, 603);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(191, 36);
            this.btnImprimir.TabIndex = 139;
            this.btnImprimir.Text = "F10 IMPRIMIR VENTA";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Visible = false;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Enabled = false;
            this.btnSalir.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSalir.FlatAppearance.BorderSize = 2;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(380, 603);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(176, 36);
            this.btnSalir.TabIndex = 6;
            this.btnSalir.Text = " F9 CERRAR VENTA ";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardaVenta
            // 
            this.btnGuardaVenta.Enabled = false;
            this.btnGuardaVenta.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnGuardaVenta.FlatAppearance.BorderSize = 2;
            this.btnGuardaVenta.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardaVenta.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardaVenta.Image")));
            this.btnGuardaVenta.Location = new System.Drawing.Point(196, 603);
            this.btnGuardaVenta.Name = "btnGuardaVenta";
            this.btnGuardaVenta.Size = new System.Drawing.Size(178, 36);
            this.btnGuardaVenta.TabIndex = 7;
            this.btnGuardaVenta.Text = "F8 GUARDA VENTA ";
            this.btnGuardaVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardaVenta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardaVenta.UseVisualStyleBackColor = true;
            this.btnGuardaVenta.Click += new System.EventHandler(this.btnGuardaVenta_Click);
            // 
            // lblCantidadProductos
            // 
            this.lblCantidadProductos.AutoSize = true;
            this.lblCantidadProductos.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCantidadProductos.Location = new System.Drawing.Point(757, 398);
            this.lblCantidadProductos.Name = "lblCantidadProductos";
            this.lblCantidadProductos.Size = new System.Drawing.Size(155, 17);
            this.lblCantidadProductos.TabIndex = 142;
            this.lblCantidadProductos.Text = "Productos Agregados: 0";
            // 
            // frmGeneraVenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1176, 647);
            this.Controls.Add(this.lblCantidadProductos);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chkVentaEspecial);
            this.Controls.Add(this.btnInicioOV);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.txtTransaccion);
            this.Controls.Add(this.lbNombreTransaccion);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnGuardaVenta);
            this.Controls.Add(this.txtCodigoBarras);
            this.Controls.Add(this.txtPedido);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSerie);
            this.Controls.Add(this.txtDocRef);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmGeneraVenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "     ";
            this.Load += new System.EventHandler(this.frmGeneraVenta_Load);
            this.Shown += new System.EventHandler(this.frmGeneraVenta_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmGeneraVenta_KeyDown);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalle)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCapchatS)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton chkFactura;
        private System.Windows.Forms.RadioButton chkBoleta;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnGuardaVenta;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.CheckBox chkVentaGratuita;
        public System.Windows.Forms.CheckBox chkVentaDsctoGlobal;
        private System.Windows.Forms.TextBox txtNombreCliente;
        private System.Windows.Forms.TextBox txtCodCliente;
        public System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.DateTimePicker dtpFechaPago;
        private System.Windows.Forms.ComboBox cmbFormaPago;
        public System.Windows.Forms.TextBox txtDocRef;
        private System.Windows.Forms.TextBox txtSunat_Capchat;
        private System.Windows.Forms.PictureBox pbCapchatS;
        public System.Windows.Forms.DataGridView dgvDetalle;
        private System.Windows.Forms.TextBox txtPrecioVenta;
        private System.Windows.Forms.TextBox txtIGV;
        private System.Windows.Forms.TextBox txtValorVenta;
        private System.Windows.Forms.TextBox txtPDescuento;
        private System.Windows.Forms.TextBox txtBruto;
        private System.Windows.Forms.TextBox txtDscto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSerie;
        private System.Windows.Forms.TextBox txtPedido;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
        private DevComponents.DotNetBar.Validator.SuperValidator superValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator2;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator1;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator4;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator6;
        private System.Windows.Forms.TextBox txtinafectas;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtexoneradas;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtgratuitas;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtgravadas;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.TextBox txtCodigoBarras;
        private System.Windows.Forms.TextBox txtDelOV;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtComentario;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txttasa;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lbLineaCredito;
        private System.Windows.Forms.TextBox txtLineaCredito;
        private System.Windows.Forms.TextBox txtLineaCreditoUso;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtLineaCreditoDisponible;
        private System.Windows.Forms.TextBox txtAddOV;
        private System.Windows.Forms.Button btnDeleteItem;
        private System.Windows.Forms.Button btnAddOV;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cbovendedor;
        private System.Windows.Forms.RadioButton rbtnPendiente;
        public System.Windows.Forms.TextBox txtTransaccion;
        private System.Windows.Forms.Label lbNombreTransaccion;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtNumDoc;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnInicioOV;
        public System.Windows.Forms.CheckBox chkVentaEspecial;
        private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label lblCantidadProductos;
		private System.Windows.Forms.DataGridViewTextBoxColumn coddetalle;
		private System.Windows.Forms.DataGridViewTextBoxColumn codproducto;
		private System.Windows.Forms.DataGridViewTextBoxColumn referencia;
		private System.Windows.Forms.DataGridViewTextBoxColumn descripcion;
		private System.Windows.Forms.DataGridViewTextBoxColumn codunidad;
		private System.Windows.Forms.DataGridViewTextBoxColumn unidad;
		private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
		private System.Windows.Forms.DataGridViewTextBoxColumn preciounit;
		private System.Windows.Forms.DataGridViewTextBoxColumn importe;
		private System.Windows.Forms.DataGridViewTextBoxColumn dscto1;
		private System.Windows.Forms.DataGridViewTextBoxColumn dscto2;
		private System.Windows.Forms.DataGridViewTextBoxColumn dscto3;
		private System.Windows.Forms.DataGridViewTextBoxColumn montodscto;
		private System.Windows.Forms.DataGridViewTextBoxColumn valorventa;
		private System.Windows.Forms.DataGridViewTextBoxColumn igv;
		private System.Windows.Forms.DataGridViewTextBoxColumn precioventa;
		private System.Windows.Forms.DataGridViewTextBoxColumn valoreal;
		private System.Windows.Forms.DataGridViewTextBoxColumn precioreal;
		private System.Windows.Forms.DataGridViewTextBoxColumn precioconigv;
		private System.Windows.Forms.DataGridViewTextBoxColumn valorpromedio;
		private System.Windows.Forms.DataGridViewTextBoxColumn Tipoarticulo;
		private System.Windows.Forms.DataGridViewTextBoxColumn Tipoimpuesto;
		private System.Windows.Forms.DataGridViewTextBoxColumn codalmacen;
		private System.Windows.Forms.DataGridViewTextBoxColumn almacen;
		private System.Windows.Forms.DataGridViewTextBoxColumn TipoUnidad;
		private System.Windows.Forms.DataGridViewTextBoxColumn CodigoEmpresa;
		private System.Windows.Forms.DataGridViewTextBoxColumn descripcion_venta;
		private System.Windows.Forms.DataGridViewTextBoxColumn serie_motor;
		private System.Windows.Forms.DataGridViewTextBoxColumn nrochasis;
		private System.Windows.Forms.DataGridViewTextBoxColumn modelo;
		private System.Windows.Forms.DataGridViewTextBoxColumn Marca;
		private System.Windows.Forms.DataGridViewTextBoxColumn color;
		private System.Windows.Forms.DataGridViewTextBoxColumn codFamilia;
		private DevComponents.DotNetBar.Controls.ComboBoxEx cmbDocumentoIdentidad;
	}
}