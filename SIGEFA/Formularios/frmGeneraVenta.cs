﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using Tesseract;
using System.Text.RegularExpressions;
using AForge.Imaging.Filters;
using AForge;
using SIGEFA.Reportes;
using SIGEFA.Reportes.clsReportes;
using System.IO;
using DevComponents.DotNetBar;

namespace SIGEFA.Formularios
{
	public partial class frmGeneraVenta : Office2007Form
	{
		clsAdmTipoDocumento Admdoc = new clsAdmTipoDocumento();
		clsPago Pag = new clsPago();
		clsAdmPago AdmPagos = new clsAdmPago();
		SIGEFA.SunatFacElec.Conexion conex = new SunatFacElec.Conexion();
		clsReporteFactura ds1 = new clsReporteFactura();
		clsConsultasExternas ext = new clsConsultasExternas();
		public List<clsDetalleNotaSalida> detalle = new List<clsDetalleNotaSalida>();
		public List<clsDetalleFacturaVenta> detalle1 = new List<clsDetalleFacturaVenta>();
		clsCaja Caja = new clsCaja();
		clsAdmAperturaCierre AdmCaja = new clsAdmAperturaCierre();
		clsAdmVendedor AdmVen = new clsAdmVendedor();

		public Byte[] firmadigital { get; set; }
        public Byte[] LogoEmpresa { get; set; }

        clsAdmFacturaVenta AdmVenta = new clsAdmFacturaVenta();
		clsFacturaVenta venta = new clsFacturaVenta();
		List<clsNotaCredito> ncredito = new List<clsNotaCredito>();
		clsAdmNotaCredito admNotac = new clsAdmNotaCredito();
		clsAdmTransaccion AdmTran = new clsAdmTransaccion();
		clsTransaccion tran = new clsTransaccion();
		List<clsFacturaVenta> ltaventa = new List<clsFacturaVenta>();
		clsReporteCodigoBarras ds = new clsReporteCodigoBarras();
		clsTipoDocumento doc = new clsTipoDocumento();
		clsAdmCliente AdmCli = new clsAdmCliente();
		clsCliente cli = new clsCliente();
		clsAdmFormaPago AdmPago = new clsAdmFormaPago();
		clsFormaPago fpago = new clsFormaPago();
		clsAdmPedido AdmPedido = new clsAdmPedido();
		clsPedido pedido = new clsPedido();
		clsValidar ok = new clsValidar();
		clsSerie ser = new clsSerie();
		clsAdmSerie AdmSerie = new clsAdmSerie();
		clsEmpresa empress = new clsEmpresa();
		clsAdmEmpresa admempress = new clsAdmEmpresa();
		clsAdmUsuario AdmUs = new clsAdmUsuario();
		clsAdmDocumentoIdentidad AdmDocumentoIdentidad = new clsAdmDocumentoIdentidad();
		public Int32 CodSerie, manual = 0;
		Sunat MyInfoSunat;
		Reniec MyInfoReniec;
		IntRange red = new IntRange(0, 255);
		IntRange green = new IntRange(0, 255);
		IntRange blue = new IntRange(0, 255);
		DataTable NuevaTabla = new DataTable();
		Int32 item = 1;
		Boolean banderainsertCabera = false;

		//Cambio André
		private List<clsPedido> PedidosIngresados = new List<clsPedido>();

		public Int32 impresion, CodCliente, CodDocumento, Tipo, CodPedido, CodTransaccion, CodigoCaja, Procede, Proceso = 0, ActivaCabecera = 0;
		public Decimal montogratuitas, montogravadas, montoexoneradas = 0, montoinafectas = 0;
		public String CodVenta, NombreCliente;
		public Boolean banderagrabada, banderaexonerada, banderainafecta, banderadelete = false, bandera = false;

		DateTime time = DateTime.Now;

		private Boolean banderaCancelarIngresoSerie = false;

		/*
		 * Clase utilizada para la version UBL 2.1
		 */
		SIGEFA.SunatFacElec.Facturacion facturacion = new SunatFacElec.Facturacion();

		public frmGeneraVenta()
		{
			InitializeComponent();
			this.PedidosIngresados = new List<clsPedido>();
		}

		private void frmGeneraVenta_KeyDown(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.F6:
					this.activaPaneles(true);

					break;

				case Keys.F3:
					if (bandera) { if (txtAddOV.Text != "") { btnAddOV_Click(sender, new EventArgs()); } else { txtAddOV.Focus(); txtAddOV.SelectAll(); } }
					break;

				case Keys.F2:
					if (bandera) { btnDeleteItem_Click(sender, new EventArgs()); }
					break;

				case Keys.F1:
					if (bandera) { txtCodCliente_KeyDown(sender, e); }
					break;

				case Keys.F8:
					btnGuardaVenta_Click(sender, e);
					break;

				case Keys.F9:
					this.Close();
					break;
			}
		}

		private void activaPaneles(Boolean estado)
		{
			groupBox2.Enabled = estado;
			btnAddOV.Enabled = estado;
			btnDeleteItem.Enabled = estado;
			btnSalir.Enabled = estado;
			btnGuardaVenta.Enabled = estado;
			groupBox4.Enabled = estado;
			panel2.Enabled = estado;
			btnInicioOV.Enabled = !estado;
			bandera = estado;// para saber si se activaron los controles de la venta
		}

		private void txtCodCliente_KeyDown(object sender, KeyEventArgs e)
		{
			/* if (e.KeyCode == Keys.F1)
             {
                 if (Application.OpenForms["frmClientesLista"] != null)
                 {
                     Application.OpenForms["frmClientesLista"].Activate();
                 }
                 else
                 {
                     frmClientesLista form = new frmClientesLista();
                     form.Proceso = 3;
                     form.ShowDialog();                   
                     cli = form.cli;
                     CodCliente = cli.CodCliente;
                     if (CodCliente != 0) {
                         txtCodCliente.Text = "";
                         txtDireccion.Text = "";
                         txtNombreCliente.Text = "";
                         NombreCliente = cli.Nombre; CargaCliente(); //ProcessTabKey(true); 

                     }
                 }
             }*/
		}

		private void CargaCliente()
		{
			cli = AdmCli.MuestraCliente(CodCliente);
			txtCodCliente.Text = cli.RucDni;
			txtNombreCliente.Text = cli.RazonSocial;
			txtDireccion.Text = cli.DireccionLegal;
		}

		private Boolean BuscaCliente()
		{
			if (txtCodCliente.Text != "")
			{
				cli = AdmCli.BuscaCliente(txtCodCliente.Text, Tipo);
				if (cli != null)
				{
					txtNombreCliente.Text = cli.RazonSocial;
					CodCliente = cli.CodCliente;
					txtDireccion.Text = cli.DireccionLegal;
					txtCodCliente.Text = cli.RucDni;
					if (cli.RucDni == "00000000")
					{
						txtNombreCliente.Enabled = true;
					}

					txtDocRef.Visible = false;
					return true;
				}
				else
				{
					txtNombreCliente.Text = "";
					CodCliente = 0;
					txtDireccion.Text = "";
					return false;
				}
			}
			else { return false; }
		}

		private void CargaFormaPagos()
		{
			cmbFormaPago.DataSource = AdmPago.CargaFormaPagos(1);
			cmbFormaPago.DisplayMember = "descripcion";
			cmbFormaPago.ValueMember = "codFormaPago";
			cmbFormaPago.SelectedIndex = 0;

			cmbFormaPago_SelectionChangeCommitted(new object(), new EventArgs());
			cmbFormaPago.Visible = true;
			dtpFechaPago.Visible = true;
		}

		private Boolean BuscaTipoDocumento()
		{
			doc = Admdoc.BuscaTipoDocumento(txtDocRef.Text);
			if (doc != null)
			{
				CodDocumento = doc.CodTipoDocumento;
				label1.Visible = true;
				return true;
			}
			else
			{
				CodDocumento = 0;
				return false;
			}

		}

		private void frmGeneraVenta_Shown(object sender, EventArgs e)
		{
			txtTransaccion.Focus();
			txtTransaccion.Text = "FT";
			KeyPressEventArgs e1 = new KeyPressEventArgs((char)Keys.Return);
			txtTransaccion_KeyPress(txtTransaccion, e1);

			if (Proceso == 1)
			{
				txtDocRef.Text = "BV";
				txtCodCliente.Text = "C000001";
				KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
				txtDocRef_KeyPress(txtDocRef, ee);
				txtSerie_KeyPress(txtDocRef, ee);
				BuscaCliente();
				cargadatoscliente(txtCodCliente.Text);
				calculatotales();
			}
			else if (Proceso == 3)
			{
				btnSalir.Visible = true; btnSalir.Enabled = true;
			}
		}

		private void txtDocRef_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)Keys.Return)
			{
				if (txtDocRef.Text != "")
				{
					if (BuscaTipoDocumento())
					{
						//ProcessTabKey(true);
					}
					else
					{
						MessageBox.Show("Codigo de Documento no existe, Presione F1 para consultar la tabla de ayuda", "NOTA DE SALIDA", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
		}

		private void cmbFormaPago_SelectionChangeCommitted(object sender, EventArgs e)
		{
			if (cmbFormaPago.SelectedIndex != -1)
			{
				fpago = AdmPago.CargaFormaPago(Convert.ToInt32(cmbFormaPago.SelectedValue));
				dtpFechaPago.Value = dtpFecha.Value.AddDays(fpago.Dias);
			}
		}

		private void txtCodCliente_KeyPress(object sender, KeyPressEventArgs e)
		{
			ok.enteros(e);
			if ((int)e.KeyChar == (int)Keys.Enter)
			{
				try
				{
					//Cursor = Cursors.WaitCursor;
					switch (this.txtCodCliente.Text.Length)
					{
						case 1:
							MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo un digito Ingresado",
								"Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
							txtCodCliente.SelectAll();
							txtCodCliente.Focus();
							break;

						case 2:
							MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo dos digitos Ingresados",
								"Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
							txtCodCliente.SelectAll();
							txtCodCliente.Focus();
							break;

						case 3:
							MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo tres digitos Ingresados",
								"Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
							txtCodCliente.SelectAll();
							txtCodCliente.Focus();
							break;

						case 4:
							MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo cuatro digitos Ingresados",
								"Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
							txtCodCliente.SelectAll();
							txtCodCliente.Focus();
							break;

						case 5:
							MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo cinco digitos Ingresados",
								"Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
							txtCodCliente.SelectAll();
							txtCodCliente.Focus();
							break;

						case 6:
							MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo seis digitos Ingresados",
								"Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
							txtCodCliente.SelectAll();
							txtCodCliente.Focus();
							break;

						case 7:
							MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo siete digitos Ingresados",
								"Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
							txtCodCliente.SelectAll();
							txtCodCliente.Focus();
							break;

						case 8:
							cli = AdmCli.ConsultaCliente(txtCodCliente.Text);
							if (cli != null)
							{
								CodCliente = cli.CodCliente;
								txtNombreCliente.Text = cli.Nombre;
								txtDireccion.Text = cli.DireccionLegal;

								if (cli.Moneda == 1)
								{
									txtLineaCredito.Text = String.Format("{0:#,##0.00}", cli.LineaCredito);
									txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoDisponible);
									txtLineaCreditoUso.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoUsado);
									txttasa.Text = String.Format("{0:#,##0.00}", cli.Tasa);
									lbLineaCredito.Text = "Línea de Crédito (S/.):";
									label23.Text = "Línea Disponible (S/.):";
									label25.Text = "Línea C. en Uso (S/.):";
									if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
								}
								else
								{
									txtLineaCredito.Text = String.Format("{0:#,##0.00}", (cli.LineaCredito / Convert.ToDecimal(mdi_Menu.tc_hoy)));
									txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoDisponible / Convert.ToDecimal(mdi_Menu.tc_hoy)));
									txtLineaCreditoUso.Text = Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoUsado / Convert.ToDecimal(mdi_Menu.tc_hoy)));
									lbLineaCredito.Text = "Línea de Crédito ($.):";
									label23.Text = "Línea Disponible ($.):";
									label25.Text = "Línea C. en Uso ($.):";
									if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
								}
								if (cli.FormaPago != 0)
								{
									cmbFormaPago.SelectedValue = cli.FormaPago;
									EventArgs ee = new EventArgs();
									cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
								}
								else
								{
									dtpFechaPago.Value = DateTime.Today;
								}
							}
							else
							{
								CargarImagenReniec();
								CargaDNI();
								CodCliente = 0;
							}
							chkBoleta.Checked = true;
							break;

						case 9:
							MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ingreso nueve digitos ",
								"Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
							break;

						case 10:
							MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ingreso diez digitos ",
								"Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
							txtCodCliente.SelectAll();
							txtCodCliente.Focus();
							break;

						case 11:
							cli = AdmCli.ConsultaCliente(txtCodCliente.Text);
							if (cli != null)
							{
								CodCliente = cli.CodCliente;
								txtNombreCliente.Text = cli.RazonSocial;
								txtDireccion.Text = cli.DireccionLegal;

								if (cli.Moneda == 1)
								{
									txtLineaCredito.Text = String.Format("{0:#,##0.00}", cli.LineaCredito);
									txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoDisponible);
									txtLineaCreditoUso.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoUsado);
									txttasa.Text = String.Format("{0:#,##0.00}", cli.Tasa);
									lbLineaCredito.Text = "Línea de Crédito (S/.):";
									label23.Text = "Línea Disponible (S/.):";
									label25.Text = "Línea C. en Uso (S/.):";
									if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
								}
								else
								{
									txtLineaCredito.Text = String.Format("{0:#,##0.00}", (cli.LineaCredito / Convert.ToDecimal(mdi_Menu.tc_hoy)));
									txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoDisponible / Convert.ToDecimal(mdi_Menu.tc_hoy)));
									txtLineaCreditoUso.Text = Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoUsado / Convert.ToDecimal(mdi_Menu.tc_hoy)));
									lbLineaCredito.Text = "Línea de Crédito ($.):";
									label23.Text = "Línea Disponible ($.):";
									label25.Text = "Línea C. en Uso ($.):";
									if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
								}
								if (cli.FormaPago != 0)
								{
									cmbFormaPago.SelectedValue = cli.FormaPago;
									EventArgs ee = new EventArgs();
									cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
								}
								else
								{
									dtpFechaPago.Value = DateTime.Today;
								}
							}
							else
							{
								CargarImagenSunat();
								CargaRUC();
								CodCliente = 0;
							}
							chkFactura.Checked = true;
							break;

						default:
							ValidaLongitud();
							break;
					}

					//cbFamilia.Select();                    

				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
					CargarImagenSunat();
				}
			}
		}

		private void ValidaLongitud()
		{
			if (txtCodCliente.Text.Length == 0)
			{
				MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ningun digito Ingresado",
							   "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
			}
			else if (txtCodCliente.Text.Length > 11)
			{
				MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ha Ingresado " + txtCodCliente.Text.Length + " Digitos",
							   "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
				txtCodCliente.SelectAll();
				txtCodCliente.Focus();
			}
		}

		private void CargaDNI()
		{
			MyInfoReniec.GetInfo(this.txtCodCliente.Text, this.txtSunat_Capchat.Text);
			switch (MyInfoReniec.GetResul)
			{
				case Reniec.Resul.Ok:
					limpiarSunat();
					txtCodCliente.Text = MyInfoReniec.Dni;
					String apellidos = MyInfoReniec.ApePaterno + " " + MyInfoReniec.ApeMaterno;
					txtNombreCliente.Text = MyInfoReniec.Nombres + " " + apellidos;
					txtDireccion.Text = "S/D";
					break;
				case Reniec.Resul.NoResul:
					limpiarSunat();
					MessageBox.Show("No Existe DNI");
					break;
				case Reniec.Resul.ErrorCapcha:
					limpiarSunat();
					MessageBox.Show("Ingrese imagen correctamente");
					break;
				default:
					MessageBox.Show("Error Desconocido");
					break;
			}
			//Comentar esta linea para consultar multiples DNI usando un solo captcha.
			CargarImagenReniec();
		}

		#region RENIEC

		private void AplicacionFiltros()
		{
			Bitmap bmp = new Bitmap(pbCapchatS.Image);
			FiltroInvertir(bmp);
			ColorFiltros();
			Bitmap bmp1 = new Bitmap(pbCapchatS.Image);
			FiltroInvertir(bmp1);
			Bitmap bmp2 = new Bitmap(pbCapchatS.Image);
			FiltroSharpen(bmp2);
		}

		private void FiltroInvertir(Bitmap bmp)
		{
			IFilter Filtro = new Invert();
			Bitmap XImage = Filtro.Apply(bmp);
			pbCapchatS.Image = XImage;
		}

		private void ColorFiltros()
		{
			//Red Min - MAX
			red.Min = Math.Min(red.Max, byte.Parse("229"));
			red.Max = Math.Max(red.Min, byte.Parse("255"));
			//Verde Min - MAX
			green.Min = Math.Min(green.Max, byte.Parse("0"));
			green.Max = Math.Max(green.Min, byte.Parse("255"));
			//Azul Min - MAX
			blue.Min = Math.Min(blue.Max, byte.Parse("0"));
			blue.Max = Math.Max(blue.Min, byte.Parse("130"));
			ActualizarFiltro();
		}

		private void ActualizarFiltro()
		{
			ColorFiltering FiltroColor = new ColorFiltering();
			FiltroColor.Red = red;
			FiltroColor.Green = green;
			FiltroColor.Blue = blue;
			IFilter Filtro = FiltroColor;
			Bitmap bmp = new Bitmap(pbCapchatS.Image);
			Bitmap XImage = Filtro.Apply(bmp);
			pbCapchatS.Image = XImage;
		}

		private void FiltroSharpen(Bitmap bmp)
		{
			IFilter Filtro = new Sharpen();
			Bitmap XImage = Filtro.Apply(bmp);
			pbCapchatS.Image = XImage;
		}
		private void CargarImagenReniec()
		{
			try
			{
				if (MyInfoReniec == null)
					MyInfoReniec = new Reniec();
				this.pbCapchatS.Image = MyInfoReniec.GetCapcha;
				AplicacionFiltros();
				LeerCaptchaReniec();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		private void LeerCaptchaReniec()
		{
			using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
			{
				using (var image = new System.Drawing.Bitmap(pbCapchatS.Image))
				{
					using (var pix = PixConverter.ToPix(image))
					{
						using (var page = engine.Process(pix))
						{
							var Porcentaje = String.Format("{0:P}", page.GetMeanConfidence());
							string CaptchaTexto = page.GetText();
							char[] eliminarChars = { '\n', ' ' };
							CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars);
							CaptchaTexto = CaptchaTexto.Replace(" ", string.Empty);
							CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z0-9]+", string.Empty);
							if (CaptchaTexto != string.Empty & CaptchaTexto.Length == 4)
								txtSunat_Capchat.Text = CaptchaTexto.ToUpper();
							//else
							//    CargarImagenReniec();
						}
					}
				}
			}
		}

		#endregion

		#region metodos Sunat

		private void CargaRUC()
		{
			if (this.txtCodCliente.Text.Length == 11)
			{
				LeerDatos();
			}
		}
		private void LeerDatos()
		{
			//llamamos a los metodos de la libreria ConsultaReniec...
			MyInfoSunat.GetInfo(this.txtCodCliente.Text, this.txtSunat_Capchat.Text);
			switch (MyInfoSunat.GetResul)
			{
				case Sunat.Resul.Ok:
					limpiarSunat();
					txtCodCliente.Text = MyInfoSunat.Ruc;
					txtDireccion.Text = MyInfoSunat.Direcion;
					txtNombreCliente.Text = MyInfoSunat.RazonSocial;
					Ciudad(MyInfoSunat.Direcion);
					BloqueaDatos();
					break;
				case Sunat.Resul.NoResul:
					limpiarSunat();
					MessageBox.Show("No Existe RUC");
					break;
				case Sunat.Resul.ErrorCapcha:
					limpiarSunat();
					MessageBox.Show("Ingrese imagen correctamente");
					break;
				default:
					MessageBox.Show("Error Desconocido");
					break;
			}
			//CargarImagenSunat();
		}
		private void CargarImagenSunat()
		{
			try
			{
				if (MyInfoSunat == null)
					MyInfoSunat = new Sunat();
				this.pbCapchatS.Image = MyInfoSunat.GetCapcha;
				LeerCaptchaSunat();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		private void LeerCaptchaSunat()
		{
			//string ruta = Directory.GetCurrentDirectory()+"\\tessdata";
			//string RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tessdata\\");
			try
			{
				using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
				{
					using (var image = new System.Drawing.Bitmap(pbCapchatS.Image))
					{
						using (var pix = PixConverter.ToPix(image))
						{
							using (var page = engine.Process(pix))
							{
								var Porcentaje = String.Format("{0:P}", page.GetMeanConfidence());
								string CaptchaTexto = page.GetText();
								char[] eliminarChars = { '\n', ' ' };
								CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars);
								CaptchaTexto = CaptchaTexto.Replace(" ", string.Empty);
								CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z]+", string.Empty);
								if (CaptchaTexto != string.Empty & CaptchaTexto.Length == 4)
									txtSunat_Capchat.Text = CaptchaTexto.ToUpper();
								else
									CargarImagenSunat();
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		private void Ciudad(string Direccion)
		{
			String[] array = Direccion.Split('-');
			if (array.Length > 1)
			{
				int a = array.Length;
				String DirTemp = array[a - 3].Trim();
				DirTemp = DirTemp.TrimEnd(' ');
				String[] ArrayDir = DirTemp.Split(' ');
				int i = ArrayDir.Length;
				//cbDepartamento.Text = ArrayDir[i - 1].Trim();
				//cbProvincia.Text = array[a - 2].Trim();
				//cbDistrito.Text = array[a - 1].Trim();
			}
		}
		private void limpiarSunat()
		{
			txtNombreCliente.Text = "";
			txtSunat_Capchat.Text = string.Empty;
		}
		private void BloqueaDatos()
		{
			/*txtRUC.ReadOnly = true; */
			txtDireccion.ReadOnly = true; txtNombreCliente.ReadOnly = true;
		}

		#endregion

		private void dgvDetalle_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
		{
			if (Proceso == 1)
			{
				Recalcular();
			}
		}

		private void calculatotales()
		{
			Double bruto = 0; Double descuen = 0; Double valor = 0;

			foreach (DataGridViewRow row in dgvDetalle.Rows)
			{
				bruto = bruto + Convert.ToDouble(row.Cells[importe.Name].Value);
				descuen = descuen + Convert.ToDouble(row.Cells[montodscto.Name].Value);
				valor = valor + Convert.ToDouble(row.Cells[valorventa.Name].Value);

				if (row.Index == dgvDetalle.CurrentRow.Index)
				{
					if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "21" && banderadelete) // gratuitas
					{
						montogratuitas = montogratuitas - (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value));
						montosventa();
						banderadelete = false;
					}

					if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "10" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "11" && banderadelete ||
						Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "12" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "13" && banderadelete ||
						Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "14" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "15" && banderadelete ||
						Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "16" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "17" && banderadelete)   // gravadas
					{
						montogravadas = montogravadas - (Convert.ToDecimal(row.Cells[valorventa.Name].Value));
						montosventa();
						banderadelete = false;
					}

					if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "20" && banderadelete) // exoneradas
					{
						montoexoneradas = montoexoneradas - (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value));
						montosventa();
						banderadelete = false;
					}

					if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "30" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "31" && banderadelete ||
						Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "32" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "33" && banderadelete ||
						Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "34" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "35" && banderadelete ||
						Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "36" && banderadelete) // inafectas
					{
						montoinafectas = montoinafectas - (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value));
						montosventa();
						banderadelete = false;
					}
				}
			}
			txtBruto.Text = String.Format("{0:#,##0.00}", bruto);
			txtValorVenta.Text = String.Format("{0:#,##0.00}", valor);
			txtIGV.Text = String.Format("{0:#,##0.00}", bruto - descuen - valor);
			txtPrecioVenta.Text = String.Format("{0:#,##0.00}", bruto - descuen);
			txtPrecioVenta.Text = txtBruto.Text;
			label11.Text = txtBruto.Text;
			montosventa();
		}

		public void montosventa()
		{
			if (Proceso != 0 && Proceso != 3)
			{
				if (montogravadas > 0)
				{
					txtgravadas.Clear();
					txtgravadas.Text = String.Format("{0:#,##0.00}", montogravadas);
				}
				else { txtgravadas.Text = String.Format("{0:#,##0.00}", 0); }

				if (montogratuitas > 0)
				{
					txtgratuitas.Clear();
					txtgratuitas.Text = String.Format("{0:#,##0.00}", montogratuitas);
				}
				else { txtgratuitas.Text = String.Format("{0:#,##0.00}", 0); }

				if (montoexoneradas > 0)
				{
					txtexoneradas.Clear();
					txtexoneradas.Text = String.Format("{0:#,##0.00}", montoexoneradas);
				}
				else { txtexoneradas.Text = String.Format("{0:#,##0.00}", 0); }

				if (montoinafectas > 0)
				{
					txtinafectas.Clear();
					txtinafectas.Text = String.Format("{0:#,##0.00}", montoinafectas);
				}
				else { txtinafectas.Text = String.Format("{0:#,##0.00}", 0); }
			}
		}


		private void Recalcular()
		{
			Double bruto = 0;
			Double descuen = 0;
			Double valor = 0;
			Double figv = 0;
			Double pven = 0;
			foreach (DataGridViewRow row in dgvDetalle.Rows)
			{
				bruto = bruto + Convert.ToDouble(row.Cells[importe.Name].Value);
				descuen = descuen + Convert.ToDouble(row.Cells[montodscto.Name].Value);
				valor = valor + Convert.ToDouble(row.Cells[valorventa.Name].Value);
				figv = figv + Convert.ToDouble(row.Cells[igv.Name].Value);
				pven = pven + Convert.ToDouble(row.Cells[precioventa.Name].Value);
			}
			txtBruto.Text = String.Format("{0:#,##0.00}", bruto);
			txtDscto.Text = String.Format("{0:#,##0.00}", descuen);
			txtValorVenta.Text = String.Format("{0:#,##0.00}", valor);
			txtIGV.Text = String.Format("{0:#,##0.00}", figv);
			txtPrecioVenta.Text = String.Format("{0:#,##0.00}", pven);
			label11.Text = txtBruto.Text;
			montosventa();
		}

		private void txtCantidadPago_KeyPress(object sender, KeyPressEventArgs e)
		{
			ok.SOLONumeros(sender, e);
		}


		private void txtVuelto_Leave(object sender, EventArgs e)
		{
			//btnGuardar.Focus();
			//btnGuardar.Select();
		}

		private void dgvDetalle_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
		{
			btnDeleteItem_Click(sender, new EventArgs());
			/* if (Proceso == 1 || Proceso == 2)
             {
                 if (txtPDescuento.Text != "")
                 {
                     calculatotales();                    
                 }
                 else
                 {
                     calculatotales();
                 }
             }*/
		}

		private void btnDeleteItem_Click(object sender, EventArgs e)
		{


			if (dgvDetalle.SelectedRows.Count > 0 && dgvDetalle.Rows.Count > 0)
			{
				if (Convert.ToInt32(dgvDetalle.CurrentRow.Cells[coddetalle.Name].Value) == 0)
				{
					banderadelete = true;
					calculatotales();
					dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow);
					calculatotales();
				}
				else
				{
					if (MessageBox.Show("¿Realmente desea eliminar el item seleccionado?", "Pedido Venta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{

						if (AdmPedido.deletedetalle(Convert.ToInt32(dgvDetalle.CurrentRow.Cells[coddetalle.Name].Value)))
						{
							MessageBox.Show("El detalle se eliminó correctamente", "Pedido Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
							banderadelete = true;
							calculatotales();
							dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow);
							calculatotales();
						}
					}
				}
			}
		}

		private void btnSalir_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void txtSerie_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				if (Application.OpenForms["frmSerie"] != null)
				{
					Application.OpenForms["frmSerie"].Activate();
				}
				else
				{
					frmSerie form = new frmSerie();
					form.Proceso = 3;
					form.DocSeleccionado = CodDocumento;
					form.ShowDialog();
					ser = form.ser;
					CodSerie = ser.CodSerie;
					manual = Convert.ToInt32(ser.PreImpreso);
					if (CodSerie != 0)
					{
						txtSerie.Text = ser.Serie;
					}
					if (CodSerie != 0) { }
				}
			}
		}

		private async void btnGuardaVenta_Click(object sender, EventArgs e)
		{
			try
			{
				btnGuardaVenta.Enabled = false;
				/*
				* buscar el documento de identidad por el codigo del
				* combobox seleccionado
				*/
				clsDocumentoIdentidad documentoIdentidadSeleccionado = AdmDocumentoIdentidad.MuestraDocumentoIdentidad(Convert.ToInt32(cmbDocumentoIdentidad.SelectedValue));

				if (documentoIdentidadSeleccionado == null)
				{
					MessageBox.Show("Por favor seleccione un tipo de documento",
									"Genera Venta", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}

				Double totalsoles = 0;
				
				if (superValidator1.Validate())
				{
					//using (var Scope = new TransactionScope())
					//{
					if (Convert.ToInt32(cli.Moneda) == 1)
					{
						totalsoles = Convert.ToDouble(txtPrecioVenta.Text);
					}
					else
					{
						totalsoles = (Convert.ToDouble(txtPrecioVenta.Text) * mdi_Menu.tc_hoy);
					}
					if ((totalsoles > Convert.ToDouble(txtLineaCreditoDisponible.Text)) && Convert.ToInt32(cmbFormaPago.SelectedValue) != 6)
					{
						MessageBox.Show("El Monto Excede a la Línea de Crédito");
						btnGuardaVenta.Enabled = true;
					}
					else
					{
						if (Proceso != 0)
						{
							GeneraListaEnpresas();
							venta.CodSucursal = frmLogin.iCodSucursal;
							venta.CodAlmacen = frmLogin.iCodAlmacen;
							venta.CodTipoTransaccion = tran.CodTransaccion;
							venta.CodCliente = CodCliente;
							/*agregado*/
							venta.NumeroDocumentoCliente = txtCodCliente.Text;
							venta.CodTipoDocumento = doc.CodTipoDocumento;
							venta.Detallecomentario = "";
							venta.CodCotizacion = 0;
							if (chkBoleta.Checked)
							{
								venta.Boletafactura = 1;
							}
							else if (chkFactura.Checked)
							{
								venta.Boletafactura = 2;
							}

							//venta.CodSerie = CodSerie;
							//venta.Serie = txtSerie.Text;
							venta.NumDoc = txtPedido.Text;

							venta.Estado = 1;
							venta.Consultorext = false;
							venta.Codsalidaconsulext = 0;

							/*
							 * ASIGNAR CODIGO DE PEDIDO
							 */
							venta.CodPedido = Convert.ToInt32(pedido.CodPedido);

							venta.Moneda = 1;
							venta.TipoCambio = mdi_Menu.tc_hoy;
							venta.FechaSalida = dtpFecha.Value;
							venta.FechaPago = dtpFechaPago.Value;
							venta.FormaPago = Convert.ToInt32(cmbFormaPago.SelectedValue);
							venta.CodListaPrecio = 0;
							venta.CodVendedor = Convert.ToInt32(cbovendedor.SelectedValue);
							venta.Comentario = txtComentario.Text;
							venta.CodUser = frmLogin.iCodUser;
							venta.Entregado = Convert.ToInt32(rbtnPendiente.Checked);

							venta.CodigoBarras = DateTime.Today.Year.ToString().Substring(2, 2) + DateTime.Today.Month.ToString().PadLeft(2, '0') +
									DateTime.Today.Day.ToString().PadLeft(2, '0') + DateTime.Now.ToShortTimeString().Substring(0, 2) + DateTime.Now.ToShortTimeString().Substring(3, 2) +
									venta.CodSerie.ToString().PadLeft(3, '0') + CodCliente;
							venta.CodigoBarrasCifrado = "";//ok.Encode(venta.CodigoBarras);
							foreach (clsPedido ped in PedidosIngresados)
							{
								ped.CodigoBarras = venta.CodigoBarras;
								ped.CodigoBarrasCifrado = venta.CodigoBarrasCifrado;
							}
							if (txtCodCliente.Text.ToString() == "00000000") { venta.Nombre = txtNombreCliente.Text; } else { venta.Nombre = ""; }

							clsFacturaVenta factura = new clsFacturaVenta();
							factura = AdmVenta.FechaCorrelativoAnterior(venta.CodSerie);

							foreach (Int32 lista in ListaEmpresa)
							{
								if (mdi_Menu.MontoTopeBoleta > 0 && chkVentaEspecial.Checked)
								{
									CrearTablaTemporal();
									OrdenarTablaTemporal();
									CreaBoletas(lista);
								}
								else { ArmaCabecera(lista); if (ActivaCabecera == 0) return; }
								venta.CodEmpresa = lista;
								if (Proceso == 4 || Proceso == 1)
								{
									if (factura.FechaSalida > venta.FechaSalida.Date)
									{
										MessageBox.Show("Error No se puede Registrar los Datos. Verifique Fecha");
									}
									else
									{
										if (VerificarDetracciones())
										{

											if (mdi_Menu.MontoTopeBoleta > 0 && chkVentaEspecial.Checked)
											{
												for (int i = 1; i <= item; i++)
												{
													NuevaCabecera(i);
													if (AdmVenta.insert(venta))
													{
														RecorreDetalleEspecial(i, lista);
														ImprimeEspecial(lista);
													}
												}
											}
											else
											{
												/*
												 * Recorrer detalle de venta antes para pasar la lista
												 * detalle1 dentro del objeto venta
												 */
												RecorreDetalle(lista);
												venta.Detalle = detalle1;

												/*
												 * agregar al objeto venta la propiedad documentoIdentidad
												 * esto se realiza para usar el codigo de documento (dni,ruc, carnet ext., etc)
												 * de manera dinamica
												 */
												venta.DocumentoIdentidad = documentoIdentidadSeleccionado;

												if (detalle1.Count > 0)
												{
													/*
													* realizar la inserción en una sola
													* función enviando el objeto venta
													* con la lista de detalles
													*/
													if (AdmVenta.insertComprobante(venta))
													{
														MessageBox.Show("Los datos se guardaron correctamente",
															"Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
														txtNumDoc.Text = venta.CodFacturaVenta.PadLeft(11, '0');
														ltaventa.Add(venta);

														/**
														 * MOSTRAR FORMULARIO PARA
														 * REALIZAR PAGO POR EFECTIVO,TARJETA DE CREDITO,
														 * TRANSFERENCIA, ETC
														 */
														frmCancelarPago form = new frmCancelarPago();
														form.CodNota = venta.CodFacturaVenta;
														form.VentComp = 1;
														form.tipo = 3;
														form.CodCliente = cli.CodCliente;
														form.FormClosed += (object senderfc, FormClosedEventArgs efc) => Form_Pago_Closed(senderfc, efc, venta.CodFacturaVenta, form.VentComp, form.tipo, form.CodCliente);
														form.ShowDialog();

														//FACTURACION ELECTRONICA
														if (lista != 13)
														{
															//conex.GeneraXML(cli, venta, detalle1);
															await facturacion.GeneraDocumento(cli, venta, detalle1);
															firmadigital = facturacion.LogoEmp;
														}
														fnImprimir();
													}
													else
													{
														MessageBox.Show("Ocurrió un problema al registrar la venta.",
																		"Registro de Venta",MessageBoxButtons.OK, 
																		MessageBoxIcon.Error);
													}

												}

											}
										}
									}
								}
							}
							//actualizo el codigo de barras de los pedido
							foreach (clsPedido ped in PedidosIngresados)
							{
								AdmPedido.GuardaCodigoBarras(ped);
							}
						}
						this.PedidosIngresados = new List<clsPedido>();
						this.Close();
					}
				}
				btnGuardaVenta.Enabled = true;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error: " + ex.Message.ToString());
			}
		}

		void Form_Closed(object sender, FormClosedEventArgs e)
		{
			frmAgregaInformacionProducto frm = (frmAgregaInformacionProducto)sender;
			banderaCancelarIngresoSerie = frm.cancelado;
		}

		void Form_Pago_Closed(object sender, FormClosedEventArgs e, String codigoVenta, Int32 compraVen, Int32 tipo, Int32 codigoCliente)
		{
			frmCancelarPago frm = (frmCancelarPago)sender;
			Decimal montoPendiente = Convert.ToDecimal(frm.txtMontoPendiente.Text);
			if (montoPendiente > 0)
			{
				DialogResult dr = MessageBox.Show("LA VENTA TIENE MONTO PENDIENTE DE PAGO ¿DESEA CONTINUAR INGRESÁNDOLOS?",
												  "ALERTA DE MONTO PENDIENTE", MessageBoxButtons.YesNo,
												  MessageBoxIcon.Exclamation);
				if (dr == DialogResult.Yes)
				{
					/**
					* mostrar formulario para ingresar precio nuevo de venta del producto
					*/
					frmCancelarPago form = new frmCancelarPago();
					form.CodNota = codigoVenta;
					form.VentComp = compraVen;
					form.tipo = tipo;
					form.CodCliente = codigoCliente;
					form.ShowDialog();
				}
			}
		}

		private void ImprimeEspecial(Int32 lista)
		{
			if (detalle1.Count > 0)
			{
				foreach (clsDetalleFacturaVenta det in detalle1)
				{
					AdmVenta.insertdetalle(det);
					if (det.CodDetalleVenta == 0)
					{
						MessageBox.Show("Error No se puede Registrar los Datos. Falta Stock de Productos");
						AdmVenta.rollback(Convert.ToInt32(venta.CodFacturaVenta), 0);
						return;
					}
				}
			}
			MessageBox.Show("Los datos se guardaron correctamente", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
			txtNumDoc.Text = venta.CodFacturaVenta.PadLeft(11, '0');
			ltaventa.Add(venta);

			if (ncredito.Count > 0)
			{
				frmCancelarPago form = new frmCancelarPago();
				form.CodNota = venta.CodFacturaVenta;
				form.VentComp = 1;
				form.tipo = 3;
				form.CodCliente = cli.CodCliente;
				form.ShowDialog();
			}
			else
			{
				if (fpago.Dias == 0 && venta.CodTipoTransaccion == 7)
				//se comprueba que el pago sea al contado y que la trnasaccion sea ingreso por compra
				{
					ingresarpago();
				}
				CodVenta = venta.CodFacturaVenta;
				//Proceso = 0;
				if (venta.FormaPago != 6)
				{
					btnImprimir.Visible = true;
				}
			}
			//FACTURACION ELECTRONICA
			if (lista != 3)
			{
				conex.GeneraXML(cli, venta, detalle1);
				// contingencia xml - SUNAT
				if (conex.enviado != 0)
				{
					var resultado = conex.respuestaserver;

					if (resultado != null)
					{
						String cad = resultado.Item1;
						String[] codigores;

						codigores = cad.Split('.');

						if (codigores.Length > 0)
						{
							switch (codigores[1])
							{
								case "sunat": conex.enviado = 2; break;
								case "1033": conex.enviado = 2; break;
								case "0130": conex.enviado = 2; break;
								case "0131": conex.enviado = 2; break;
								case "0132": conex.enviado = 2; break;
								case "0133": conex.enviado = 2; break;
								case "0134": conex.enviado = 2; break;
								case "0135": conex.enviado = 2; break;
								case "0136": conex.enviado = 2; break;
								case "0137": conex.enviado = 2; break;
								case "0138": conex.enviado = 2; break;
							}
						}

						Boolean actualiza = AdmVenta.actualizaEstadoEnvio(conex.enviado, Convert.ToInt32(venta.CodFacturaVenta));
						if (actualiza && conex.enviado == 2)
						{
							Boolean actualizaError = AdmVenta.actualizaEstadoEnvioConError(conex.CodigoErrorEnvio, Convert.ToInt32(venta.CodFacturaVenta));
							if (actualizaError && conex.CodigoErrorEnvio == 1)
							{
								AdmVenta.rollback(Convert.ToInt32(venta.CodFacturaVenta), 1);
							}
						}
					}
				}
				firmadigital = conex.LogoEmp;
			}
			fnImprimir();
		}

		private void RecorreDetalleEspecial(Int32 codigo, Int32 codEmpresa)
		{
			detalle.Clear();
			detalle1.Clear();
			if (dgvDetalle.Rows.Count > 0)
			{
				foreach (DataRow row in NuevaTabla.Rows)
				{
					AñadeDetalleEspecial(row, codigo, codEmpresa);
				}
			}
		}

		private void AñadeDetalleEspecial(DataRow row, Int32 codigo, Int32 codEmpresa)
		{
			if (codigo == Convert.ToInt32(row[0]) && codEmpresa == Convert.ToInt32(row[25]))
			{
				clsDetalleFacturaVenta deta = new clsDetalleFacturaVenta();
				deta.CodProducto = Convert.ToInt32(row[1]);
				deta.CodVenta = Convert.ToInt32(venta.CodFacturaVenta);
				deta.CodAlmacen = Convert.ToInt32(row[22]);
				deta.UnidadIngresada = Convert.ToInt32(row[4]);
				deta.SerieLote = "";
				deta.Cantidad = Convert.ToDouble(row[6]);
				deta.PrecioUnitario = Convert.ToDouble(row[7]);
				deta.Subtotal = Convert.ToDouble(row[8]);
				deta.Descuento1 = Convert.ToDouble(row[9]);
				deta.MontoDescuento = Convert.ToDouble(row[12]);
				deta.Igv = Convert.ToDouble(row[14]);
				deta.Importe = Convert.ToDouble(row[15]);
				deta.PrecioReal = Convert.ToDouble(row[17]);
				deta.ValoReal = Convert.ToDouble(row[16]);
				deta.CodUser = frmLogin.iCodUser;
				deta.CantidadPendiente = Convert.ToDouble(row[6]);
				deta.Moneda = 1;
				deta.Descripcion = row[3].ToString();
				deta.CodTipoArticulo = Convert.ToInt32(row[20]);
				deta.Tipoimpuesto = row[21].ToString();
				deta.Entregado = rbtnPendiente.Checked;
				deta.TipoUnidad = Convert.ToInt32(row[24]);
				deta.CodDetalleCotizacion = 0;
				//if (Procede == 4)//pedido
				//{
				deta.CodDetallePedido = 0;
				//}
				//else// venta
				//{
				//    deta.CodDetallePedido = 0;
				//}
				detalle1.Add(deta);
			}
		}

		private void NuevaCabecera(Int32 codbol)
		{
			try
			{
				ser = AdmSerie.CargaSerieEmpresa(venta.CodEmpresa, doc.CodTipoDocumento);
				if (ser == null)
				{

					MessageBox.Show("Faltan series para algunas empresas, Por favor verifique..!");
					return;
				}
				venta.CodSerie = ser.CodSerie;
				venta.Serie = ser.Serie;
				venta.NumDoc = ser.Numeracion.ToString().PadLeft(8, '0');

				if (chkVentaGratuita.Checked) { venta.Tipoventa = 4; }  // venta gratuita
				else if (chkVentaDsctoGlobal.Checked)
				{
					venta.Tipoventa = 5; // venta con descuento Global
				}

				detalle.Clear();
				detalle1.Clear();

				Decimal bruto = 0; Decimal Dscto = 0; Decimal igv = 0; Decimal valor = 0;
				String montoBruto, montodescuento, montoigv, montovv, montotal;
				montogratuitas = 0; montoexoneradas = 0; montogravadas = 0; montoinafectas = 0;
				banderagrabada = false; banderaexonerada = false; banderainafecta = false;



				if (NuevaTabla.Rows.Count > 0)
				{
					foreach (DataRow row in NuevaTabla.Rows)
					{
						if (Convert.ToInt32(row[0]) == codbol)
						{

							if (venta.CodEmpresa == Convert.ToInt32(row[25]))
							{
								bruto = bruto + Convert.ToDecimal(row[15]);
								Dscto = Dscto + Convert.ToDecimal(row[12]);
								valor = valor + Convert.ToDecimal(row[13]);


								if (Convert.ToString(row[21]) == "21") // gratuitas
								{
									montogratuitas = montogratuitas + (Convert.ToDecimal(row[7]) * Convert.ToDecimal(row[6]));
								}

								if (Convert.ToString(row[21]) == "10" || Convert.ToString(row[21]) == "11" ||
									Convert.ToString(row[21]) == "12" || Convert.ToString(row[21]) == "13" ||
									Convert.ToString(row[21]) == "14" || Convert.ToString(row[21]) == "15" ||
									Convert.ToString(row[21]) == "16" || Convert.ToString(row[21]) == "17")   // gravadas
								{
									montogravadas = montogravadas + (Convert.ToDecimal(row[13])); banderagrabada = true;
								}

								if (Convert.ToString(row[21]) == "20") // exoneradas
								{
									montoexoneradas = montoexoneradas + (Convert.ToDecimal(row[7]) * Convert.ToDecimal(row[6]) - Convert.ToDecimal(row[12]));
									banderaexonerada = true;
								}

								if (Convert.ToString(row[21]) == "30" || Convert.ToString(row[21]) == "31" ||
									Convert.ToString(row[21]) == "32" || Convert.ToString(row[21]) == "33" ||
									Convert.ToString(row[21]) == "34" || Convert.ToString(row[21]) == "35" ||
									Convert.ToString(row[21]) == "36") // inafectas
								{
									montoinafectas = montoinafectas + (Convert.ToDecimal(row[7]) * Convert.ToDecimal(row[6]) - Convert.ToDecimal(row[12]));
									banderainafecta = true;
								}
							}
						}
					}

					venta.Gratuitas = montogratuitas;
					venta.Exoneradas = montoexoneradas;
					venta.Gravadas = montogravadas;
					venta.Inafectas = montoinafectas;
					if (chkVentaGratuita.Checked) { venta.Tipoventa = 4; }  // venta gratuita
					else if (chkVentaDsctoGlobal.Checked)
					{
						venta.Tipoventa = 5; // venta con descuento Global
					}
					else if (banderagrabada == true && banderaexonerada == false && banderainafecta == false)
					{
						venta.Tipoventa = 1;  // venta grabada
					}
					else if (banderagrabada == false && banderaexonerada == true && banderainafecta == false)
					{
						venta.Tipoventa = 2;  // venta exonerada
					}
					else if (banderagrabada == false && banderaexonerada == false && banderainafecta == true)
					{
						venta.Tipoventa = 3;  // venta inafecta
					}
					else if (banderagrabada == true && banderaexonerada == true && banderainafecta == false)
					{
						venta.Tipoventa = 6;  // venta grabada + exonerada
					}
					else if (banderagrabada == true && banderaexonerada == false && banderainafecta == true)
					{
						venta.Tipoventa = 7;  // venta grabada + inafecta
					}
					montodescuento = String.Format("{0:#,##0.00}", Dscto);
					montoBruto = String.Format("{0:#,##0.00}", bruto);
					montovv = String.Format("{0:#,##0.00}", valor);
					montoigv = String.Format("{0:#,##0.00}", bruto - Dscto - valor);
					montotal = String.Format("{0:#,##0.00}", bruto - Dscto);

					venta.MontoBruto = Convert.ToDouble(montoBruto);
					venta.MontoDscto = Convert.ToDouble(montodescuento);
					venta.Igv = Convert.ToDouble(montoigv);
					venta.Total = Convert.ToDouble(montotal);
				}


				/* foreach (DataRow row in NuevaTabla.Rows)
                 {
                     if (Convert.ToInt32(row[0]) == codbol)
                     {
                         //vamos armar la cabecera
                     }
                 }*/
			}
			catch (Exception a) { MessageBox.Show(a.Message); }
		}

		private void OrdenarTablaTemporal()
		{
			DataTable t1 = new DataTable();
			t1 = new DataTable("Tabla1");
			foreach (DataGridViewColumn column in dgvDetalle.Columns)
			{
				DataColumn dc = new DataColumn(column.Name.ToString());
				t1.Columns.Add(dc);
			}

			foreach (Int32 cod in ListaEmpresa)
			{
				foreach (DataRow row in NuevaTabla.Rows)
				{
					if (Convert.ToInt32(row[25]) == cod)
					{
						DataRow dr = t1.NewRow();
						for (Int32 i = 0; i < NuevaTabla.Columns.Count; i++)
						{
							dr[i] = row[i];
						}
						t1.Rows.Add(dr);
					}
				}
			}

			NuevaTabla.Clear();
			NuevaTabla = t1;
		}

		private void CrearTablaTemporal()
		{
			NuevaTabla = new DataTable("TablaDetalle");
			// Columnas
			foreach (DataGridViewColumn column in dgvDetalle.Columns)
			{
				DataColumn dc = new DataColumn(column.Name.ToString());
				NuevaTabla.Columns.Add(dc);
			}

			// Datos de la grilla
			for (int i = 0; i < dgvDetalle.Rows.Count; i++)
			{
				DataGridViewRow row = dgvDetalle.Rows[i];
				DataRow dr = NuevaTabla.NewRow();
				for (int j = 0; j < dgvDetalle.Columns.Count; j++)
				{
					dr[j] = (row.Cells[j].Value == null) ? "" : row.Cells[j].Value.ToString();
				}
				NuevaTabla.Rows.Add(dr);
			}
		}

		private void GeneraListaEnpresas()
		{
			Boolean bandera = false;
			ListaEmpresa.Clear();
			foreach (DataGridViewRow row in dgvDetalle.Rows)
			{
				bandera = false;
				if (ListaEmpresa.Count == 0)
				{
					ListaEmpresa.Add(Convert.ToInt32(row.Cells[CodigoEmpresa.Name].Value));
				}
				else
				{
					foreach (Int32 lista in ListaEmpresa)
					{
						if (lista == Convert.ToInt32(row.Cells[CodigoEmpresa.Name].Value))
						{
							bandera = true;
						}
					}

					if (!bandera)
					{
						ListaEmpresa.Add(Convert.ToInt32(row.Cells[CodigoEmpresa.Name].Value));
					}
				}
			}
		}

		private void CreaBoletas(Int32 lista)
		{
			try
			{
				DataTable dt1 = new DataTable();
				Decimal bruto = 0; Decimal Ncantidad = 0; Decimal Nimporte = 0; Decimal valorV = 0;
				item = 1;

				foreach (DataGridViewColumn column in dgvDetalle.Columns)
				{
					DataColumn dc = new DataColumn(column.Name.ToString());
					dt1.Columns.Add(dc);
				}

				foreach (DataRow row in NuevaTabla.Rows)
				{
					if (Convert.ToInt32(row[25]) == lista)
					{
						bruto = bruto + Convert.ToDecimal(row[15]);

						if (bruto <= mdi_Menu.MontoTopeBoleta)
						{
							DataRow dr = dt1.NewRow();
							for (Int32 i = 0; i < NuevaTabla.Columns.Count; i++)
							{
								if (i == 0) { dr[i] = item; } else { dr[i] = row[i]; }
							}
							dt1.Rows.Add(dr);
						}
						else
						{
							bruto = bruto - Convert.ToDecimal(row[15]);
							decimal ValorRestante = mdi_Menu.MontoTopeBoleta - bruto;
							decimal cantidad = Math.Truncate(ValorRestante / Convert.ToDecimal(row[7]));

							decimal cantidadRestante = Convert.ToDecimal(row[6]) - cantidad;

							DataRow dr = dt1.NewRow();
							valorV = (Convert.ToDecimal(row[7]) * (1 - (Convert.ToDecimal(frmLogin.Configuracion.IGV) / 100)));
							bruto = bruto + (Convert.ToDecimal(row[7]) * cantidad);
							for (Int32 j = 0; j < NuevaTabla.Columns.Count; j++)
							{
								if (j == 0) { dr[j] = item; }
								else if (j == 6) { dr[j] = cantidad; }
								else if (j == 8) { dr[j] = (cantidad * Convert.ToDecimal(dr[7])); }
								else if (j == 13) { dr[j] = Math.Round((cantidad * valorV), 4); }
								else if (j == 14) { dr[j] = ((cantidad * Convert.ToDecimal(dr[7])) - Convert.ToDecimal(dr[13])); }
								else if (j == 15) { dr[j] = (cantidad * Convert.ToDecimal(dr[7])); }
								else { dr[j] = row[j]; }
							}
							item = item + 1;
							bruto = 0;
							dt1.Rows.Add(dr);

							if (cantidadRestante > 0)
							{
								Decimal NuevaCantidad = 0; Decimal ultimacantida = 0; Int32 filas = 0;
								if ((cantidadRestante * Convert.ToDecimal(dr[7])) > mdi_Menu.MontoTopeBoleta)
								{
									NuevaCantidad = Math.Truncate(mdi_Menu.MontoTopeBoleta / Convert.ToDecimal(row[7]));
									filas = Convert.ToInt32(Math.Truncate(cantidadRestante / NuevaCantidad));
									if (cantidadRestante > (NuevaCantidad * filas)) { ultimacantida = cantidadRestante - (NuevaCantidad * filas); filas++; }
									else { ultimacantida = NuevaCantidad; }
								}
								else
								{

								}

								for (Int32 k = 1; k <= filas; k++)
								{
									DataRow dr1 = dt1.NewRow();

									if (k == filas)
									{
										NuevaCantidad = ultimacantida;
									}

									bruto = bruto + (NuevaCantidad * Convert.ToDecimal(dr[7]));
									for (Int32 j = 0; j < NuevaTabla.Columns.Count; j++)
									{
										if (j == 0) { dr1[j] = item; }
										else if (j == 6) { dr1[j] = NuevaCantidad; }
										else if (j == 8) { dr1[j] = (NuevaCantidad * Convert.ToDecimal(dr[7])); }
										else if (j == 13) { dr1[j] = (NuevaCantidad * valorV); }
										else if (j == 14) { dr1[j] = ((NuevaCantidad * Convert.ToDecimal(dr[7])) - Convert.ToDecimal(dr1[13])); }
										else if (j == 15) { dr1[j] = (NuevaCantidad * Convert.ToDecimal(dr[7])); }
										else { dr1[j] = row[j]; }
									}
									dt1.Rows.Add(dr1);
									if (k != filas)
									{
										item++;
										bruto = 0;
									}

								}
							}

							//break;
						}
					}


				}

				NuevaTabla.Clear();
				NuevaTabla = dt1;



			}
			catch (Exception e) { MessageBox.Show(e.Message); }
		}

		private void ArmaCabecera(Int32 CodigoE)
		{
			try
			{
				if (CodigoE == 13) { doc.CodTipoDocumento = 7; venta.CodTipoDocumento = 7; } else { doc.CodTipoDocumento = CodDocumento; venta.CodTipoDocumento = CodDocumento; }

				//ser = AdmSerie.CargaSerieEmpresa(CodigoE, doc.CodTipoDocumento);
				ser = AdmSerie.CargaSerieEmpresa(frmLogin.iCodAlmacen, doc.CodTipoDocumento);
				if (ser == null)
				{
					MessageBox.Show("Registre Serie y Correlativos para las Empresas");

					return;
				}
				venta.CodSerie = ser.CodSerie;
				venta.Serie = ser.Serie;
				venta.NumDoc = ser.Numeracion.ToString().PadLeft(8, '0');

				if (chkVentaGratuita.Checked) { venta.Tipoventa = 4; }  // venta gratuita
				else if (chkVentaDsctoGlobal.Checked)
				{
					venta.Tipoventa = 5; // venta con descuento Global
				}

				detalle.Clear();
				detalle1.Clear();

				Decimal bruto = 0; Decimal Dscto = 0; Decimal igv = 0; Decimal valor = 0;
				String montoBruto, montodescuento, montoigv, montovv, montotal;
				montogratuitas = 0; montoexoneradas = 0; montogravadas = 0; montoinafectas = 0;
				banderagrabada = false; banderaexonerada = false; banderainafecta = false;
				if (dgvDetalle.Rows.Count > 0)
				{
					foreach (DataGridViewRow row in dgvDetalle.Rows)
					{

						if (CodigoE == Convert.ToInt32(row.Cells[CodigoEmpresa.Name].Value))
						{
							bruto = bruto + Convert.ToDecimal(row.Cells[importe.Name].Value);
							Dscto = Dscto + Convert.ToDecimal(row.Cells[montodscto.Name].Value);
							valor = valor + Convert.ToDecimal(row.Cells[valorventa.Name].Value);


							if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "21") // gratuitas
							{
								montogratuitas = montogratuitas + (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value));
							}

							if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "10" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "11" ||
								Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "12" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "13" ||
								Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "14" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "15" ||
								Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "16" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "17")   // gravadas
							{
								montogravadas = montogravadas + (Convert.ToDecimal(row.Cells[valorventa.Name].Value)); banderagrabada = true;
							}

							if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "20") // exoneradas
							{
								montoexoneradas = montoexoneradas + (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value) - Convert.ToDecimal(row.Cells[montodscto.Name].Value));
								banderaexonerada = true;
							}

							if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "30" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "31" ||
								Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "32" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "33" ||
								Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "34" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "35" ||
								Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "36") // inafectas
							{
								montoinafectas = montoinafectas + (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value) - Convert.ToDecimal(row.Cells[montodscto.Name].Value));
								banderainafecta = true;
							}
						}
					}

					venta.Gratuitas = montogratuitas;
					venta.Exoneradas = montoexoneradas;
					venta.Gravadas = montogravadas;
					venta.Inafectas = montoinafectas;
					if (chkVentaGratuita.Checked) { venta.Tipoventa = 4; }  // venta gratuita
					else if (chkVentaDsctoGlobal.Checked)
					{
						venta.Tipoventa = 5; // venta con descuento Global
					}
					else if (banderagrabada == true && banderaexonerada == false && banderainafecta == false)
					{
						venta.Tipoventa = 1;  // venta grabada
					}
					else if (banderagrabada == false && banderaexonerada == true && banderainafecta == false)
					{
						venta.Tipoventa = 2;  // venta exonerada
					}
					else if (banderagrabada == false && banderaexonerada == false && banderainafecta == true)
					{
						venta.Tipoventa = 3;  // venta inafecta
					}
					else if (banderagrabada == true && banderaexonerada == true && banderainafecta == false)
					{
						venta.Tipoventa = 6;  // venta grabada + exonerada
					}
					else if (banderagrabada == true && banderaexonerada == false && banderainafecta == true)
					{
						venta.Tipoventa = 7;  // venta grabada + inafecta
					}
					montodescuento = String.Format("{0:#,##0.00}", Dscto);
					montoBruto = String.Format("{0:#,##0.00}", bruto);
					montovv = String.Format("{0:#,##0.00}", valor);
					montoigv = String.Format("{0:#,##0.00}", bruto - Dscto - valor);
					montotal = String.Format("{0:#,##0.00}", bruto - Dscto);

					venta.MontoBruto = Convert.ToDouble(montoBruto);
					venta.MontoDscto = Convert.ToDouble(montodescuento);
					venta.Igv = Convert.ToDouble(montoigv);
					venta.Total = Convert.ToDouble(montotal);
				}
				ActivaCabecera = 1;
			}
			catch (Exception a) { MessageBox.Show(a.Message); }
		}

		public void fnImprimir()
		{
			try
			{
				impresion = AdmVenta.chekeaImpresion(Convert.ToInt32(venta.CodFacturaVenta));
				empress = admempress.CargaEmpresa(venta.CodEmpresa);

				if (impresion == 0)
				{
					PrintaDocumento();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void PrintaDocumento()
		{
			try
			{
				if (frmLogin.iCodAlmacen != 0)
				{
                    String rutaLogo = $"{Program.CarpetaLogosEmpresa}\\{empress.CodEmpresa}.jpg";

                    DataSet jes = new DataSet();
					frmRptFactura frm = new frmRptFactura();
					//CRFacturaInsumos rpt = new CRFacturaInsumos();
					CRFacturaFomatoContinuo rpt = new CRFacturaFomatoContinuo();

					rpt.Load("CRNotaCreditoVenta.rpt");
					jes = ds1.ReporteFactura2(Convert.ToInt32(venta.CodFacturaVenta));

                    LogoEmpresa = CargarImagen(rutaLogo);

                    foreach (DataTable mel in jes.Tables)
					{
						foreach (DataRow changesRow in mel.Rows)
						{
							changesRow["firma"] = firmadigital;
                            changesRow["logo_campo"] = LogoEmpresa;
                        }

						if (mel.HasErrors)
						{
							foreach (DataRow changesRow in mel.Rows)
							{
								if ((int)changesRow["Item", DataRowVersion.Current] > 100)
								{
									changesRow.RejectChanges();
									changesRow.ClearErrors();
								}
							}
						}
					}

					rpt.SetDataSource(jes);
					CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
					rptoption.PrinterName = ser.NombreImpresora;
					rptoption.PaperSize = (CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(ser.NombreImpresora, ser.PaperSize);

					rptoption.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(30, 5, 0, 10));
					rpt.PrintToPrinter(1, false, 1, 1);
					rpt.Close();
					rpt.Dispose();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private Boolean BuscaSerie2()
		{
			ser = AdmSerie.MuestraSerie(CodSerie, frmLogin.iCodAlmacen);
			if (ser != null)
			{
				CodSerie = ser.CodSerie;
				return true;
			}
			else
			{
				CodSerie = 0;
				return false;
			}
		}

		private void ingresarpago()
		{
			VerificaSaldoCaja();
			if (CodigoCaja != 0)
			{
				Pag.CodNota = venta.CodFacturaVenta.ToString();
				Pag.CodLetra = 0;
				Pag.CodTipoPago = 5; //metodo de pago
				Pag.CodMoneda = venta.Moneda;
				Pag.CodCobrador = Convert.ToInt32(frmLogin.iCodUser);
				Pag.Tipo = true;// total o parcial
				Pag.IngresoEgreso = true;//ingreso
				Pag.TipoCambio = Convert.ToDecimal(venta.TipoCambio);
				Pag.MontoPagado = Convert.ToDecimal(venta.Total);
				Pag.MontoCobrado = Convert.ToDecimal(venta.Total);
				Pag.Vuelto = 0;
				Pag.codCtaCte = 0;
				Pag.CtaCte = "";
				Pag.NOperacion = "";
				Pag.NCheque = "";
				Pag.FechaPago = venta.FechaPago.Date;
				Pag.Observacion = "";
				Pag.CodUser = frmLogin.iCodUser;
				Pag.CodAlmacen = frmLogin.iCodAlmacen;
				Pag.CodSucursal = frmLogin.iCodSucursal;
				Pag.CodDoc = venta.CodTipoDocumento;
				Pag.Serie = "";
				Pag.NumDoc = "";
				Pag.Referencia = "";
				Pag.Codcaja = CodigoCaja;
				Pag.CodBanco = 0;
				Pag.CodTarjeta = 0;
				Pag.Aprobado = 4;
				//montoPag = 1;
				if (AdmPagos.insert(Pag))
				{
					//this.Close();
				}
			}
		}

		private void VerificaSaldoCaja()
		{
			try
			{
				Caja = AdmCaja.ValidarAperturaDia(frmLogin.iCodSucursal, DateTime.Now.Date, 1, frmLogin.iCodAlmacen, frmLogin.iCodUser);//1 caja ventas

				if (Caja == null)
				{
					MessageBox.Show("Aperture caja, el pago no se a registrado", "SGE SYSTEM'S", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

				}
				else
				{
					CodigoCaja = Caja.Codcaja;
				}
			}
			catch (Exception a) { MessageBox.Show(a.Message); }
		}

		public Boolean VerificarDetracciones()
		{
			Boolean grav = false;
			Decimal sumadet = 0;
			if (CodDocumento == 2)
			{
				if (dgvDetalle.Rows.Count > 0)
				{
					foreach (DataGridViewRow row in dgvDetalle.Rows)
					{
						if (Convert.ToDecimal(row.Cells[igv.Name].Value) != 0)
						{
							grav = true;
						}
						else
						{
							if (Convert.ToDecimal(row.Cells[precioventa.Name].Value) == 0)
							{
								grav = true;
							}
							else
							{
								sumadet = sumadet + Convert.ToDecimal(row.Cells[precioventa.Name].Value);
							}
						}
					}
				}
				if (sumadet <= 700)
				{
					return true;
				}
				else
				{
					if (grav)
					{
						MessageBox.Show("Operacion no permitida, por estar afecta a detracción!", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return false;
					}
					else
					{
						return true;
					}
				}
			}
			else
			{
				return true;
			}
		}

		private void ValidaCliente(String rucdni)
		{
			cli = AdmCli.ConsultaCliente(rucdni);
			if (cli == null)
			{
				cli = new clsCliente();
				Int32 id = AdmCli.GetUltimoId() + 1;
				if (rucdni.Length == 8) { cli.Dni = rucdni; cli.DireccionEntrega = "S/D"; cli.DireccionLegal = "S/D"; }
				else if (rucdni.Length == 11) { cli.Ruc = rucdni; cli.DireccionEntrega = txtDireccion.Text; cli.DireccionLegal = txtDireccion.Text; }
				cli.Nombre = txtNombreCliente.Text;
				cli.RazonSocial = txtNombreCliente.Text;
				cli.CodigoPersonalizado = "C" + id.ToString().PadLeft(6, '0').Trim();
				cli.FormaPago = 6; //pago ala contado
				cli.Moneda = 1;
				cli.LineaCredito = 0;
				cli.LineaCreditoDisponible = 0;
				cli.Habilitado = true;
				cli.CodUser = frmLogin.iCodUser;

				if (AdmCli.insert(cli)) { CodCliente = cli.CodClienteNuevo; }
			}
		}

		private void CargaPedido()
		{
			try
			{
				pedido = AdmPedido.CargaPedido(Convert.ToInt32(CodPedido));
				if (pedido != null)
				{
					txtCodigoBarras.Text = pedido.CodigoBarrasCifrado;

					if (pedido.Boletafactura == 1) // 1 dni
					{
						chkBoleta.Checked = true;
						chkFactura.Checked = false;
						CodCliente = pedido.CodCliente;
						txtCodCliente.Text = pedido.DNI;

						if (pedido.Nombrecliente != "")
						{
							txtNombreCliente.Text = pedido.Nombrecliente;
						}
						else
						{
							txtNombreCliente.Text = pedido.Nombre;
						}
						// txtNombreCliente.Enabled = false;
						txtDireccion.Text = pedido.Direccion;
						cargadatoscliente(pedido.DNI);
					}
					else
					{
						chkBoleta.Checked = false;
						chkFactura.Checked = true;
						CodCliente = pedido.CodCliente;
						cli.CodCliente = CodCliente;
						txtCodCliente.Text = pedido.RUCCliente;
						if (pedido.RazonSocialCliente != "") txtNombreCliente.Text = pedido.RazonSocialCliente;
						else txtNombreCliente.Text = pedido.Nombre;
						txtDireccion.Text = pedido.Direccion;
						cargadatoscliente(pedido.RUCCliente);
					}

					dtpFecha.Value = pedido.FechaPedido;

					txtBruto.Text = String.Format("{0:#,##0.00}", pedido.MontoBruto);
					txtDscto.Text = String.Format("{0:#,##0.00}", pedido.MontoDscto);
					txtValorVenta.Text = String.Format("{0:#,##0.00}", pedido.Total - pedido.Igv);
					txtIGV.Text = String.Format("{0:#,##0.00}", pedido.Igv);
					txtPrecioVenta.Text = String.Format("{0:#,##0.00}", pedido.Total);
					montogravadas = pedido.Gravadas;
					montoexoneradas = pedido.Exoneradas;
					montoinafectas = pedido.Inafectas;
					montogratuitas = pedido.Gratuitas;
					label11.Text = txtPrecioVenta.Text;
					cbovendedor.SelectedValue = pedido.CodVendedor;
					montosventa();
					if (pedido.Tipoventa == 4) { chkVentaGratuita.Checked = true; }
					else if (pedido.Tipoventa == 5) { chkVentaDsctoGlobal.Checked = true; }

					CargaDetallePedido(Convert.ToInt32(pedido.CodPedido));
					PedidosIngresados.Add(pedido);

					lblCantidadProductos.Text = "Productos Agregados : " + dgvDetalle.RowCount;

					/*
					 * establecer la forma de pago del pedido
					 */
					cmbFormaPago.SelectedValue = pedido.FormaPago;

				}
				else
				{
					MessageBox.Show("El documento solicitado no existe", "Nota de Ingreso", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		List<Int32> ListaEmpresa = new List<int>();
		private void CargaDetallePedido(Int32 CodPedido)
		{
			DataTable newData = new DataTable();
			dgvDetalle.Rows.Clear();
			try
			{
				newData = AdmPedido.CargaDetalle(CodPedido);
				foreach (DataRow row in newData.Rows)
				{
					if (Convert.ToInt32(row[26].ToString()) == 1 || Convert.ToInt32(row[26].ToString()) == 0)
					{
						dgvDetalle.Rows.Add(row[0].ToString(), row[1].ToString(), row[2].ToString(), row[3].ToString(), row[4].ToString(),
												row[5].ToString(), row[6].ToString(), row[7].ToString(), row[8].ToString(), row[9].ToString(),
												row[10].ToString(), row[11].ToString(), row[12].ToString(), row[13].ToString(), row[14].ToString(),
												row[15].ToString(), row[16].ToString(), row[17].ToString(), row[18].ToString(), row[19].ToString(),
												row[20].ToString(), row[21].ToString(), row[22].ToString(), row[23].ToString(), row[24].ToString(),
												row[25].ToString(), row[26].ToString(), row[27].ToString(), row[28].ToString(), row[29].ToString(),
												row[30].ToString(), row[31].ToString(), row[32].ToString());
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void RecorreDetalle(Int32 codigo)
		{
			detalle.Clear();
			detalle1.Clear();
			if (dgvDetalle.Rows.Count > 0)
			{
				foreach (DataGridViewRow row in dgvDetalle.Rows)
				{
					añadedetalle(row, codigo);
				}
			}
		}
		private void añadedetalle(DataGridViewRow fila, Int32 cod)
		{
			if (cod == Convert.ToInt32(fila.Cells[CodigoEmpresa.Name].Value))
			{
				clsDetalleFacturaVenta deta = new clsDetalleFacturaVenta();
				deta.CodProducto = Convert.ToInt32(fila.Cells[codproducto.Name].Value);
				deta.CodVenta = Convert.ToInt32(venta.CodFacturaVenta);
				deta.CodAlmacen = frmLogin.iCodAlmacen;//Convert.ToInt32(fila.Cells[codalmacen.Name].Value);
				deta.UnidadIngresada = Convert.ToInt32(fila.Cells[codunidad.Name].Value);
				deta.SerieLote = "";
				deta.Cantidad = Convert.ToDouble(fila.Cells[cantidad.Name].Value);
				deta.PrecioUnitario = Convert.ToDouble(fila.Cells[preciounit.Name].Value);
				deta.Subtotal = Convert.ToDouble(fila.Cells[importe.Name].Value);
				deta.Descuento1 = Convert.ToDouble(fila.Cells[dscto1.Name].Value);
				deta.MontoDescuento = Convert.ToDouble(fila.Cells[montodscto.Name].Value);
				deta.Igv = Convert.ToDouble(fila.Cells[igv.Name].Value);
				deta.Importe = Convert.ToDouble(fila.Cells[precioventa.Name].Value);
				deta.PrecioReal = Convert.ToDouble(fila.Cells[precioreal.Name].Value);
				deta.ValoReal = Convert.ToDouble(fila.Cells[valoreal.Name].Value);
				deta.CodUser = frmLogin.iCodUser;
				deta.CantidadPendiente = Convert.ToDouble(fila.Cells[cantidad.Name].Value);
				deta.Moneda = 1;
				deta.Descripcion = fila.Cells[descripcion.Name].Value.ToString();
				//deta.CodTipoArticulo = Convert.ToInt32(fila.Cells[Tipoarticulo.Name].Value);
				deta.Tipoimpuesto = fila.Cells[Tipoimpuesto.Name].Value.ToString();
				deta.Entregado = rbtnPendiente.Checked;
				deta.TipoUnidad = Convert.ToInt32(fila.Cells[TipoUnidad.Name].Value);
				deta.CodDetalleCotizacion = 0;
				//if (Procede == 4)//pedido
				//{
				deta.CodDetallePedido = Convert.ToInt32(fila.Cells[coddetalle.Name].Value);
				//}
				//else// venta
				//{
				//    deta.CodDetallePedido = 0;
				//}
				deta.CodFamilia = Convert.ToInt32(fila.Cells[codFamilia.Name].Value);
				deta.SerieMotor = fila.Cells[serie_motor.Name].Value.ToString();
				deta.NroChasis = fila.Cells[nrochasis.Name].Value.ToString();
				deta.Modelo = fila.Cells[modelo.Name].Value.ToString();
				deta.Marca = fila.Cells[Marca.Name].Value.ToString();
				deta.Color = fila.Cells[color.Name].Value.ToString();
				detalle1.Add(deta);
			}
		}
		public Boolean verificarPrecioVenta()
		{
			Boolean valor = false;
			if (Convert.ToDecimal(txtPrecioVenta.Text) >= 700 && CodCliente == 1)
			{
				MessageBox.Show("Precio venta > S/. 700, registrar(DNI, datos completos del cliente) o seleccionar cliente para guardar pedido", "Advertencia",
					MessageBoxButtons.OK, MessageBoxIcon.Warning);
				if (Application.OpenForms["frmClientesLista"] != null)
				{
					Application.OpenForms["frmClientesLista"].Activate();
				}
				else
				{
					frmClientesLista form = new frmClientesLista();
					form.Proceso = 3;
					form.ShowDialog();
					txtCodCliente.Text = "";
					txtDireccion.Text = "";
					txtNombreCliente.Text = "";
					if (form.exit)
					{
						cli = form.cli;
						CodCliente = cli.CodCliente;
						if (CodCliente != 0)
						{
							NombreCliente = cli.Nombre;
							CargaCliente();
							valor = true;
							ProcessTabKey(true);
						}
					}
					else
					{
						txtCodCliente.Focus();
						valor = false;
					}
				}
			}
			else
			{
				valor = true;
			}
			return valor;
		}

		/*clsAdmNotaSalida AdmNota = new clsAdmNotaSalida();
        public Boolean VerificarStockDisponible()
        {
            try
            {
                Boolean valor = false;

                foreach (DataGridViewRow row in dgvDetalle.Rows)
                {

                    Decimal x = AdmNota.VerificarStock(Convert.ToInt32(row.Cells[codproducto.Name].Value), frmLogin.iCodAlmacen, 0);

                    if (Convert.ToDecimal(row.Cells[cantidad.Name].Value) <= x)
                    {
                        valor = true;
                    }

                    else
                    {
                        valor = false;
                        break;
                    }
                }

                return valor;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message.ToString());
                return false;
            }
        } */

		#region super valideitor
		private void customValidator1_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
		{
			if (Proceso != 0)
				if (e.ControlToValidate.Text != "")
					e.IsValid = true;
				else
					e.IsValid = false;
			else
				e.IsValid = true;
		}

		private void customValidator2_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
		{
			if (Proceso != 0)
				if (e.ControlToValidate.Text != "")
					e.IsValid = true;
				else
					e.IsValid = false;
			else
				e.IsValid = true;
		}

		private void customValidator3_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
		{
			if (Proceso != 0)
				if (e.ControlToValidate.Text != "")
					e.IsValid = true;
				else
					e.IsValid = false;
			else
				e.IsValid = true;
		}

		private void customValidator4_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
		{
			if (Proceso != 0)
				if (e.ControlToValidate.Text != "")
					e.IsValid = true;
				else
					e.IsValid = false;
			else
				e.IsValid = true;
		}

		private void customValidator5_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
		{
			if (Proceso != 0)
				if (e.ControlToValidate.Text != "")
					e.IsValid = true;
				else
					e.IsValid = false;
			else
				e.IsValid = true;
		}

		private void customValidator6_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
		{
			if (Proceso != 0)
				if (e.ControlToValidate.Text != "")
					e.IsValid = true;
				else
					e.IsValid = false;
			else
				e.IsValid = true;
		}

		private void customValidator7_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
		{
			if (Proceso != 0)
				if (e.ControlToValidate.Text != "")
					e.IsValid = true;
				else
					e.IsValid = false;
			else
				e.IsValid = true;
		}

		#endregion super valideitor

		private void chkBoleta_Click(object sender, EventArgs e)
		{
			chkBoleta.Checked = true;
			chkFactura.Checked = false;
		}

		private void chkFactura_Click(object sender, EventArgs e)
		{
			chkBoleta.Checked = false;
			chkFactura.Checked = true;
		}

		private void frmGeneraVenta_Load(object sender, EventArgs e)
		{
			CargaVendedores();
			CargaFormaPagos();
			if (Proceso == 4)
			{
				CargaPedido();
				CargaDocumentoIdentidad();
				btnInicioOV.Focus();
				btnInicioOV.Select();
			}
			else if (Proceso == 3)
			{
				CargaVenta();
				sololectura(true);
				groupBox3.Visible = false;
				btnImprimir.Visible = true;
				button1.Visible = true;
			}
		}

		private void CargaVendedores()
		{
			cbovendedor.DataSource = AdmUs.CargaUsuarios();
			cbovendedor.DisplayMember = "vendedor";
			cbovendedor.ValueMember = "codUsuario";
			cbovendedor.SelectedIndex = -1;
		}

		private void CargaDocumentoIdentidad()
		{
			Int32 codigoTipoDocumento = (pedido.Boletafactura == 1) ? 1 : 2;
			cmbDocumentoIdentidad.DataSource = AdmDocumentoIdentidad.ListaDocumentoIdentidad(codigoTipoDocumento);
			cmbDocumentoIdentidad.DisplayMember = "descripcion";
			cmbDocumentoIdentidad.ValueMember = "codDocumentoIdentidad";
		}

		private void button1_Click(object sender, EventArgs e)
		{
			try
			{
				//printaOV();
			}
			catch (Exception a) { MessageBox.Show(a.Message); }
		}

		private void CargaVenta()
		{
			try
			{
				venta = AdmVenta.CargaFacturaVenta(Convert.ToInt32(CodVenta));
				ser = AdmSerie.MuestraSerie(venta.CodSerie, frmLogin.iCodAlmacen);

				if (venta != null)
				{
					txtNumDoc.Text = venta.CodFacturaVenta;
					CodTransaccion = venta.CodTipoTransaccion;
					CargaTransaccion();

					if (txtCodCliente.Enabled)
					{
						CodCliente = venta.CodCliente;
						cli = AdmCli.MuestraCliente(CodCliente);
						txtCodCliente.Text = venta.DNI;
						txtNombreCliente.Text = venta.RazonSocialCliente;
						txtDireccion.Text = venta.Direccion;
						txtLineaCredito.Text = cli.LineaCredito.ToString();
						txtLineaCreditoDisponible.Text = cli.LineaCreditoDisponible.ToString();
						txtLineaCreditoUso.Text = cli.LineaCreditoUsado.ToString();
					}
					dtpFecha.Value = venta.FechaSalida;

					CodDocumento = venta.CodTipoDocumento;
					//txtCodDocumento.Text = CodDocumento.ToString();
					txtDocRef.Text = venta.SiglaDocumento;
					txtSerie.Text = venta.Serie;
					if (Procede != 4) txtPedido.Text = venta.NumDoc;
					//else txtPedido.Text = numSerie;

					if (cbovendedor.Enabled)
					{
						if (venta.CodVendedor != 0)
						{
							cbovendedor.SelectedValue = venta.CodVendedor;
						}
					}
					cmbFormaPago.SelectedValue = venta.FormaPago;
					cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, null);
					dtpFechaPago.Value = venta.FechaPago;
					txtComentario.Text = venta.Comentario;
					txtBruto.Text = String.Format("{0:#,##0.00}", venta.MontoBruto);
					txtDscto.Text = String.Format("{0:#,##0.00}", venta.MontoDscto);
					txtValorVenta.Text = String.Format("{0:#,##0.00}", venta.Total - venta.Igv);
					txtIGV.Text = String.Format("{0:#,##0.00}", venta.Igv);
					txtPrecioVenta.Text = String.Format("{0:#,##0.00}", venta.Total);
					CargaDetalle();
				}
				else
				{
					MessageBox.Show("El documento solicitado no existe", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				return;
			}
		}

		private void CargaDetalle()
		{
			dgvDetalle.DataSource = AdmVenta.CargaDetalle(Convert.ToInt32(venta.CodFacturaVenta), frmLogin.iCodAlmacen);
			btnImprimir.Visible = true; btnImprimir.Focus(); btnImprimir.Select();
		}

		private void CargaTransaccion()
		{
			tran = AdmTran.MuestraTransaccion(CodTransaccion);
			tran.Configuracion = AdmTran.MuestraConfiguracion(tran.CodTransaccion);
			txtTransaccion.Text = tran.Sigla;
			lbNombreTransaccion.Text = tran.Descripcion;
			lbNombreTransaccion.Visible = true;
			foreach (Control t in groupBox1.Controls)
			{
				if (t.Tag != null)
				{
					if (t.Tag != "")
					{
						Int32 con = Convert.ToInt32(t.Tag);
						if (tran.Configuracion.Contains(con))
						{
							t.Visible = true;
						}
						else
						{
							t.Visible = false;
						}
					}
				}
			}
		}

		private void sololectura(Boolean estado)
		{
			dtpFecha.Enabled = !estado;

			txtCodCliente.ReadOnly = estado;
			txtDocRef.ReadOnly = estado;
			txtPedido.ReadOnly = estado;
			txtBruto.ReadOnly = estado;
			txtDscto.ReadOnly = estado;
			txtValorVenta.ReadOnly = estado;
			txtIGV.ReadOnly = estado;
			txtPrecioVenta.ReadOnly = estado;
			btnInicioOV.Visible = !estado;
			btnAddOV.Visible = !estado;
			btnDeleteItem.Visible = !estado;
			btnGuardaVenta.Visible = !estado;
			btnSalir.Visible = estado;
			btnSalir.Enabled = estado;

			groupBox4.Enabled = true;

		}

		private void btnInicioOV_Click(object sender, EventArgs e)
		{
			this.activaPaneles(true);
			txtSerie.Focus();
			txtSerie.SelectAll();
		}

		//private void btnImprimirTicket_Click(object sender, EventArgs e)
		//{
		//    CRCodigodeBarras rpt = new CRCodigodeBarras();
		//    frmCodigobarra frm = new frmCodigobarra();          
		//    rpt.SetDataSource(ds.CodigoBarras(Convert.ToInt32(CodPedido), frmLogin.iCodAlmacen));
		//    frm.crystalReportViewer1.ReportSource = rpt;
		//    frm.Show();
		//}


		private void cargadatoscliente(String rucdni)
		{
			if (rucdni.Length == 11)
			{
				txtDocRef.Text = "FT";
				KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
				txtDocRef_KeyPress(txtDocRef, ee);
				txtSerie_KeyPress(txtDocRef, ee);

			}
			else if (rucdni.Length == 8 || (rucdni.Length >= 12 && rucdni.Length <= 15))
			{

				txtDocRef.Text = "BV";
				KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
				txtDocRef_KeyPress(txtDocRef, ee);
				txtSerie_KeyPress(txtDocRef, ee);

			}

			/*
			 * lineas anteriores
			 */
			cli = AdmCli.ConsultaCliente(rucdni);
			cli.RucDni = rucdni;

			cli = AdmCli.MuestraCliente(CodCliente);

			if (cli.Moneda == 1)
			{
				txtLineaCredito.Text = String.Format("{0:#,##0.00}", cli.LineaCredito);
				txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoDisponible);
				txtLineaCreditoUso.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoUsado);
				txttasa.Text = String.Format("{0:#,##0.00}", cli.Tasa);
				lbLineaCredito.Text = "Línea de Crédito (S/.):";
				label23.Text = "Línea Disponible (S/.):";
				label25.Text = "Línea C. en Uso (S/.):";
				if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
			}
			else
			{
				txtLineaCredito.Text = String.Format("{0:#,##0.00}", (cli.LineaCredito / Convert.ToDecimal(mdi_Menu.tc_hoy)));
				txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoDisponible / Convert.ToDecimal(mdi_Menu.tc_hoy)));
				txtLineaCreditoUso.Text = Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoUsado / Convert.ToDecimal(mdi_Menu.tc_hoy)));
				lbLineaCredito.Text = "Línea de Crédito ($.):";
				label23.Text = "Línea Disponible ($.):";
				label25.Text = "Línea C. en Uso ($.):";
				if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
			}


			if (cli.FormaPago != 0)
			{
				if (Proceso == 4)
				{
					cmbFormaPago.SelectedValue = pedido.FormaPago;
				}
				else
				{
					cmbFormaPago.SelectedValue = cli.FormaPago;
				}
				EventArgs ee = new EventArgs();
				cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
			}
			else
			{
				dtpFechaPago.Value = DateTime.Today;
			}
			/*
            if (cli.CodVendedor != 0)
            {

                cbovendedor.SelectedValue = cli.CodVendedor;
                txtvendedor.Text = cbovendedor.Text;
            }
           */
		}

		private void txtSerie_KeyPress(object sender, KeyPressEventArgs e)
		{
			ok.enteros(e);
			if (e.KeyChar == (char)Keys.Return)
			{
				if (BuscaSerie())
				{
					txtSerie.Text = ser.Serie.ToString();
					if (ser.PreImpreso)
					{
						txtPedido.Visible = true;
						txtPedido.Enabled = false;
						txtPedido.Focus();
						txtPedido.Text = "";
					}
					else
					{
						txtPedido.Text = "";
						txtPedido.Enabled = false;
						txtPedido.Text = ser.Numeracion.ToString().PadLeft(8, '0');
						txtPedido.Visible = false;
					}

				}
			}
			if (e.KeyChar == (char)Keys.Enter)
			{
				cmbFormaPago.Focus();
			}
		}

		private Boolean BuscaSerie()
		{
			ser = AdmSerie.BuscaSeriexDocumento(CodDocumento, frmLogin.iCodAlmacen);
			if (ser != null)
			{
				CodSerie = ser.CodSerie;
				return true;
			}
			else
			{
				CodSerie = 0;
				return false;
			}
		}

		private void txtTransaccion_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)Keys.Return)
			{
				if (txtTransaccion.Text != "")
				{
					if (BuscaTransaccion())
					{
						btnInicioOV.Focus();
						btnInicioOV.Select();
					}
					else
					{
						MessageBox.Show("Codigo de transacción no existe, Presione F1 para consultar la tabla de ayuda", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
		}

		private Boolean BuscaTransaccion()
		{
			tran = AdmTran.MuestraTransaccionS(txtTransaccion.Text, 1);
			if (tran != null)
			{
				CodTransaccion = tran.CodTransaccion;
				tran.Configuracion = AdmTran.MuestraConfiguracion(tran.CodTransaccion);
				txtTransaccion.Text = tran.Sigla;
				lbNombreTransaccion.Text = tran.Descripcion;
				lbNombreTransaccion.Visible = true;
				foreach (Control t in groupBox1.Controls)
				{
					if (t.Tag != null && t.Tag != "")
					{
						Int32 con = Convert.ToInt32(t.Tag);
						if (tran.Configuracion.Contains(con))
						{
							t.Visible = true;
						}
						else
						{
							t.Visible = false;
						}
					}
				}
				return true;
			}
			else
			{
				lbNombreTransaccion.Text = "";
				lbNombreTransaccion.Visible = false;
				foreach (Control t in groupBox1.Controls)
				{
					if (t.Tag != null)
					{
						t.Visible = false;
					}
				}
				return false;
			}
		}

		private void btnImprimir_Click(object sender, EventArgs e)
		{
			try
			{
				if (frmLogin.iCodAlmacen != 0)
				{
					DataSet jes = new DataSet();
					frmRptFactura frm = new frmRptFactura();
					CRFacturaFomatoContinuo rpt = new CRFacturaFomatoContinuo();

					String nombrearchivo = "";

					venta = AdmVenta.BuscaFacturaVenta(Convert.ToInt32(venta.CodFacturaVenta), frmLogin.iCodAlmacen);
					if (venta.CodTipoDocumento == 1)
					{
						if (venta.Moneda == 1)
						{
							switch (venta.Tipoventa)
							{
								case 1:  // gravada
									nombrearchivo = frmLogin.RUC + "-03-B" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
									break;
								case 2: // exonerada
									nombrearchivo = frmLogin.RUC + "-03-B" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
									break;
								case 3: // inafecta
									nombrearchivo = frmLogin.RUC + "-03-B" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
									break;
								case 4: // gratuita
									nombrearchivo = frmLogin.RUC + "-03-B" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
									break;
								case 5: // descuento
									nombrearchivo = frmLogin.RUC + "-03-B" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
									break;
							}
						}
						else
						{
							nombrearchivo = frmLogin.RUC + "-03-B" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
						}
					}
					else if (venta.CodTipoDocumento == 2)
					{
						if (venta.Moneda == 1)
						{
							switch (venta.Tipoventa)
							{
								case 1:  // gravada
									nombrearchivo = frmLogin.RUC + "-01-F" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
									break;
								case 2:  // exonerada
									nombrearchivo = frmLogin.RUC + "-01-F" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
									break;
								case 3:  // inafecta
									nombrearchivo = frmLogin.RUC + "-01-F" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
									break;
								case 4:  // gratuita
									nombrearchivo = frmLogin.RUC + "-01-F" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
									break;
								case 5: // descuento
									nombrearchivo = frmLogin.RUC + "-01-F" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
									break;
							}
						}
						else
						{
							nombrearchivo = frmLogin.RUC + "-01-F" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
						}
					}
					firmadigital = CargarImagen(@"C:\DOCUMENTOS-" + frmLogin.RUC + "\\CERTIFIK\\QR\\" + nombrearchivo + ".jpeg");
					rpt.Load("CRFacturaFomatoContinuo.rpt");
					jes = ds1.ReporteFactura2(Convert.ToInt32(venta.CodFacturaVenta));
					foreach (DataTable mel in jes.Tables)
					{
						foreach (DataRow changesRow in mel.Rows)
						{

							changesRow["firma"] = firmadigital;
						}
						if (mel.HasErrors)
						{
							foreach (DataRow changesRow in mel.Rows)
							{
								if ((int)changesRow["Item", DataRowVersion.Current] > 100)
								{
									changesRow.RejectChanges();
									changesRow.ClearErrors();
								}
							}
						}
					}
					rpt.SetDataSource(jes);
					CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
					rptoption.PrinterName = ser.NombreImpresora;
					rptoption.PaperSize = (CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(ser.NombreImpresora, ser.PaperSize);

					//rptoption.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(50, 0, 0, 0));
					rptoption.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(50, 5, 0, 0));
					rpt.PrintToPrinter(1, false, 1, 1);
					rpt.Close();
					rpt.Dispose();
				}

			}
			catch (Exception ex)
			{
				MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public static Byte[] CargarImagen(string rutaArchivo)
		{
			if (rutaArchivo != "")
			{
				try
				{
					FileStream Archivo = new FileStream(rutaArchivo, FileMode.Open);//Creo el archivo
					BinaryReader binRead = new BinaryReader(Archivo);//Cargo el Archivo en modo binario
					Byte[] imagenEnBytes = new Byte[(Int64)Archivo.Length]; //Creo un Array de Bytes donde guardare la imagen
					binRead.Read(imagenEnBytes, 0, (int)Archivo.Length);//Cargo la imagen en el array de Bytes
					binRead.Close();
					Archivo.Close();
					return imagenEnBytes;//Devuelvo la imagen convertida en un array de bytes
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
					return new Byte[0];
				}
			}
			return new byte[0];
		}

		private void btnAddOV_Click(object sender, EventArgs e)
		{
			if (bandera)
			{
				if (!string.IsNullOrEmpty(txtAddOV.Text))
				{
					String CodPedido = txtAddOV.Text;
					clsPedido PedidoObtenido = AdmPedido.CargaPedido(Convert.ToInt32(CodPedido));
					pedido = PedidoObtenido;
					if (PedidoObtenido != null)
					{
						txtAddOV.Text = string.Empty;
						if (PedidoObtenido.Pendiente == 1)
						{
							clsCliente ClienteDeVenta = AdmCli.ConsultaCliente(txtCodCliente.Text);
							if (PedidoObtenido.CodCliente == ClienteDeVenta.CodCliente)
							{
								if (PedidosIngresados.Count > 0)
								{
									clsPedido PedidoEncontrado = (from ped in PedidosIngresados where ped.CodPedido.Equals(PedidoObtenido.CodPedido) select ped).SingleOrDefault();
									if (PedidoEncontrado != null) PedidosIngresados.Remove(PedidoEncontrado);
								}
								PedidosIngresados.Add(PedidoObtenido);
								foreach (clsPedido ped in PedidosIngresados)
								{
									montogravadas += ped.Gravadas;
									montoexoneradas += ped.Exoneradas;
									montoinafectas += ped.Inafectas;
									montogratuitas += ped.Gratuitas;
									montosventa();
									if (ped.Tipoventa == 4) { chkVentaGratuita.Checked = true; }
									else if (ped.Tipoventa == 5) { chkVentaDsctoGlobal.Checked = true; }

									CargaDetallePedido(Convert.ToInt32(ped.CodPedido));

								}
								calculatotales();
								MessageBox.Show("PEDIDO AGREGADO", "SGE SYSTEM'S", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							else
							{
								MessageBox.Show("ESTE PEDIDO NO CORRESPONDE A ESTE USUARIO", "SGE SYSTEM'S", MessageBoxButtons.OK, MessageBoxIcon.Error);
							}
						}
						else
						{
							MessageBox.Show("ESTE PEDIDO DE VENTA YA HA SIDO FACTURADO", "SGE SYSTEM'S", MessageBoxButtons.OK, MessageBoxIcon.Error);
							this.txtAddOV.Text = string.Empty;
							return;
						}
					}
					else
					{
						MessageBox.Show("EL PEDIDO INGRESADO NO EXISTE", "SGE SYSTEM'S", MessageBoxButtons.OK, MessageBoxIcon.Error);
						return;
					}
				}
				else
				{
					MessageBox.Show("INGRESE EL CODIGO DEL PEDIDO DE VENTA", "SGE SYSTEM'S", MessageBoxButtons.OK, MessageBoxIcon.Error);
					txtAddOV.Focus();
				}
			}
		}

		private void txtCodCliente_KeyUp(object sender, KeyEventArgs e)
		{
			try
			{
				if (e.KeyCode == Keys.F1)
				{
					if (Application.OpenForms["frmClientesLista"] != null)
					{
						Application.OpenForms["frmClientesLista"].Activate();
					}
					else
					{
						frmClientesLista form = new frmClientesLista();
						form.Proceso = 3;
						form.ShowDialog();
						cli = form.cli;
						CodCliente = cli.CodCliente;
						if (CodCliente != 0)
						{
							txtCodCliente.Text = "";
							txtDireccion.Text = "";
							txtNombreCliente.Text = "";
							NombreCliente = cli.Nombre; CargaCliente(); //ProcessTabKey(true); 
							this.LimpiarDataGridView(this.dgvDetalle);
							this.PedidosIngresados = new List<clsPedido>();
						}
					}
				}
			}
			catch (Exception a) { MessageBox.Show(a.Message); }

		}

		private void LimpiarDataGridView(DataGridView DGVALimpiar)
		{
			if (DGVALimpiar.DataSource != null)
			{
				DGVALimpiar.DataSource = null;
			}
			else
			{
				DGVALimpiar.Rows.Clear();
			}
		}

		private void btnInicioOV_Click_1(object sender, EventArgs e)
		{
			this.activaPaneles(true);
		}

		private void btnDeleteItem_Click_1(object sender, EventArgs e)
		{
			btnDeleteItem_Click(sender, e);
			/*
            if (bandera)
            {
                String CodPedido = txtAddOV.Text;
                clsPedido PedidoObtenido = AdmPedido.CargaPedido(Convert.ToInt32(CodPedido));
                if (PedidosIngresados.Count > 0)
                {
                    clsPedido PedidoEncontrado = (from ped in PedidosIngresados where ped.CodPedido.Equals(PedidoObtenido.CodPedido) select ped).SingleOrDefault();
                    if (PedidoEncontrado != null)
                    {
                        PedidosIngresados.Remove(PedidoEncontrado);
                        foreach (clsPedido Ped in PedidosIngresados)
                        {
                            this.CargaDetallePedido(Convert.ToInt32(Ped.CodPedido));
                        }
                    }
                    else
                    {
                        MessageBox.Show("EL PEDIDO INGRESADO NO EXISTE", "SGE SYSTEM'S", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("NO HAS AGREGADO NINGUN PEDIDO", "SGE SYSTEM'S", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }*/
		}

		private void txtAddOV_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
			{
				e.Handled = true;
			}
		}
	}
}
