﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using SIGEFA.Administradores;
using SIGEFA.Entidades;

namespace SIGEFA.Formularios
{
    public partial class frmProductosLista : DevComponents.DotNetBar.Office2007Form
    {
        public List<clsProducto> prodvendidos;
        clsAdmProducto AdmProd = new clsAdmProducto();
        public Boolean consultorext;
        clsAdmProducto AdmPro = new clsAdmProducto();
        clsAdmTipoArticulo AdmTip = new clsAdmTipoArticulo();
        public clsProducto pro = new clsProducto();
        public Int32 CodLista = 0, codproveedor = 0;
        public Boolean bvalorventa = false;
        public Int32 Proceso = 0; //(1) Ingreso (2)Salida (3)Relacion (4)Guia
        public Int32 Procede = 0; //(1)Nota de Salida (2)Venta       
        public Int32 Moneda = 0;
        public Double tc = 0;
        public Int32 codtrans = 0;
        public static BindingSource data = new BindingSource();
        String filtro = String.Empty;
        public List<Int32> seleccion = new List<Int32>();
        public List<clsDetalleNotaIngreso> productoscargados = new List<clsDetalleNotaIngreso>();// relacion de los productos que ya han sido cargado en la nota de ingreso
        public List<clsDetalleFacturaVenta> productosfactura = new List<clsDetalleFacturaVenta>();// relacion de los productos seleccionados para la venta
        public List<clsDetalleCotizacion> productoscotizacion = new List<clsDetalleCotizacion>();// relacion de los productos seleccionados para la cotizacion
        public List<clsDetalleNotaSalida> productosNotaSalida = new List<clsDetalleNotaSalida>();
        public Int32 codalmacen = 0;

        public Int32 codigoPro, alma = 0;
        public String referenciaPro, descripcionPro;
        public Int32 Tipo = 0;
        public Int32 CodVendedor;

        public frmProductosLista()
        {
            InitializeComponent();
        }

        public int GetCodigoProducto()
        {
            try
            {
                return Convert.ToInt32(dgvProductos.CurrentRow.Cells[referencia.Name].Value);
            }
            catch (Exception ex) { return 0; this.Close(); }
        }

        private void frmProductosLista_Load(object sender, EventArgs e)
        {
            //en teoria sirve para que la carga del DGV sea más liviana
            dgvProductos.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            dgvProductos.AutoGenerateColumns = false;
            dgvProductos.ColumnHeadersDefaultCellStyle.Font = new Font("Segoe UI Semibold", 8);
            // set it to false if not needed
            dgvProductos.RowHeadersVisible = false;
            CargaTipoArticulos();
            CargaLista(Procede);
            lblCantidadProductos.Text = lblCantidadProductos.Text + " " + dgvProductos.RowCount;
            label2.Text = "Descripcion";
            label3.Text = "descripcion";
        }

        private void dgvProductos_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
        }

        private void CargaLista(Int32 proce)
        {
            if (proce == 6 || proce == 8 || proce == 10)//6) nota de ingreso por compra rapida, 8)orden compra 10)Nota de ingreso por orden compra
            {
                dgvProductos.DataSource = data;
                data.DataSource = AdmPro.RelacionIngresoPorProveedor(Convert.ToInt32(cbTipoArticulo.SelectedValue), frmLogin.iCodAlmacen, codproveedor);
                DepurarLista();
                data.Filter = String.Empty;
                filtro = String.Empty;
                dgvProductos.ClearSelection();
                stockdisponible.Visible = false;
                preciooferta.Visible = false;
                precioventa.Visible = false;
                preciodolares.Visible = false;
                preciosoles.Visible = false;
                Procede = 3;
            }
            else if (proce == 7)// 7)nota de credito
            {
                dgvProductos.DataSource = data;
                data.DataSource = AdmPro.RelacionIngreso(Convert.ToInt32(cbTipoArticulo.SelectedValue), frmLogin.iCodAlmacen);
                DepurarLista();
                data.Filter = String.Empty;
                filtro = String.Empty;
                dgvProductos.ClearSelection();
                stockdisponible.Visible = false;
                preciooferta.Visible = false;
                precioventa.Visible = false;
            }
            else if (proce == 4)//Cotizacion
            {
                //cbTipoArticulo.SelectedValue = 1;//selecciona MERCADERIAS
                dgvProductos.DataSource = data;
                data.DataSource = AdmPro.RelacionCotizacion(Convert.ToInt32(cbTipoArticulo.SelectedValue), frmLogin.iCodAlmacen, CodLista);
                DepurarLista3();
                data.Filter = String.Empty;
                filtro = String.Empty;
                dgvProductos.ClearSelection();
                //stockdisponible.Visible = false;
                preciooferta.Visible = false;
                precioventa.Visible = false;
            }
            /**
			 * 24-04-2018: LEONARDO VILLEGAS
			 * proce == 3 estaba considerado en esta seccion pero se tuvo que comentar debido a que el listado 
			 * mostrado no se ajustaba
			 */
            else if (/*proce == 3 || proce == 4 ||*/ proce == 5 || proce == 9 || proce == 41) //  3)Pedido Venta, 4)Cotizacion,5)Guia
            {
                dgvProductos.DataSource = data;
                if (codalmacen != 0)
                {
                    data.DataSource = AdmPro.RelacionSalida(Convert.ToInt32(cbTipoArticulo.SelectedValue), codalmacen, CodLista);
                }
                else
                {
                    data.DataSource = AdmPro.RelacionSalida(Convert.ToInt32(cbTipoArticulo.SelectedValue), frmLogin.iCodAlmacen, CodLista);
                }

                DepurarLista2();
                data.Filter = String.Empty;
                filtro = String.Empty;
                dgvProductos.ClearSelection();
                preciooferta.Visible = false;
                precioventa.Visible = false;
            }
            else if (proce == 1)// 1)Nota de salida
            {
                //cbTipoArticulo.SelectedValue = 1;//selecciona MERCADERIAS
                dgvProductos.DataSource = data;
                if (codalmacen != 0)
                {
                    data.DataSource = AdmPro.RelacionSalida(Convert.ToInt32(cbTipoArticulo.SelectedValue), codalmacen, CodLista);
                }
                else
                {
                    data.DataSource = AdmPro.RelacionSalida(Convert.ToInt32(cbTipoArticulo.SelectedValue), frmLogin.iCodAlmacen, CodLista);
                }
                DepurarLista4();
                data.Filter = String.Empty;
                filtro = String.Empty;
                dgvProductos.ClearSelection();
                preciooferta.Visible = false;
                precioventa.Visible = false;
            }
            else if (proce == 42)// Productos Vendedores
            {
                cbTipoArticulo.SelectedValue = 1;//selecciona MERCADERIAS
                dgvProductos.DataSource = data;
                if (codalmacen != 0)
                {
                    data.DataSource = AdmPro.RelacionVendedor(Convert.ToInt32(cbTipoArticulo.SelectedValue), codalmacen, 0, CodVendedor);
                }
                else
                {
                    data.DataSource = AdmPro.RelacionVendedor(Convert.ToInt32(cbTipoArticulo.SelectedValue), frmLogin.iCodAlmacen, 0, CodVendedor);
                }
                if (data.Count == 0)
                {
                    MessageBox.Show("El vendedor no tiene asignada una entrega");
                    this.Close();
                }
                prodvendidos = AdmProd.ListaProdConsultor(CodVendedor);
                if (prodvendidos.Count > 0)
                {
                    foreach (DataGridViewRow row in dgvProductos.Rows)
                    {
                        foreach (clsProducto prod in prodvendidos)
                        {
                            if (prod.CodProducto == Convert.ToInt32(row.Cells[codigo.Name].Value))
                            {
                                row.Cells[stockdisponible.Name].Value = String.Format("{0:#,##0.00}", Convert.ToDouble(row.Cells[stockdisponible.Name].Value) - Convert.ToDouble(prod.StockActual));
                                break;
                            }
                        }
                    }
                }
                DepurarLista2();
                data.Filter = String.Empty;
                filtro = String.Empty;
                dgvProductos.ClearSelection();
                preciooferta.Visible = false;
                precioventa.Visible = false;
            }
            else if (proce == 2 || proce == 3)//2) Venta	3) Orden de Venta
            {
                //cbTipoArticulo.SelectedValue = 1;//selecciona MERCADERIAS
                dgvProductos.DataSource = data;
                if (codalmacen != 0)
                {
                    data.DataSource = AdmPro.RelacionSalida(Convert.ToInt32(cbTipoArticulo.SelectedValue), codalmacen, CodLista);
                }
                else
                {
                    /**
					 * variable CodListaEnviado agregado para condicionar el listado de productos
					 */
                    int CodListaEnviado = (Convert.ToInt32(cbTipoArticulo.SelectedValue) == 2) ? 0 : 1;
                    data.DataSource = AdmPro.RelacionSalida(Convert.ToInt32(cbTipoArticulo.SelectedValue), alma, CodListaEnviado);
                }

                if (consultorext == true)
                {
                    if (data.Count == 0)
                    {
                        MessageBox.Show("El vendedor no tiene asignada una entrega");
                        this.Close();
                    }
                    prodvendidos = AdmProd.ListaProdConsultor(CodVendedor);
                    if (prodvendidos.Count > 0)
                    {
                        foreach (DataGridViewRow row in dgvProductos.Rows)
                        {
                            foreach (clsProducto prod in prodvendidos)
                            {
                                if (prod.CodProducto == Convert.ToInt32(row.Cells[codigo.Name].Value))
                                {
                                    row.Cells[stockdisponible.Name].Value = String.Format("{0:#,##0.00}", Convert.ToDouble(row.Cells[stockdisponible.Name].Value) - Convert.ToDouble(prod.StockActual));
                                    break;
                                }
                            }
                        }
                    }
                }
                DepurarLista2();
                data.Filter = String.Empty;
                filtro = String.Empty;
                dgvProductos.ClearSelection();
                preciooferta.Visible = false;
                precioventa.Visible = false;
            }
            else if (Procede == 11 || Procede == 12 || Procede == 13 || Procede == 14 || Procede == 20)// 11)Requerimiento 12) OrdenCompra
            {
                dgvProductos.DataSource = data;
                data.DataSource = AdmPro.RelacionIngreso(Convert.ToInt32(cbTipoArticulo.SelectedValue), frmLogin.iCodAlmacen);
                data.Filter = String.Empty;
                filtro = String.Empty;
                dgvProductos.ClearSelection();
                precioventa.Visible = false;
                preciooferta.Visible = false;
                stockdisponible.Visible = false;
                preciodolares.Visible = false;
                preciosoles.Visible = false;
            }
            else if (Procede == 15)// 15)frmParamVentxArticulo
            {
                dgvProductos.DataSource = data;
                data.DataSource = AdmPro.RelacionProductos(frmLogin.iCodAlmacen);
                data.Filter = String.Empty;
                filtro = String.Empty;
                dgvProductos.ClearSelection();
                precioventa.Visible = false;
                preciooferta.Visible = false;
                stockdisponible.Visible = false;
                preciodolares.Visible = false;
                preciosoles.Visible = false;
            }
        }

        private void DepurarLista()
        {
            foreach (clsDetalleNotaIngreso deta in productoscargados)
            {
                foreach (DataGridViewRow row in dgvProductos.Rows)
                {
                    if (Convert.ToInt32(row.Cells[codigo.Name].Value) == deta.CodProducto)
                    {
                        dgvProductos.Rows.Remove(row);
                    }
                }
            }
        }

        private void DepurarLista2()
        {
            foreach (clsDetalleFacturaVenta deta in productosfactura)
            {
                foreach (DataGridViewRow row in dgvProductos.Rows)
                {
                    if (Convert.ToInt32(row.Cells[codigo.Name].Value) == deta.CodProducto)
                    {
                        dgvProductos.Rows.Remove(row);
                    }
                }
            }
        }

        private void DepurarLista3()
        {
            foreach (clsDetalleCotizacion deta in productoscotizacion)
            {
                foreach (DataGridViewRow row in dgvProductos.Rows)
                {
                    if (Convert.ToInt32(row.Cells[codigo.Name].Value) == deta.CodProducto)
                    {
                        dgvProductos.Rows.Remove(row);
                    }
                }
            }
        }

        private void DepurarLista4()
        {
            foreach (clsDetalleNotaSalida deta in productosNotaSalida)
            {
                foreach (DataGridViewRow row in dgvProductos.Rows)
                {
                    if (Convert.ToInt32(row.Cells[codigo.Name].Value) == deta.CodProducto)
                    {
                        dgvProductos.Rows.Remove(row);
                    }
                }
            }
        }

        private void CargaTipoArticulos()
        {
            cbTipoArticulo.DataSource = AdmTip.MuestraTipoArticulos();
            cbTipoArticulo.DisplayMember = "descripcion";
            cbTipoArticulo.ValueMember = "codTipoArticulo";
            cbTipoArticulo.SelectedValue = 1;
        }

        private void frmProductosLista_Shown(object sender, EventArgs e)
        {
            //CargaLista(Procede);
            dgvProductos.ClearSelection();
            txtFiltroCodigo.Focus();
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            /***
			 * COMENTADO POR LEONARDO DEBIDO A QUE EL FILTRO SOLO PERMITIA USAR UN CAMPO
			 */
            /*try
            {
                if (txtFiltro.Text.Length >= 2)
                {
                    data.Filter = String.Format("Convert([{0}], System.String) like '*{1}*'", label3.Text.Trim(), txtFiltro.Text.Trim());
                }
                else
                {
                    data.Filter = String.Empty;
                }
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                if(ee.KeyChar != (char)Keys.Down)
                {
                    dgvProductos.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                return;
            }*/
        }

        private void dgvProductos_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            label2.Text = dgvProductos.Columns[e.ColumnIndex].HeaderText;
            label3.Text = dgvProductos.Columns[e.ColumnIndex].DataPropertyName;
            txtFiltro.Focus();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvProductos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            recorrelista();
            if (Procede == 6 || Procede == 7 || Procede == 10)
            {
                DialogResult = DialogResult.OK;
            }
            else if (Procede == 1 || Procede == 2 || Procede == 3 || Procede == 4 || Procede == 41 || Procede == 42 || Procede == 20)
            {
                DialogResult = DialogResult.OK;
            }
            else if (Procede == 5 || Procede == 11 || Procede == 12 || Procede == 9 /*|| Procede==10*/)
            {
                DialogResult = DialogResult.OK;
            }
            else if (Procede == 13)
            {
                codigoPro = pro.CodProducto;
                referenciaPro = pro.Referencia;
                descripcionPro = pro.Descripcion;

            }
            else if (Procede == 14)
            {
                codigoPro = pro.CodProducto;
                referenciaPro = pro.Referencia;
                descripcionPro = pro.Descripcion;
            }

            else if (Procede == 15)
            {
                this.Close();
                DialogResult = DialogResult.OK;
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            recorrelista();
            if (Procede == 6 || Procede == 7 || Procede == 10)
            {
                DialogResult = DialogResult.OK;
            }
            else if (Procede == 1 || Procede == 2 || Procede == 3 || Procede == 4 || Procede == 41 || Procede == 42 || Procede == 20)
            {
                DialogResult = DialogResult.OK;
            }
            else if (Procede == 5 || Procede == 11 || Procede == 12 || Procede == 9 /*|| Procede==10*/)
            {
                DialogResult = DialogResult.OK;
            }
            else if (Procede == 13)
            {
                codigoPro = pro.CodProducto;
                referenciaPro = pro.Referencia;
                descripcionPro = pro.Descripcion;

            }
            else if (Procede == 14)
            {
                codigoPro = pro.CodProducto;
                referenciaPro = pro.Referencia;
                descripcionPro = pro.Descripcion;
            }

            else if (Procede == 15)
            {
                this.Close();
                DialogResult = DialogResult.OK;
            }
            /*recorrelista();
            if (Procede == 6 || Procede == 7 || Procede == 8)
            {
                foreach (int cod in seleccion)
                {
                    if (Application.OpenForms["frmDetalleIngreso"] != null)
                    {
                        Application.OpenForms["frmDetalleIngreso"].Close();
                    }
                    frmDetalleIngreso form = new frmDetalleIngreso();
                    form.Proceso = Proceso;
                    form.Seleccion = 2;
                    form.Procede = Procede;
                    form.bvalorventa = bvalorventa;
                    form.txtCodigo.Text = cod.ToString();
                    if (form.repetido == 1) { form.Close(); this.Close(); }
                    else
                    {
                        form.txtCantidad.Focus();
                        form.ShowDialog();
                    }                    
                }
            }
            
            else if (Procede == 1 || Procede == 2 || Procede == 3 || Procede == 4)
            {
                foreach (int cod in seleccion)
                {
                    if (Application.OpenForms["frmDetalleSalida"] != null)
                    {
                        Application.OpenForms["frmDetalleSalida"].Close();
                    }
                    frmDetalleSalida form = new frmDetalleSalida();
                    form.Seleccion = 2;
                    form.Proceso = Proceso;
                    form.Codlista = CodLista;
                    form.Procede = Procede;
                    form.Moneda = Moneda;
                    form.tc = tc;
                    form.alma = alma;
                    form.txtCodigo.Text = cod.ToString();
                    form.txtPrecio.ReadOnly = true;
                    form.codTran = codtrans;
                    form.ShowDialog();
                }
            }
            else if (Procede == 5 || Procede == 11 || Procede == 12 || Procede == 9 || Procede == 10)
            {
                foreach (int cod in seleccion)
                {
                    if (Application.OpenForms["frmDetalleGuia"] != null)
                    {
                        Application.OpenForms["frmDetalleGuia"].Close();
                    }
                    frmDetalleGuia form = new frmDetalleGuia();
                    form.txtCantidad.Focus();
                    form.Seleccion = 2;
                    form.Proceso = Proceso;
                    form.Procede = Procede;
                    if (Procede == 10) form.chBonificacion.Visible = true;
                    form.txtCodigo.Text = cod.ToString();
                    if (form.repetido == 1) { form.Close(); this.Close(); }
                    else
                    {
                        form.txtCantidad.Focus();
                        form.ShowDialog();
                    }
                    
                }
            }
            else if (Procede == 13)
            {
                codigoPro = pro.CodProducto;
                referenciaPro = pro.Referencia;
                descripcionPro = pro.Descripcion;

            }
            else if (Procede == 14)
            {
                codigoPro = pro.CodProducto;
                referenciaPro = pro.Referencia;
                descripcionPro = pro.Descripcion;
            }
            this.Close();*/

        }

        private void recorrelista()
        {
            seleccion.Clear();
            if (dgvProductos.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvProductos.SelectedRows)
                {
                    seleccion.Add(Convert.ToInt32(row.Cells[codigo.Name].Value));
                }
            }
        }

        private void cbTipoArticulo_SelectionChangeCommitted(object sender, EventArgs e)
        {
            CargaLista(Procede);
        }

        private void frmProductosLista_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void txtFiltroCodigo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int tipoArticuloSeleccionado = Convert.ToInt32(cbTipoArticulo.SelectedValue);
                if (txtFiltroCodigo.Text.Length >= 1)
                {
                    if (tipoArticuloSeleccionado == 1)
                    {
                        //mercaderia
                        String Filtro = (txtFiltroCodigo.Text.Trim().Length > 0) ?
                                        "[referencia] like '{0}' and [modelo] like '%{1}%' and " +
                                        "[nmarca] like '%{2}%' and [codUniversal] like '%{3}%' and " +
                                        "[ubicacion] like '%{4}%' and [descripcion] like '%{5}%'" :
                                        "[referencia] like '%{0}%' and [modelo] like '%{1}%' and " +
                                        "[nmarca] like '%{2}%' and [codUniversal] like '%{3}%' and " +
                                        "[ubicacion] like '%{4}%' and [descripcion] like '%{5}%'";

                        data.Filter = String.Format(Filtro, txtFiltroCodigo.Text.Trim(), txtFiltroModelo.Text.Trim(),
                                                           txtFiltroMarca.Text.Trim(), txtFiltroCodUniv.Text.Trim(),
                                                           txtFiltroUbicacion.Text.Trim(), txtFiltroDescripcion.Text.Trim());
                    }
                    else
                    {
                        //servicio
                        String Filtro = (txtFiltroCodigo.Text.Trim().Length > 0) ? "[referencia] like '{0}' and [descripcion] like '%{1}%'" :
                                                                               "[referencia] like '%{0}%' and [descripcion] like '%{1}%'";
                        data.Filter = String.Format(Filtro, txtFiltroCodigo.Text.Trim(), txtFiltroDescripcion.Text.Trim());
                    }
                }
                else
                {
                    data.Filter = String.Empty;
                }
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                if (ee.KeyChar != (char)Keys.Down)
                {
                    dgvProductos.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void txtFiltroModelo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtFiltroModelo.Text.Length >= 1)
                {
                    String Filtro = (txtFiltroCodigo.Text.Trim().Length > 0) ?
                                    "[referencia] like '{0}' and [modelo] like '%{1}%' and " +
                                    "[nmarca] like '%{2}%' and [codUniversal] like '%{3}%' and " +
                                    "[ubicacion] like '%{4}%' and [descripcion] like '%{5}%'" :
                                    "[referencia] like '%{0}%' and [modelo] like '%{1}%' and " +
                                    "[nmarca] like '%{2}%' and [codUniversal] like '%{3}%' and " +
                                    "[ubicacion] like '%{4}%' and [descripcion] like '%{5}%'";

                    data.Filter = String.Format(Filtro, txtFiltroCodigo.Text.Trim(), txtFiltroModelo.Text.Trim(),
                                                       txtFiltroMarca.Text.Trim(), txtFiltroCodUniv.Text.Trim(),
                                                       txtFiltroUbicacion.Text.Trim(), txtFiltroDescripcion.Text.Trim());
                }
                else
                {
                    data.Filter = String.Empty;
                }
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                if (ee.KeyChar != (char)Keys.Down)
                {
                    dgvProductos.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void txtFiltroMarca_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtFiltroMarca.Text.Length >= 1)
                {

                    String Filtro = (txtFiltroCodigo.Text.Trim().Length > 0) ?
                                    "[referencia] like '{0}' and [modelo] like '%{1}%' and " +
                                    "[nmarca] like '%{2}%' and [codUniversal] like '%{3}%' and " +
                                    "[ubicacion] like '%{4}%' and [descripcion] like '%{5}%'" :
                                    "[referencia] like '%{0}%' and [modelo] like '%{1}%' and " +
                                    "[nmarca] like '%{2}%' and [codUniversal] like '%{3}%' and " +
                                    "[ubicacion] like '%{4}%' and [descripcion] like '%{5}%'";

                    data.Filter = String.Format(Filtro, txtFiltroCodigo.Text.Trim(), txtFiltroModelo.Text.Trim(),
                                                       txtFiltroMarca.Text.Trim(), txtFiltroCodUniv.Text.Trim(),
                                                       txtFiltroUbicacion.Text.Trim(), txtFiltroDescripcion.Text.Trim());
                }
                else
                {
                    data.Filter = String.Empty;
                }
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                if (ee.KeyChar != (char)Keys.Down)
                {
                    dgvProductos.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void txtFiltroCodUniv_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtFiltroCodUniv.Text.Length >= 1)
                {
                    String Filtro = (txtFiltroCodigo.Text.Trim().Length > 0) ?
                                    "[referencia] like '{0}' and [modelo] like '%{1}%' and " +
                                    "[nmarca] like '%{2}%' and [codUniversal] like '%{3}%' and " +
                                    "[ubicacion] like '%{4}%' and [descripcion] like '%{5}%'" :
                                    "[referencia] like '%{0}%' and [modelo] like '%{1}%' and " +
                                    "[nmarca] like '%{2}%' and [codUniversal] like '%{3}%' and " +
                                    "[ubicacion] like '%{4}%' and [descripcion] like '%{5}%'";

                    data.Filter = String.Format(Filtro, txtFiltroCodigo.Text.Trim(), txtFiltroModelo.Text.Trim(),
                                                       txtFiltroMarca.Text.Trim(), txtFiltroCodUniv.Text.Trim(),
                                                       txtFiltroUbicacion.Text.Trim(), txtFiltroDescripcion.Text.Trim());
                }
                else
                {
                    data.Filter = String.Empty;
                }
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                if (ee.KeyChar != (char)Keys.Down)
                {
                    dgvProductos.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void txtFiltroUbicacion_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtFiltroUbicacion.Text.Length >= 1)
                {
                    String Filtro = (txtFiltroCodigo.Text.Trim().Length > 0) ?
                                    "[referencia] like '{0}' and [modelo] like '%{1}%' and " +
                                    "[nmarca] like '%{2}%' and [codUniversal] like '%{3}%' and " +
                                    "[ubicacion] like '%{4}%' and [descripcion] like '%{5}%'" :
                                    "[referencia] like '%{0}%' and [modelo] like '%{1}%' and " +
                                    "[nmarca] like '%{2}%' and [codUniversal] like '%{3}%' and " +
                                    "[ubicacion] like '%{4}%' and [descripcion] like '%{5}%'";

                    data.Filter = String.Format(Filtro, txtFiltroCodigo.Text.Trim(), txtFiltroModelo.Text.Trim(),
                                                       txtFiltroMarca.Text.Trim(), txtFiltroCodUniv.Text.Trim(),
                                                       txtFiltroUbicacion.Text.Trim(), txtFiltroDescripcion.Text.Trim());
                }
                else
                {
                    data.Filter = String.Empty;
                }
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                if (ee.KeyChar != (char)Keys.Down)
                {
                    dgvProductos.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void txtFiltroDescripcion_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int tipoArticuloSeleccionado = Convert.ToInt32(cbTipoArticulo.SelectedValue);

                if (txtFiltroDescripcion.Text.Length >= 1)
                {
                    if (tipoArticuloSeleccionado == 1)
                    {
                        //mercaderia
                        String Filtro = (txtFiltroCodigo.Text.Trim().Length > 0) ?
                                        "[referencia] like '{0}' and [modelo] like '%{1}%' and " +
                                        "[nmarca] like '%{2}%' and [codUniversal] like '%{3}%' and " +
                                        "[ubicacion] like '%{4}%' and [descripcion] like '%{5}%'" :
                                        "[referencia] like '%{0}%' and [modelo] like '%{1}%' and " +
                                        "[nmarca] like '%{2}%' and [codUniversal] like '%{3}%' and " +
                                        "[ubicacion] like '%{4}%' and [descripcion] like '%{5}%'";

                        data.Filter = String.Format(Filtro, txtFiltroCodigo.Text.Trim(), txtFiltroModelo.Text.Trim(),
                                                           txtFiltroMarca.Text.Trim(), txtFiltroCodUniv.Text.Trim(),
                                                           txtFiltroUbicacion.Text.Trim(), txtFiltroDescripcion.Text.Trim());
                    }
                    else
                    {
                        //servicio
                        String Filtro = (txtFiltroCodigo.Text.Trim().Length > 0) ? "[referencia] like '{0}' and [descripcion] like '%{1}%'" :
                                                                                   "[referencia] like '%{0}%' and [descripcion] like '%{1}%'";

                        data.Filter = String.Format(Filtro, txtFiltroCodigo.Text.Trim(), txtFiltroDescripcion.Text.Trim());
                    }
                }
                else
                {
                    data.Filter = String.Empty;
                }
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                if (ee.KeyChar != (char)Keys.Down)
                {
                    dgvProductos.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void cbTipoArticulo_SelectedIndexChanged(object sender, EventArgs e)
        {
            /**
			 * se trabajo con SelectedIndex pues con SelectedValue generaba error al cargar
			 * el formulario
			 */
            int tipoArticuloSeleccionado = cbTipoArticulo.SelectedIndex;
            switch (tipoArticuloSeleccionado)
            {
                case 0:
                    txtFiltroModelo.Enabled = true;
                    txtFiltroMarca.Enabled = true;
                    txtFiltroCodUniv.Enabled = true;
                    txtFiltroUbicacion.Enabled = true;
                    break;
                case 1:
                    txtFiltroModelo.Enabled = false;
                    txtFiltroMarca.Enabled = false;
                    txtFiltroCodUniv.Enabled = false;
                    txtFiltroUbicacion.Enabled = false;
                    break;
            }
        }

        private void txtFiltroCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dgvProductos.Focus();
            }
        }

        private void dgvProductos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvProductos.Rows.Count >= 1 && e.RowIndex != -1 && dgvProductos.CurrentRow.Index == e.RowIndex)
                {
                    DataGridViewRow Row = dgvProductos.Rows[e.RowIndex];
                    pro.CodProducto = Convert.ToInt32(Row.Cells[codigo.Name].Value);
                    pro.Referencia = Row.Cells[referencia.Name].Value.ToString();
                    pro.Descripcion = Row.Cells[descripcion.Name].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtFiltro_KeyDown(object sender, KeyEventArgs e)
        {
            //Flecha Hacia ABAJO 
            if (e.KeyCode == Keys.Down)
            {
                dgvProductos.Focus();
            }
        }

        private void dgvProductos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                recorrelista();
                if (Procede == 6 || Procede == 7 || Procede == 8)
                {
                    foreach (int cod in seleccion)
                    {
                        if (Application.OpenForms["frmDetalleIngreso"] != null)
                        {
                            Application.OpenForms["frmDetalleIngreso"].Close();
                        }
                        frmDetalleIngreso form = new frmDetalleIngreso();
                        form.Proceso = Proceso;
                        form.Seleccion = 2;
                        form.Procede = Procede;
                        form.bvalorventa = bvalorventa;
                        form.txtCodigo.Text = cod.ToString();
                        if (form.repetido == 1) { form.Close(); this.Close(); }
                        else
                        {
                            form.txtCantidad.Focus();
                            form.ShowDialog();
                        }
                    }
                }

                else if (Procede == 1 || Procede == 2 || Procede == 3 || Procede == 4 || Procede == 20)/*20 agregado por reporte*/
                {
                    DialogResult = DialogResult.OK;
                    /*foreach (int cod in seleccion)
                    {
                        if (Application.OpenForms["frmDetalleSalida"] != null)
                        {
                            Application.OpenForms["frmDetalleSalida"].Close();
                        }
                        frmDetalleSalida form = new frmDetalleSalida();
                        form.Seleccion = 2;
                        form.Proceso = Proceso;
                        form.Codlista = CodLista;
                        form.Procede = Procede;
                        form.Moneda = Moneda;
                        form.tc = tc;
                        form.alma = alma;
                        form.txtCodigo.Text = cod.ToString();
                        form.txtPrecio.ReadOnly = true;
                        form.codTran = codtrans;
                        form.ShowDialog();
                    }*/

                }
                else if (Procede == 5 || Procede == 11 || Procede == 12 || Procede == 9 || Procede == 10)
                {
                    foreach (int cod in seleccion)
                    {
                        if (Application.OpenForms["frmDetalleGuia"] != null)
                        {
                            Application.OpenForms["frmDetalleGuia"].Close();
                        }
                        frmDetalleGuia form = new frmDetalleGuia();
                        form.txtCantidad.Focus();
                        form.Seleccion = 2;
                        form.Proceso = Proceso;
                        form.Procede = Procede;
                        if (Procede == 10) form.chBonificacion.Visible = true;
                        form.txtCodigo.Text = cod.ToString();
                        if (form.repetido == 1) { form.Close(); this.Close(); }
                        else
                        {
                            form.txtCantidad.Focus();
                            form.ShowDialog();
                        }

                    }
                }
                else if (Procede == 13)
                {
                    codigoPro = pro.CodProducto;
                    referenciaPro = pro.Referencia;
                    descripcionPro = pro.Descripcion;

                }
                else if (Procede == 14)
                {
                    codigoPro = pro.CodProducto;
                    referenciaPro = pro.Referencia;
                    descripcionPro = pro.Descripcion;
                }
                else if (Procede == 15)
                {
                    int f = dgvProductos.CurrentRow.Index;
                    pro.CodProducto = Convert.ToInt32(dgvProductos.Rows[f].Cells[codigo.Name].Value);
                }
                this.Close();
            }
        }
    }
}
