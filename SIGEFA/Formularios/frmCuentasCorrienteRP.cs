﻿using System;
using System.Data;
using System.Windows.Forms;
using SIGEFA.Reportes;

namespace SIGEFA.Formularios
{
	public partial class frmCuentasCorrienteRP : Form
    {
        public DataTable DTable;

        public frmCuentasCorrienteRP()
        {
            InitializeComponent();
        }

        private void frmCuentasCorrienteRP_Load(object sender, EventArgs e)
        {
            CRCtasCte CRep = new CRCtasCte();
            CRep.Load("CRCtasCte.rpt");
            CRep.SetDataSource(DTable);
            CRVCtasCte.ReportSource = CRep;
        }
    }
}
