﻿using DevComponents.DotNetBar.Controls;
namespace SIGEFA.Formularios
{
    partial class frmOrdenVenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOrdenVenta));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvDetalle = new System.Windows.Forms.DataGridView();
            this.coddetalle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codproducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codunidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preciounit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montodscto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.igv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valoreal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioreal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioconigv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorpromedio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipoarticulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipoimpuesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codalmacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.almacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoUnidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodEmpresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnEditaOV = new System.Windows.Forms.Button();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.btnDeleteItem = new System.Windows.Forms.Button();
            this.btnAnulaOV = new System.Windows.Forms.Button();
            this.btnInicioOV = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkFactura = new System.Windows.Forms.RadioButton();
            this.chkBoleta = new System.Windows.Forms.RadioButton();
            this.btnSalir = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnGuardaOV = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtNombreVendedor = new System.Windows.Forms.TextBox();
            this.txtCodigoVendedor = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnImprimirTicket = new System.Windows.Forms.Button();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.txtPDescuento = new System.Windows.Forms.TextBox();
            this.txtBruto = new System.Windows.Forms.TextBox();
            this.txtDscto = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.pbCapchatS = new System.Windows.Forms.PictureBox();
            this.txtSunat_Capchat = new System.Windows.Forms.TextBox();
            this.dtpFechaPago = new System.Windows.Forms.DateTimePicker();
            this.cmbFormaPago = new System.Windows.Forms.ComboBox();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.txtNombreCliente = new System.Windows.Forms.TextBox();
            this.txtCodCliente = new System.Windows.Forms.TextBox();
            this.chkVentaDsctoGlobal = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkVentaGratuita = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtinafectas = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtexoneradas = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtgratuitas = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtgravadas = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtPrecioVenta = new System.Windows.Forms.TextBox();
            this.txtIGV = new System.Windows.Forms.TextBox();
            this.txtValorVenta = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.lbDocumento = new System.Windows.Forms.Label();
            this.txtDocRef = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSerie = new System.Windows.Forms.TextBox();
            this.txtPedido = new System.Windows.Forms.TextBox();
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.superValidator1 = new DevComponents.DotNetBar.Validator.SuperValidator();
            this.requiredFieldValidator1 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("DEBE SELECCIONAR UN VENDEDOR");
            this.requiredFieldValidator4 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Your error message here.");
            this.customValidator6 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.requiredFieldValidator3 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("SELECCIONE UN CLIENTE");
            this.requiredFieldValidator2 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("SELECCIONE UN CLIENTE");
            this.customValidator5 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.txtCodigoBarras = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnCancelarOV = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txttasa = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.lbLineaCredito = new System.Windows.Forms.Label();
            this.txtLineaCredito = new System.Windows.Forms.TextBox();
            this.txtLineaCreditoUso = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtLineaCreditoDisponible = new System.Windows.Forms.TextBox();
            this.btVendedor = new DevComponents.DotNetBar.BalloonTip();
            this.lblCantidadProductos = new System.Windows.Forms.Label();
            this.cachedCRCuotasPrestamo1 = new SIGEFA.Reportes.clsReportes.CachedCRCuotasPrestamo();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalle)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCapchatS)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvDetalle);
            this.groupBox1.Location = new System.Drawing.Point(12, 77);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(865, 320);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalle Orden Venta";
            // 
            // dgvDetalle
            // 
            this.dgvDetalle.AllowUserToAddRows = false;
            this.dgvDetalle.AllowUserToDeleteRows = false;
            this.dgvDetalle.AllowUserToResizeRows = false;
            this.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.coddetalle,
            this.codproducto,
            this.referencia,
            this.descripcion,
            this.codunidad,
            this.unidad,
            this.cantidad,
            this.preciounit,
            this.importe,
            this.dscto1,
            this.dscto2,
            this.dscto3,
            this.montodscto,
            this.valorventa,
            this.igv,
            this.precioventa,
            this.valoreal,
            this.precioreal,
            this.precioconigv,
            this.valorpromedio,
            this.Tipoarticulo,
            this.Tipoimpuesto,
            this.codalmacen,
            this.almacen,
            this.TipoUnidad,
            this.CodEmpresa});
            this.dgvDetalle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDetalle.Location = new System.Drawing.Point(3, 18);
            this.dgvDetalle.Name = "dgvDetalle";
            this.dgvDetalle.RowHeadersVisible = false;
            this.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvDetalle.Size = new System.Drawing.Size(859, 299);
            this.dgvDetalle.TabIndex = 2;
            this.dgvDetalle.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetalle_CellClick);
            this.dgvDetalle.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetalle_CellDoubleClick);
            this.dgvDetalle.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetalle_CellEndEdit);
            this.dgvDetalle.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvDetalle_RowsAdded);
            this.dgvDetalle.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvDetalle_RowsRemoved);
            // 
            // coddetalle
            // 
            this.coddetalle.DataPropertyName = "codDetalle";
            this.coddetalle.HeaderText = "CodDetalle";
            this.coddetalle.Name = "coddetalle";
            this.coddetalle.ReadOnly = true;
            this.coddetalle.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.coddetalle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.coddetalle.Visible = false;
            // 
            // codproducto
            // 
            this.codproducto.DataPropertyName = "codProducto";
            this.codproducto.HeaderText = "Código de Producto_";
            this.codproducto.Name = "codproducto";
            this.codproducto.ReadOnly = true;
            this.codproducto.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.codproducto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.codproducto.Visible = false;
            this.codproducto.Width = 80;
            // 
            // referencia
            // 
            this.referencia.DataPropertyName = "referencia";
            this.referencia.HeaderText = "Código de Producto";
            this.referencia.Name = "referencia";
            this.referencia.ReadOnly = true;
            this.referencia.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.referencia.Width = 80;
            // 
            // descripcion
            // 
            this.descripcion.DataPropertyName = "producto";
            this.descripcion.HeaderText = "Descripción";
            this.descripcion.Name = "descripcion";
            this.descripcion.ReadOnly = true;
            this.descripcion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.descripcion.Width = 405;
            // 
            // codunidad
            // 
            this.codunidad.DataPropertyName = "codUnidadMedida";
            this.codunidad.HeaderText = "Cod. Unidad";
            this.codunidad.Name = "codunidad";
            this.codunidad.ReadOnly = true;
            this.codunidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.codunidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.codunidad.Visible = false;
            // 
            // unidad
            // 
            this.unidad.DataPropertyName = "unidad";
            this.unidad.HeaderText = "Unidad";
            this.unidad.Name = "unidad";
            this.unidad.ReadOnly = true;
            this.unidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.unidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.unidad.Width = 130;
            // 
            // cantidad
            // 
            this.cantidad.DataPropertyName = "cantidad";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.cantidad.DefaultCellStyle = dataGridViewCellStyle1;
            this.cantidad.HeaderText = "Cantidad";
            this.cantidad.Name = "cantidad";
            this.cantidad.ReadOnly = true;
            this.cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cantidad.Width = 80;
            // 
            // preciounit
            // 
            this.preciounit.DataPropertyName = "preciounitario";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.preciounit.DefaultCellStyle = dataGridViewCellStyle2;
            this.preciounit.HeaderText = "P. Unit.";
            this.preciounit.Name = "preciounit";
            this.preciounit.ReadOnly = true;
            this.preciounit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.preciounit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.preciounit.Width = 80;
            // 
            // importe
            // 
            this.importe.DataPropertyName = "subtotal";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.importe.DefaultCellStyle = dataGridViewCellStyle3;
            this.importe.HeaderText = "Importe";
            this.importe.Name = "importe";
            this.importe.ReadOnly = true;
            this.importe.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.importe.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.importe.Visible = false;
            // 
            // dscto1
            // 
            this.dscto1.DataPropertyName = "descuento1";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.dscto1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dscto1.HeaderText = "% Dscto1";
            this.dscto1.Name = "dscto1";
            this.dscto1.ReadOnly = true;
            this.dscto1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto1.Visible = false;
            // 
            // dscto2
            // 
            this.dscto2.DataPropertyName = "descuento2";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            this.dscto2.DefaultCellStyle = dataGridViewCellStyle5;
            this.dscto2.HeaderText = "% Dscto2";
            this.dscto2.Name = "dscto2";
            this.dscto2.ReadOnly = true;
            this.dscto2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto2.Visible = false;
            // 
            // dscto3
            // 
            this.dscto3.DataPropertyName = "descuento3";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            this.dscto3.DefaultCellStyle = dataGridViewCellStyle6;
            this.dscto3.HeaderText = "% Dscto3";
            this.dscto3.Name = "dscto3";
            this.dscto3.ReadOnly = true;
            this.dscto3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto3.Visible = false;
            // 
            // montodscto
            // 
            this.montodscto.DataPropertyName = "montodscto";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = null;
            this.montodscto.DefaultCellStyle = dataGridViewCellStyle7;
            this.montodscto.HeaderText = "Monto Dscto";
            this.montodscto.Name = "montodscto";
            this.montodscto.ReadOnly = true;
            this.montodscto.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.montodscto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.montodscto.Visible = false;
            // 
            // valorventa
            // 
            this.valorventa.DataPropertyName = "valorventa";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            dataGridViewCellStyle8.NullValue = null;
            this.valorventa.DefaultCellStyle = dataGridViewCellStyle8;
            this.valorventa.HeaderText = "V. Venta";
            this.valorventa.Name = "valorventa";
            this.valorventa.ReadOnly = true;
            this.valorventa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valorventa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.valorventa.Visible = false;
            // 
            // igv
            // 
            this.igv.DataPropertyName = "igv";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N2";
            this.igv.DefaultCellStyle = dataGridViewCellStyle9;
            this.igv.HeaderText = "IGV";
            this.igv.Name = "igv";
            this.igv.ReadOnly = true;
            this.igv.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.igv.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.igv.Visible = false;
            // 
            // precioventa
            // 
            this.precioventa.DataPropertyName = "importe";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N2";
            this.precioventa.DefaultCellStyle = dataGridViewCellStyle10;
            this.precioventa.HeaderText = "P. Venta";
            this.precioventa.Name = "precioventa";
            this.precioventa.ReadOnly = true;
            this.precioventa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.precioventa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.precioventa.Width = 80;
            // 
            // valoreal
            // 
            this.valoreal.DataPropertyName = "valoreal";
            dataGridViewCellStyle11.Format = "N2";
            dataGridViewCellStyle11.NullValue = null;
            this.valoreal.DefaultCellStyle = dataGridViewCellStyle11;
            this.valoreal.HeaderText = "V. real";
            this.valoreal.Name = "valoreal";
            this.valoreal.ReadOnly = true;
            this.valoreal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valoreal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.valoreal.Visible = false;
            // 
            // precioreal
            // 
            this.precioreal.DataPropertyName = "precioreal";
            dataGridViewCellStyle12.Format = "N2";
            dataGridViewCellStyle12.NullValue = null;
            this.precioreal.DefaultCellStyle = dataGridViewCellStyle12;
            this.precioreal.HeaderText = "P. real";
            this.precioreal.Name = "precioreal";
            this.precioreal.ReadOnly = true;
            this.precioreal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.precioreal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.precioreal.Visible = false;
            // 
            // precioconigv
            // 
            this.precioconigv.DataPropertyName = "precioigv";
            this.precioconigv.HeaderText = "precioconigv";
            this.precioconigv.Name = "precioconigv";
            this.precioconigv.ReadOnly = true;
            this.precioconigv.Visible = false;
            // 
            // valorpromedio
            // 
            this.valorpromedio.DataPropertyName = "valorpromedio";
            this.valorpromedio.HeaderText = "valorpromedio";
            this.valorpromedio.Name = "valorpromedio";
            this.valorpromedio.ReadOnly = true;
            this.valorpromedio.Visible = false;
            // 
            // Tipoarticulo
            // 
            this.Tipoarticulo.DataPropertyName = "Tipoarticulo";
            this.Tipoarticulo.HeaderText = "Tipoarticulo";
            this.Tipoarticulo.Name = "Tipoarticulo";
            this.Tipoarticulo.ReadOnly = true;
            this.Tipoarticulo.Visible = false;
            // 
            // Tipoimpuesto
            // 
            this.Tipoimpuesto.DataPropertyName = "Tipoimpuesto";
            this.Tipoimpuesto.HeaderText = "Tipoimpuesto";
            this.Tipoimpuesto.Name = "Tipoimpuesto";
            this.Tipoimpuesto.ReadOnly = true;
            this.Tipoimpuesto.Visible = false;
            // 
            // codalmacen
            // 
            this.codalmacen.DataPropertyName = "codalmacen";
            this.codalmacen.HeaderText = "codalmacen";
            this.codalmacen.Name = "codalmacen";
            this.codalmacen.ReadOnly = true;
            this.codalmacen.Visible = false;
            // 
            // almacen
            // 
            this.almacen.DataPropertyName = "almacen";
            this.almacen.HeaderText = "Almacen";
            this.almacen.Name = "almacen";
            this.almacen.ReadOnly = true;
            this.almacen.Visible = false;
            // 
            // TipoUnidad
            // 
            this.TipoUnidad.HeaderText = "TipoUnidad";
            this.TipoUnidad.Name = "TipoUnidad";
            this.TipoUnidad.ReadOnly = true;
            this.TipoUnidad.Visible = false;
            // 
            // CodEmpresa
            // 
            this.CodEmpresa.DataPropertyName = "CodEmpresa";
            this.CodEmpresa.HeaderText = "CodEmpresa";
            this.CodEmpresa.Name = "CodEmpresa";
            this.CodEmpresa.ReadOnly = true;
            this.CodEmpresa.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(883, 93);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(283, 224);
            this.panel1.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnEditaOV);
            this.groupBox3.Controls.Add(this.btnAddItem);
            this.groupBox3.Controls.Add(this.btnDeleteItem);
            this.groupBox3.Controls.Add(this.btnAnulaOV);
            this.groupBox3.Controls.Add(this.btnInicioOV);
            this.groupBox3.Location = new System.Drawing.Point(3, 9);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(275, 158);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Opciones";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // btnEditaOV
            // 
            this.btnEditaOV.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnEditaOV.FlatAppearance.BorderSize = 2;
            this.btnEditaOV.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditaOV.Location = new System.Drawing.Point(98, 117);
            this.btnEditaOV.Name = "btnEditaOV";
            this.btnEditaOV.Size = new System.Drawing.Size(90, 35);
            this.btnEditaOV.TabIndex = 5;
            this.btnEditaOV.Text = "F4 - EDITA OV";
            this.btnEditaOV.UseVisualStyleBackColor = true;
            this.btnEditaOV.Click += new System.EventHandler(this.btnEditaOV_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.btnAddItem.Enabled = false;
            this.btnAddItem.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnAddItem.FlatAppearance.BorderSize = 2;
            this.btnAddItem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnAddItem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.Color.Black;
            this.btnAddItem.Image = ((System.Drawing.Image)(resources.GetObject("btnAddItem.Image")));
            this.btnAddItem.Location = new System.Drawing.Point(39, 29);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(206, 33);
            this.btnAddItem.TabIndex = 1;
            this.btnAddItem.Text = "AGREGAR PRODUCTO (F3)";
            this.btnAddItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddItem.UseVisualStyleBackColor = true;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.btnDeleteItem.Enabled = false;
            this.btnDeleteItem.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnDeleteItem.FlatAppearance.BorderSize = 2;
            this.btnDeleteItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteItem.Image")));
            this.btnDeleteItem.Location = new System.Drawing.Point(39, 68);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(206, 33);
            this.btnDeleteItem.TabIndex = 2;
            this.btnDeleteItem.Text = "ELIMINAR PRODUCTO (F2)";
            this.btnDeleteItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDeleteItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDeleteItem.UseVisualStyleBackColor = true;
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // btnAnulaOV
            // 
            this.btnAnulaOV.Enabled = false;
            this.btnAnulaOV.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnAnulaOV.FlatAppearance.BorderSize = 2;
            this.btnAnulaOV.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnulaOV.Location = new System.Drawing.Point(197, 117);
            this.btnAnulaOV.Name = "btnAnulaOV";
            this.btnAnulaOV.Size = new System.Drawing.Size(71, 35);
            this.btnAnulaOV.TabIndex = 4;
            this.btnAnulaOV.Text = "ANULA \r\n OV";
            this.btnAnulaOV.UseVisualStyleBackColor = true;
            // 
            // btnInicioOV
            // 
            this.btnInicioOV.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnInicioOV.FlatAppearance.BorderSize = 2;
            this.btnInicioOV.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInicioOV.Location = new System.Drawing.Point(6, 117);
            this.btnInicioOV.Name = "btnInicioOV";
            this.btnInicioOV.Size = new System.Drawing.Size(86, 35);
            this.btnInicioOV.TabIndex = 0;
            this.btnInicioOV.Text = "F6 INICIA OV";
            this.btnInicioOV.UseVisualStyleBackColor = true;
            this.btnInicioOV.Click += new System.EventHandler(this.btnInicioOV_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkFactura);
            this.groupBox2.Controls.Add(this.chkBoleta);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(3, 173);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(275, 41);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // chkFactura
            // 
            this.chkFactura.AutoSize = true;
            this.chkFactura.Enabled = false;
            this.chkFactura.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.chkFactura.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkFactura.Location = new System.Drawing.Point(164, 16);
            this.chkFactura.Name = "chkFactura";
            this.chkFactura.Size = new System.Drawing.Size(77, 18);
            this.chkFactura.TabIndex = 1;
            this.chkFactura.Text = "FACTURA";
            this.chkFactura.UseVisualStyleBackColor = true;
            this.chkFactura.Click += new System.EventHandler(this.chkFactura_Click);
            // 
            // chkBoleta
            // 
            this.chkBoleta.AutoSize = true;
            this.chkBoleta.Checked = true;
            this.chkBoleta.Enabled = false;
            this.chkBoleta.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.chkBoleta.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkBoleta.Location = new System.Drawing.Point(49, 16);
            this.chkBoleta.Name = "chkBoleta";
            this.chkBoleta.Size = new System.Drawing.Size(69, 18);
            this.chkBoleta.TabIndex = 0;
            this.chkBoleta.TabStop = true;
            this.chkBoleta.Text = "BOLETA";
            this.chkBoleta.UseVisualStyleBackColor = true;
            this.chkBoleta.Click += new System.EventHandler(this.chkBoleta_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Enabled = false;
            this.btnSalir.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSalir.FlatAppearance.BorderSize = 2;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(130, 122);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(108, 43);
            this.btnSalir.TabIndex = 6;
            this.btnSalir.Text = "CERRAR OV F9";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Write Document.png");
            this.imageList1.Images.SetKeyName(1, "New Document.png");
            this.imageList1.Images.SetKeyName(2, "Remove Document.png");
            this.imageList1.Images.SetKeyName(3, "document-print.png");
            this.imageList1.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
            this.imageList1.Images.SetKeyName(5, "exit.png");
            // 
            // btnGuardaOV
            // 
            this.btnGuardaOV.Enabled = false;
            this.btnGuardaOV.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnGuardaOV.FlatAppearance.BorderSize = 2;
            this.btnGuardaOV.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardaOV.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardaOV.Image")));
            this.btnGuardaOV.Location = new System.Drawing.Point(6, 122);
            this.btnGuardaOV.Name = "btnGuardaOV";
            this.btnGuardaOV.Size = new System.Drawing.Size(122, 43);
            this.btnGuardaOV.TabIndex = 7;
            this.btnGuardaOV.Text = "GUARDA OV F8";
            this.btnGuardaOV.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardaOV.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardaOV.UseVisualStyleBackColor = true;
            this.btnGuardaOV.Click += new System.EventHandler(this.btnGuardaOV_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtNombreVendedor);
            this.groupBox4.Controls.Add(this.txtCodigoVendedor);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.btnImprimirTicket);
            this.groupBox4.Controls.Add(this.txtPDescuento);
            this.groupBox4.Controls.Add(this.txtBruto);
            this.groupBox4.Controls.Add(this.txtDscto);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.pbCapchatS);
            this.groupBox4.Controls.Add(this.txtSunat_Capchat);
            this.groupBox4.Controls.Add(this.dtpFechaPago);
            this.groupBox4.Controls.Add(this.cmbFormaPago);
            this.groupBox4.Controls.Add(this.txtDireccion);
            this.groupBox4.Controls.Add(this.txtNombreCliente);
            this.groupBox4.Controls.Add(this.txtCodCliente);
            this.groupBox4.Controls.Add(this.chkVentaDsctoGlobal);
            this.groupBox4.Controls.Add(this.chkVentaGratuita);
            this.groupBox4.Controls.Add(this.dtpFecha);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(15, 403);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(609, 211);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Datos del Cliente";
            // 
            // txtNombreVendedor
            // 
            this.txtNombreVendedor.Location = new System.Drawing.Point(175, 106);
            this.txtNombreVendedor.Name = "txtNombreVendedor";
            this.txtNombreVendedor.ReadOnly = true;
            this.txtNombreVendedor.Size = new System.Drawing.Size(320, 22);
            this.txtNombreVendedor.TabIndex = 131;
            this.txtNombreVendedor.Text = "<--  SELECCIONE UN VENDEDOR";
            this.superValidator1.SetValidator1(this.txtNombreVendedor, this.requiredFieldValidator1);
            // 
            // txtCodigoVendedor
            // 
            this.txtCodigoVendedor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btVendedor.SetBalloonCaption(this.txtCodigoVendedor, "BUSCA DE VENDEDOR");
            this.btVendedor.SetBalloonText(this.txtCodigoVendedor, "PRESIONE F1 PARA BUSCAR VENDEDORES O DIGITE EL CÓDIGO DEL VENDEDOR Y PRESIONE ENT" +
        "ER");
            this.txtCodigoVendedor.Location = new System.Drawing.Point(114, 106);
            this.txtCodigoVendedor.Name = "txtCodigoVendedor";
            this.txtCodigoVendedor.Size = new System.Drawing.Size(39, 22);
            this.txtCodigoVendedor.TabIndex = 130;
            this.superValidator1.SetValidator1(this.txtCodigoVendedor, this.requiredFieldValidator4);
            this.txtCodigoVendedor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigoVendedor_KeyDown);
            this.txtCodigoVendedor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCodigoVendedor_KeyUp);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(9, 109);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 13);
            this.label14.TabIndex = 129;
            this.label14.Text = "VENDEDOR (F1) :";
            // 
            // btnImprimirTicket
            // 
            this.btnImprimirTicket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnImprimirTicket.ImageIndex = 7;
            this.btnImprimirTicket.ImageList = this.imageList2;
            this.btnImprimirTicket.Location = new System.Drawing.Point(420, 15);
            this.btnImprimirTicket.Name = "btnImprimirTicket";
            this.btnImprimirTicket.Size = new System.Drawing.Size(75, 31);
            this.btnImprimirTicket.TabIndex = 128;
            this.btnImprimirTicket.Text = "Tickets";
            this.btnImprimirTicket.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnImprimirTicket.UseVisualStyleBackColor = true;
            this.btnImprimirTicket.Visible = false;
            this.btnImprimirTicket.Click += new System.EventHandler(this.btnImprimirTicket_Click);
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "Write Document.png");
            this.imageList2.Images.SetKeyName(1, "New Document.png");
            this.imageList2.Images.SetKeyName(2, "Remove Document.png");
            this.imageList2.Images.SetKeyName(3, "document-print.png");
            this.imageList2.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
            this.imageList2.Images.SetKeyName(5, "exit.png");
            this.imageList2.Images.SetKeyName(6, "barcode-29485.jpg");
            this.imageList2.Images.SetKeyName(7, "images.png");
            // 
            // txtPDescuento
            // 
            this.txtPDescuento.Location = new System.Drawing.Point(513, 180);
            this.txtPDescuento.Name = "txtPDescuento";
            this.txtPDescuento.Size = new System.Drawing.Size(76, 22);
            this.txtPDescuento.TabIndex = 126;
            this.txtPDescuento.Tag = "7";
            this.txtPDescuento.Visible = false;
            this.txtPDescuento.TextChanged += new System.EventHandler(this.txtPDescuento_TextChanged);
            // 
            // txtBruto
            // 
            this.txtBruto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBruto.Location = new System.Drawing.Point(513, 113);
            this.txtBruto.Name = "txtBruto";
            this.txtBruto.ReadOnly = true;
            this.txtBruto.Size = new System.Drawing.Size(78, 22);
            this.txtBruto.TabIndex = 122;
            this.txtBruto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.superValidator1.SetValidator1(this.txtBruto, this.customValidator6);
            // 
            // txtDscto
            // 
            this.txtDscto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDscto.Location = new System.Drawing.Point(513, 152);
            this.txtDscto.Name = "txtDscto";
            this.txtDscto.ReadOnly = true;
            this.txtDscto.Size = new System.Drawing.Size(75, 22);
            this.txtDscto.TabIndex = 123;
            this.txtDscto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(510, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 125;
            this.label2.Text = "Descuento :";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(510, 97);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 124;
            this.label13.Text = "Bruto :";
            // 
            // pbCapchatS
            // 
            this.pbCapchatS.Location = new System.Drawing.Point(386, 160);
            this.pbCapchatS.Name = "pbCapchatS";
            this.pbCapchatS.Size = new System.Drawing.Size(109, 38);
            this.pbCapchatS.TabIndex = 121;
            this.pbCapchatS.TabStop = false;
            this.pbCapchatS.Visible = false;
            // 
            // txtSunat_Capchat
            // 
            this.txtSunat_Capchat.Location = new System.Drawing.Point(406, 135);
            this.txtSunat_Capchat.Name = "txtSunat_Capchat";
            this.txtSunat_Capchat.Size = new System.Drawing.Size(89, 22);
            this.txtSunat_Capchat.TabIndex = 120;
            this.txtSunat_Capchat.Visible = false;
            // 
            // dtpFechaPago
            // 
            this.dtpFechaPago.Enabled = false;
            this.dtpFechaPago.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaPago.Location = new System.Drawing.Point(270, 163);
            this.dtpFechaPago.Name = "dtpFechaPago";
            this.dtpFechaPago.Size = new System.Drawing.Size(81, 22);
            this.dtpFechaPago.TabIndex = 119;
            this.dtpFechaPago.Tag = "16";
            this.dtpFechaPago.Visible = false;
            // 
            // cmbFormaPago
            // 
            this.cmbFormaPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFormaPago.FormattingEnabled = true;
            this.cmbFormaPago.Location = new System.Drawing.Point(114, 163);
            this.cmbFormaPago.Name = "cmbFormaPago";
            this.cmbFormaPago.Size = new System.Drawing.Size(150, 20);
            this.cmbFormaPago.TabIndex = 118;
            this.cmbFormaPago.Tag = "16";
            this.cmbFormaPago.Visible = false;
            this.cmbFormaPago.SelectionChangeCommitted += new System.EventHandler(this.cmbFormaPago_SelectionChangeCommitted);
            // 
            // txtDireccion
            // 
            this.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDireccion.Enabled = false;
            this.txtDireccion.Location = new System.Drawing.Point(114, 75);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.ReadOnly = true;
            this.txtDireccion.Size = new System.Drawing.Size(381, 22);
            this.txtDireccion.TabIndex = 117;
            this.txtDireccion.Tag = "21";
            // 
            // txtNombreCliente
            // 
            this.txtNombreCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreCliente.Enabled = false;
            this.txtNombreCliente.Location = new System.Drawing.Point(114, 48);
            this.txtNombreCliente.Name = "txtNombreCliente";
            this.txtNombreCliente.Size = new System.Drawing.Size(381, 22);
            this.txtNombreCliente.TabIndex = 116;
            this.txtNombreCliente.Tag = "3";
            this.superValidator1.SetValidator1(this.txtNombreCliente, this.requiredFieldValidator3);
            this.txtNombreCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombreCliente_KeyPress);
            // 
            // txtCodCliente
            // 
            this.txtCodCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.txtCodCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodCliente.Location = new System.Drawing.Point(114, 21);
            this.txtCodCliente.Name = "txtCodCliente";
            this.txtCodCliente.Size = new System.Drawing.Size(109, 22);
            this.txtCodCliente.TabIndex = 115;
            this.txtCodCliente.Tag = "5";
            this.txtCodCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.superValidator1.SetValidator1(this.txtCodCliente, this.requiredFieldValidator2);
            this.txtCodCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodCliente_KeyPress);
            this.txtCodCliente.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCodCliente_KeyUp);
            // 
            // chkVentaDsctoGlobal
            // 
            this.chkVentaDsctoGlobal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkVentaDsctoGlobal.AutoSize = true;
            // 
            // 
            // 
            this.chkVentaDsctoGlobal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkVentaDsctoGlobal.Location = new System.Drawing.Point(513, 78);
            this.chkVentaDsctoGlobal.Name = "chkVentaDsctoGlobal";
            this.chkVentaDsctoGlobal.Size = new System.Drawing.Size(96, 17);
            this.chkVentaDsctoGlobal.TabIndex = 114;
            this.chkVentaDsctoGlobal.Text = "Vta. Descuento";
            this.chkVentaDsctoGlobal.Visible = false;
            this.chkVentaDsctoGlobal.CheckedChanged += new System.EventHandler(this.chkVentaDsctoGlobal_CheckedChanged);
            this.chkVentaDsctoGlobal.Click += new System.EventHandler(this.chkVentaDsctoGlobal_Click);
            // 
            // chkVentaGratuita
            // 
            this.chkVentaGratuita.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkVentaGratuita.AutoSize = true;
            // 
            // 
            // 
            this.chkVentaGratuita.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkVentaGratuita.Location = new System.Drawing.Point(513, 54);
            this.chkVentaGratuita.Name = "chkVentaGratuita";
            this.chkVentaGratuita.Size = new System.Drawing.Size(83, 17);
            this.chkVentaGratuita.TabIndex = 111;
            this.chkVentaGratuita.Text = "Vta. Gratuita";
            this.chkVentaGratuita.Visible = false;
            this.chkVentaGratuita.CheckedChanged += new System.EventHandler(this.chkVentaGratuita_CheckedChanged);
            this.chkVentaGratuita.Click += new System.EventHandler(this.chkVentaGratuita_Click);
            // 
            // dtpFecha
            // 
            this.dtpFecha.Location = new System.Drawing.Point(114, 135);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(237, 22);
            this.dtpFecha.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(14, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "TIPO DE PAGO  :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "FECHA DOC.      :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Nº DOC. (F1)   :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "DIRECCION       :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "CLIENTE           :";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtinafectas);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.txtexoneradas);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.txtgratuitas);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.txtgravadas);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.txtPrecioVenta);
            this.panel2.Controls.Add(this.txtIGV);
            this.panel2.Controls.Add(this.txtValorVenta);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Enabled = false;
            this.panel2.Location = new System.Drawing.Point(883, 323);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(283, 196);
            this.panel2.TabIndex = 8;
            // 
            // txtinafectas
            // 
            this.txtinafectas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtinafectas.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtinafectas.Location = new System.Drawing.Point(204, 37);
            this.txtinafectas.Name = "txtinafectas";
            this.txtinafectas.ReadOnly = true;
            this.txtinafectas.Size = new System.Drawing.Size(67, 18);
            this.txtinafectas.TabIndex = 46;
            this.txtinafectas.Tag = "7";
            this.txtinafectas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(154, 40);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 12);
            this.label26.TabIndex = 44;
            this.label26.Tag = "7";
            this.label26.Text = "Inafectas :";
            // 
            // txtexoneradas
            // 
            this.txtexoneradas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtexoneradas.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexoneradas.Location = new System.Drawing.Point(204, 9);
            this.txtexoneradas.Name = "txtexoneradas";
            this.txtexoneradas.ReadOnly = true;
            this.txtexoneradas.Size = new System.Drawing.Size(67, 18);
            this.txtexoneradas.TabIndex = 45;
            this.txtexoneradas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(144, 12);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(59, 12);
            this.label32.TabIndex = 43;
            this.label32.Text = "Exoneradas :";
            // 
            // txtgratuitas
            // 
            this.txtgratuitas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtgratuitas.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgratuitas.Location = new System.Drawing.Point(65, 38);
            this.txtgratuitas.Name = "txtgratuitas";
            this.txtgratuitas.ReadOnly = true;
            this.txtgratuitas.Size = new System.Drawing.Size(67, 18);
            this.txtgratuitas.TabIndex = 42;
            this.txtgratuitas.Tag = "7";
            this.txtgratuitas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(16, 41);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(48, 12);
            this.label19.TabIndex = 40;
            this.label19.Tag = "7";
            this.label19.Text = "Gratuitas :";
            // 
            // txtgravadas
            // 
            this.txtgravadas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtgravadas.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgravadas.Location = new System.Drawing.Point(65, 10);
            this.txtgravadas.Name = "txtgravadas";
            this.txtgravadas.ReadOnly = true;
            this.txtgravadas.Size = new System.Drawing.Size(67, 18);
            this.txtgravadas.TabIndex = 41;
            this.txtgravadas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(13, 13);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(51, 12);
            this.label22.TabIndex = 39;
            this.label22.Text = "Gravadas :";
            // 
            // txtPrecioVenta
            // 
            this.txtPrecioVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrecioVenta.Location = new System.Drawing.Point(145, 151);
            this.txtPrecioVenta.Name = "txtPrecioVenta";
            this.txtPrecioVenta.ReadOnly = true;
            this.txtPrecioVenta.Size = new System.Drawing.Size(122, 22);
            this.txtPrecioVenta.TabIndex = 8;
            this.txtPrecioVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.superValidator1.SetValidator1(this.txtPrecioVenta, this.customValidator5);
            // 
            // txtIGV
            // 
            this.txtIGV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIGV.Location = new System.Drawing.Point(145, 115);
            this.txtIGV.Name = "txtIGV";
            this.txtIGV.ReadOnly = true;
            this.txtIGV.Size = new System.Drawing.Size(122, 22);
            this.txtIGV.TabIndex = 7;
            this.txtIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtValorVenta
            // 
            this.txtValorVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValorVenta.Location = new System.Drawing.Point(145, 80);
            this.txtValorVenta.Name = "txtValorVenta";
            this.txtValorVenta.ReadOnly = true;
            this.txtValorVenta.Size = new System.Drawing.Size(122, 22);
            this.txtValorVenta.TabIndex = 6;
            this.txtValorVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(48, 155);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 16);
            this.label10.TabIndex = 2;
            this.label10.Text = "TOTAL :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(72, 119);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "IGV :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "SUBTOTAL :";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label11);
            this.panel3.Location = new System.Drawing.Point(883, 532);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(283, 76);
            this.panel3.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(5, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 42);
            this.label11.TabIndex = 10;
            this.label11.Text = "00.00";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbDocumento
            // 
            this.lbDocumento.AutoSize = true;
            this.lbDocumento.Font = new System.Drawing.Font("Segoe UI Semibold", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDocumento.Location = new System.Drawing.Point(87, 20);
            this.lbDocumento.Name = "lbDocumento";
            this.lbDocumento.Size = new System.Drawing.Size(220, 50);
            this.lbDocumento.TabIndex = 42;
            this.lbDocumento.Tag = "22";
            this.lbDocumento.Text = "Documento";
            this.lbDocumento.Visible = false;
            // 
            // txtDocRef
            // 
            this.txtDocRef.BackColor = System.Drawing.Color.PeachPuff;
            this.txtDocRef.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDocRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocRef.Location = new System.Drawing.Point(25, 26);
            this.txtDocRef.Multiline = true;
            this.txtDocRef.Name = "txtDocRef";
            this.txtDocRef.Size = new System.Drawing.Size(56, 33);
            this.txtDocRef.TabIndex = 40;
            this.txtDocRef.Tag = "10";
            this.txtDocRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDocRef.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocRef_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 13);
            this.label12.TabIndex = 41;
            this.label12.Text = "Doc. Ref.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(874, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 42);
            this.label1.TabIndex = 43;
            this.label1.Tag = "22";
            this.label1.Text = "-";
            this.label1.Visible = false;
            // 
            // txtSerie
            // 
            this.txtSerie.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSerie.ForeColor = System.Drawing.Color.Red;
            this.txtSerie.Location = new System.Drawing.Point(752, 9);
            this.txtSerie.Margin = new System.Windows.Forms.Padding(5);
            this.txtSerie.Multiline = true;
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.ReadOnly = true;
            this.txtSerie.Size = new System.Drawing.Size(122, 60);
            this.txtSerie.TabIndex = 45;
            this.txtSerie.Text = "000";
            this.txtSerie.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSerie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSerie_KeyDown);
            // 
            // txtPedido
            // 
            this.txtPedido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPedido.ForeColor = System.Drawing.Color.Red;
            this.txtPedido.Location = new System.Drawing.Point(907, 9);
            this.txtPedido.Multiline = true;
            this.txtPedido.Name = "txtPedido";
            this.txtPedido.ReadOnly = true;
            this.txtPedido.Size = new System.Drawing.Size(258, 60);
            this.txtPedido.TabIndex = 46;
            this.txtPedido.Text = "00000000";
            this.txtPedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            this.highlighter1.FocusHighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // superValidator1
            // 
            this.superValidator1.ContainerControl = this;
            this.superValidator1.ErrorProvider = this.errorProvider1;
            this.superValidator1.Highlighter = this.highlighter1;
            // 
            // requiredFieldValidator1
            // 
            this.requiredFieldValidator1.ErrorMessage = "DEBE SELECCIONAR UN VENDEDOR";
            this.requiredFieldValidator1.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // requiredFieldValidator4
            // 
            this.requiredFieldValidator4.ErrorMessage = "Your error message here.";
            this.requiredFieldValidator4.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // customValidator6
            // 
            this.customValidator6.ErrorMessage = "Your error message here.";
            this.customValidator6.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator6.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator6_ValidateValue);
            // 
            // requiredFieldValidator3
            // 
            this.requiredFieldValidator3.ErrorMessage = "SELECCIONE UN CLIENTE";
            this.requiredFieldValidator3.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // requiredFieldValidator2
            // 
            this.requiredFieldValidator2.ErrorMessage = "SELECCIONE UN CLIENTE";
            this.requiredFieldValidator2.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // customValidator5
            // 
            this.customValidator5.ErrorMessage = "Your error message here.";
            this.customValidator5.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator5.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator5_ValidateValue);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
            // 
            // txtCodigoBarras
            // 
            this.txtCodigoBarras.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoBarras.Location = new System.Drawing.Point(611, 10);
            this.txtCodigoBarras.Multiline = true;
            this.txtCodigoBarras.Name = "txtCodigoBarras";
            this.txtCodigoBarras.ReadOnly = true;
            this.txtCodigoBarras.Size = new System.Drawing.Size(126, 61);
            this.txtCodigoBarras.TabIndex = 129;
            this.txtCodigoBarras.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnSalir);
            this.groupBox6.Controls.Add(this.btnCancelarOV);
            this.groupBox6.Controls.Add(this.groupBox5);
            this.groupBox6.Controls.Add(this.btnGuardaOV);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(630, 438);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(244, 176);
            this.groupBox6.TabIndex = 130;
            this.groupBox6.TabStop = false;
            // 
            // btnCancelarOV
            // 
            this.btnCancelarOV.Enabled = false;
            this.btnCancelarOV.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnCancelarOV.FlatAppearance.BorderSize = 2;
            this.btnCancelarOV.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelarOV.Image")));
            this.btnCancelarOV.Location = new System.Drawing.Point(134, 121);
            this.btnCancelarOV.Name = "btnCancelarOV";
            this.btnCancelarOV.Size = new System.Drawing.Size(104, 43);
            this.btnCancelarOV.TabIndex = 133;
            this.btnCancelarOV.Text = "CANCELA OV F9";
            this.btnCancelarOV.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelarOV.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelarOV.UseVisualStyleBackColor = true;
            this.btnCancelarOV.Visible = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.groupBox5.Controls.Add(this.txttasa);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.lbLineaCredito);
            this.groupBox5.Controls.Add(this.txtLineaCredito);
            this.groupBox5.Controls.Add(this.txtLineaCreditoUso);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.txtLineaCreditoDisponible);
            this.groupBox5.Enabled = false;
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(6, 10);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(232, 108);
            this.groupBox5.TabIndex = 132;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Condiciones de Crédito:";
            // 
            // txttasa
            // 
            this.txttasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttasa.Location = new System.Drawing.Point(104, 82);
            this.txttasa.Name = "txttasa";
            this.txttasa.ReadOnly = true;
            this.txttasa.Size = new System.Drawing.Size(124, 18);
            this.txttasa.TabIndex = 106;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(30, 85);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 12);
            this.label30.TabIndex = 105;
            this.label30.Text = "Tasa de Interés:";
            // 
            // lbLineaCredito
            // 
            this.lbLineaCredito.AutoSize = true;
            this.lbLineaCredito.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLineaCredito.Location = new System.Drawing.Point(7, 20);
            this.lbLineaCredito.Name = "lbLineaCredito";
            this.lbLineaCredito.Size = new System.Drawing.Size(94, 12);
            this.lbLineaCredito.TabIndex = 85;
            this.lbLineaCredito.Text = "Línea de Crédito (S/.):";
            // 
            // txtLineaCredito
            // 
            this.txtLineaCredito.Enabled = false;
            this.txtLineaCredito.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineaCredito.Location = new System.Drawing.Point(104, 17);
            this.txtLineaCredito.Name = "txtLineaCredito";
            this.txtLineaCredito.ReadOnly = true;
            this.txtLineaCredito.Size = new System.Drawing.Size(124, 18);
            this.txtLineaCredito.TabIndex = 84;
            // 
            // txtLineaCreditoUso
            // 
            this.txtLineaCreditoUso.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineaCreditoUso.Location = new System.Drawing.Point(104, 61);
            this.txtLineaCreditoUso.Name = "txtLineaCreditoUso";
            this.txtLineaCreditoUso.ReadOnly = true;
            this.txtLineaCreditoUso.Size = new System.Drawing.Size(124, 18);
            this.txtLineaCreditoUso.TabIndex = 98;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 40);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 12);
            this.label23.TabIndex = 95;
            this.label23.Text = "Línea Disponible (S/.):";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(8, 64);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(93, 12);
            this.label25.TabIndex = 97;
            this.label25.Text = "Línea C. en Uso (S/.):";
            // 
            // txtLineaCreditoDisponible
            // 
            this.txtLineaCreditoDisponible.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineaCreditoDisponible.Location = new System.Drawing.Point(104, 40);
            this.txtLineaCreditoDisponible.Name = "txtLineaCreditoDisponible";
            this.txtLineaCreditoDisponible.ReadOnly = true;
            this.txtLineaCreditoDisponible.Size = new System.Drawing.Size(124, 18);
            this.txtLineaCreditoDisponible.TabIndex = 96;
            // 
            // btVendedor
            // 
            this.btVendedor.DefaultBalloonWidth = 300;
            this.btVendedor.InitialDelay = 20;
            // 
            // lblCantidadProductos
            // 
            this.lblCantidadProductos.AutoSize = true;
            this.lblCantidadProductos.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCantidadProductos.Location = new System.Drawing.Point(644, 404);
            this.lblCantidadProductos.Name = "lblCantidadProductos";
            this.lblCantidadProductos.Size = new System.Drawing.Size(227, 25);
            this.lblCantidadProductos.TabIndex = 131;
            this.lblCantidadProductos.Text = "Productos Agregados: 0";
            // 
            // frmOrdenVenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1171, 622);
            this.Controls.Add(this.lblCantidadProductos);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.txtCodigoBarras);
            this.Controls.Add(this.txtPedido);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSerie);
            this.Controls.Add(this.lbDocumento);
            this.Controls.Add(this.txtDocRef);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmOrdenVenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "     ";
            this.Load += new System.EventHandler(this.frmOrdenVenta_Load);
            this.Shown += new System.EventHandler(this.frmOrdenVenta_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmOrdenVenta_KeyDown);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalle)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCapchatS)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnEditaOV;
        private System.Windows.Forms.Button btnAnulaOV;
        private System.Windows.Forms.Button btnDeleteItem;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.RadioButton chkFactura;
        private System.Windows.Forms.RadioButton chkBoleta;
        private System.Windows.Forms.Button btnInicioOV;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnGuardaOV;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label11;
        public CheckBoxX chkVentaGratuita;
        public CheckBoxX chkVentaDsctoGlobal;
        private System.Windows.Forms.TextBox txtNombreCliente;
        private System.Windows.Forms.TextBox txtCodCliente;
        public System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.DateTimePicker dtpFechaPago;
        private System.Windows.Forms.ComboBox cmbFormaPago;
        private System.Windows.Forms.Label lbDocumento;
        public System.Windows.Forms.TextBox txtDocRef;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSunat_Capchat;
        private System.Windows.Forms.PictureBox pbCapchatS;
        public System.Windows.Forms.DataGridView dgvDetalle;
        private System.Windows.Forms.TextBox txtPrecioVenta;
        private System.Windows.Forms.TextBox txtIGV;
        private System.Windows.Forms.TextBox txtValorVenta;
        private System.Windows.Forms.TextBox txtPDescuento;
        private System.Windows.Forms.TextBox txtBruto;
        private System.Windows.Forms.TextBox txtDscto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSerie;
        private System.Windows.Forms.TextBox txtPedido;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
        private DevComponents.DotNetBar.Validator.SuperValidator superValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator5;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator6;
        private System.Windows.Forms.TextBox txtinafectas;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtexoneradas;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtgratuitas;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtgravadas;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnImprimirTicket;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.TextBox txtCodigoBarras;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txttasa;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lbLineaCredito;
        private System.Windows.Forms.TextBox txtLineaCredito;
        private System.Windows.Forms.TextBox txtLineaCreditoUso;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtLineaCreditoDisponible;
        private System.Windows.Forms.Button btnCancelarOV;
        private Reportes.clsReportes.CachedCRCuotasPrestamo cachedCRCuotasPrestamo1;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox txtCodigoVendedor;
		private System.Windows.Forms.TextBox txtNombreVendedor;
		private DevComponents.DotNetBar.BalloonTip btVendedor;
		public System.Windows.Forms.Label lblCantidadProductos;
		private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator1;
		private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator3;
		private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator2;
		private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator4;
		private System.Windows.Forms.DataGridViewTextBoxColumn coddetalle;
		private System.Windows.Forms.DataGridViewTextBoxColumn codproducto;
		private System.Windows.Forms.DataGridViewTextBoxColumn referencia;
		private System.Windows.Forms.DataGridViewTextBoxColumn descripcion;
		private System.Windows.Forms.DataGridViewTextBoxColumn codunidad;
		private System.Windows.Forms.DataGridViewTextBoxColumn unidad;
		private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
		private System.Windows.Forms.DataGridViewTextBoxColumn preciounit;
		private System.Windows.Forms.DataGridViewTextBoxColumn importe;
		private System.Windows.Forms.DataGridViewTextBoxColumn dscto1;
		private System.Windows.Forms.DataGridViewTextBoxColumn dscto2;
		private System.Windows.Forms.DataGridViewTextBoxColumn dscto3;
		private System.Windows.Forms.DataGridViewTextBoxColumn montodscto;
		private System.Windows.Forms.DataGridViewTextBoxColumn valorventa;
		private System.Windows.Forms.DataGridViewTextBoxColumn igv;
		private System.Windows.Forms.DataGridViewTextBoxColumn precioventa;
		private System.Windows.Forms.DataGridViewTextBoxColumn valoreal;
		private System.Windows.Forms.DataGridViewTextBoxColumn precioreal;
		private System.Windows.Forms.DataGridViewTextBoxColumn precioconigv;
		private System.Windows.Forms.DataGridViewTextBoxColumn valorpromedio;
		private System.Windows.Forms.DataGridViewTextBoxColumn Tipoarticulo;
		private System.Windows.Forms.DataGridViewTextBoxColumn Tipoimpuesto;
		private System.Windows.Forms.DataGridViewTextBoxColumn codalmacen;
		private System.Windows.Forms.DataGridViewTextBoxColumn almacen;
		private System.Windows.Forms.DataGridViewTextBoxColumn TipoUnidad;
		private System.Windows.Forms.DataGridViewTextBoxColumn CodEmpresa;
	}
}