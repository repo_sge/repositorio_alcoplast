﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using Tesseract;
using System.Text.RegularExpressions;
using AForge.Imaging.Filters;
using AForge;
using SIGEFA.Reportes;
using SIGEFA.Reportes.clsReportes;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;

namespace SIGEFA.Formularios
{
    public partial class frmOrdenVenta : Office2007Form
    {
        clsAdmTipoDocumento Admdoc = new clsAdmTipoDocumento();
        public List<clsDetallePedido> detalle = new List<clsDetallePedido>();
        clsReporteCodigoBarras ds = new clsReporteCodigoBarras();
        clsTipoDocumento doc = new clsTipoDocumento();
        clsAdmCliente AdmCli = new clsAdmCliente();
        clsCliente cli = new clsCliente();
        clsAdmFormaPago AdmPago = new clsAdmFormaPago();
        clsFormaPago fpago = new clsFormaPago();
        clsAdmPedido AdmPedido = new clsAdmPedido();
        clsConsultasExternas ext = new clsConsultasExternas();
        clsPedido pedido = new clsPedido();
        clsValidar ok = new clsValidar();
        clsSerie ser = new clsSerie();
        clsAdmSerie AdmSerie = new clsAdmSerie();
        public Int32 CodSerie, manual = 0;
        Sunat MyInfoSunat;
        Reniec MyInfoReniec;
        IntRange red = new IntRange(0, 255);
        IntRange green = new IntRange(0, 255);
        IntRange blue = new IntRange(0, 255);

        //Necesitamos esto para almacenar el ultimo checkbox en el que se hizo clic
        private CheckBoxX lastChecked;

        public Int32 CodCliente, CodDocumento, Tipo, Proceso = 0;
        public Decimal montogratuitas, montogravadas, montoexoneradas = 0, montoinafectas = 0;
        public String NombreCliente, CodPedido;
        public Boolean banderagrabada, banderaexonerada, banderainafecta, banderadelete = false, bandera = false;

        public Int32 cuentaForm = 0;
        private clsAdmUsuario admUsuario = new clsAdmUsuario();
        public clsUsuario vendedor;

        private clsAdmProducto admProducto = new clsAdmProducto();
        public Decimal cantidadOriginalProducto;
        public mdi_Menu menu;

        /*
         * administradores de parametro
         */
        clsAdmParametro admParametro = new clsAdmParametro();
        String parametroMontoMáximoBoleta, parametroCodigoClienteGeneral;

        public frmOrdenVenta(mdi_Menu menu)
        {
            InitializeComponent();
            this.menu = menu;
        }

        public frmOrdenVenta()
        {
            InitializeComponent();
        }

        #region Metodos
        private void LimpiaForm()
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    c.Text = "";
                }

            }
            dgvDetalle.Rows.Clear();
            calculatotales();
        }

        private void CerrarOV()
        {
            if (dgvDetalle.Rows.Count > 0 && txtPedido.Text == "")
            {
                if (MessageBox.Show("Existen productos pendientes por vender..! \n Desea cancelar el pedido?", "Pedido Venta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
            else
            {
                this.Close();
            }
        }
        private void CargaNumeracionOV()
        {
            try
            {
                ser = AdmSerie.CargaSerieOV(frmLogin.iCodEmpresa, frmLogin.iCodAlmacen);
                if (ser != null)
                {
                    CodSerie = ser.CodSerie;
                    manual = Convert.ToInt32(ser.PreImpreso);
                    if (CodSerie != 0)
                    {
                        txtSerie.Text = ser.Serie;
                    }
                    if (CodSerie != 0) { }
                }
                else
                {
                    MessageBox.Show("No existe numeración para la orden de venta", "Orden de venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception a) { MessageBox.Show(a.Message); }
        }
        #endregion

        private void frmOrdenVenta_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F6:
                    this.activaPaneles(true);
                    //txtSerie.Focus();
                    //txtSerie.SelectAll();
                    sololectura(false);
                    Proceso = 1;
                    CargaFormaPagos();
                    if (Proceso == 1)
                    {
                        txtDocRef.Text = "OV";
                        txtCodCliente.Text = "C000001";
                        KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                        txtDocRef_KeyPress(txtDocRef, ee);
                        BuscaCliente();

                    }

                    LimpiaForm();
                    CargaNumeracionOV();
                    break;

                case Keys.F3:
                    if (bandera) { LlamarAddDetalle(); dgvDetalle.Focus(); }
                    break;

                case Keys.F2:
                    if (bandera) { btnDeleteItem_Click(sender, new EventArgs()); }
                    break;

                case Keys.F1:
                    if (bandera)
                    {
                        /*
						 * 21-06-2018: se tomo instancia de elementos del
						 * formulario que hacen uso de la tecla F1
						 */
                        if (ActiveControl is TextBox)
                        {
                            TextBox campoTexto = ActiveControl as TextBox;
                            if (campoTexto.Name == "txtCodigoVendedor")
                            {
                                txtCodigoVendedor_KeyUp(sender, e);
                            }

                            if (campoTexto.Name == "txtCodCliente")
                            {
                                txtCodCliente_KeyUp(sender, e);
                            }

                        }

                    }
                    break;

                case Keys.F8:
                    btnGuardaOV_Click(sender, e);
                    break;

                case Keys.F9:
                    this.Close();
                    break;

                case Keys.F4: // Editar orden de venta
                    sololectura2(false);
                    this.activaPaneles(true);
                    Proceso = 2; //Agregado para modificar OV
                    Recalcular();
                    break;
            }
        }
        private void LlamarAddDetalle()
        {
            if (Application.OpenForms["frmDetalleSalida"] != null)
            {
                Application.OpenForms["frmDetalleSalida"].Activate();
            }
            else
            {
                frmDetalleSalida form = new frmDetalleSalida();
                form.Procede = 3;
                form.Proceso = 1;
                form.Moneda = 1; // 1 nuevos soles
                form.codTipodoc = doc.CodTipoDocumento;
                form.ShowDialog(this);
            }
        }
        public void ChangeTextBoxText(clsDetalleNotaSalida detalle)
        {
            try
            {
                if (detalle != null)
                {
                    montogratuitas = detalle.Vmontogratuitas;
                    montogravadas = detalle.Vmontogravadas;
                    montoexoneradas = detalle.Vmontoexoneradas;
                    montoinafectas = detalle.Vmontoinafectas;
                    dgvDetalle.Rows.Add("0", detalle.CodProducto, detalle.Referencia, detalle.Descripcion,
                                            detalle.CodUnidad, detalle.Unidad, Convert.ToDouble(detalle.Cantidad),
                                            Convert.ToDouble(detalle.PrecioUnitario), detalle.Vbruto, 0, 0, 0, detalle.Vmontodescuento,
                                            detalle.Vvalorventa, detalle.Vigv, detalle.Vprecioventa, detalle.Vvalorreal, detalle.Vprecioreal, detalle.Vprecioventa, 0,
                                            detalle.TipoArticulo, detalle.TipoImpuesto, detalle.CodAlmacen,
                                            detalle.DescripcionAlmacen, detalle.TipoUnidad, detalle.CodEmpresa);
                }
            }
            catch (Exception a) { MessageBox.Show(a.Message); }
        }
        private void activaPaneles(Boolean estado)
        {
            groupBox2.Enabled = estado;
            btnAddItem.Enabled = estado;
            btnDeleteItem.Enabled = estado;
            btnEditaOV.Enabled = !estado;
            btnSalir.Enabled = estado;
            btnGuardaOV.Enabled = estado;
            groupBox4.Enabled = estado;
            panel2.Enabled = estado;
            btnInicioOV.Enabled = !estado;
            bandera = estado;// para saber si se activaron los controles de la venta
        }

        private void CargaCliente()
        {
            try
            {
                cli = AdmCli.MuestraCliente(CodCliente);

                if (cli.RucDni.Length == 8)
                {
                    chkBoleta.Checked = true;
                }
                else if (cli.RucDni.Length == 11)
                {
                    chkFactura.Checked = true;
                }

                txtCodCliente.Text = cli.RucDni;
                txtNombreCliente.Text = cli.RazonSocial;
                txtDireccion.Text = cli.DireccionLegal;

                btnAddItem.Focus();
            }
            catch (Exception a) { MessageBox.Show(a.Message); }

        }

        private Boolean BuscaCliente()
        {
            if (txtCodCliente.Text != "")
            {
                cli = AdmCli.BuscaCliente(txtCodCliente.Text, Tipo);
                if (cli != null)
                {
                    txtNombreCliente.Text = cli.RazonSocial;
                    CodCliente = cli.CodCliente;
                    txtDireccion.Text = cli.DireccionLegal;
                    txtCodCliente.Text = cli.RucDni;
                    if (cli.RucDni == "00000000")
                    {
                        txtNombreCliente.Enabled = true;
                    }

                    txtDocRef.Visible = false;
                    label12.Visible = false;
                    return true;
                }
                else
                {
                    txtNombreCliente.Text = "";
                    CodCliente = 0;
                    txtDireccion.Text = "";
                    return false;
                }
            }
            else { return false; }
        }

        private void CargaFormaPagos()
        {
            cmbFormaPago.DataSource = AdmPago.CargaFormaPagos(1);
            cmbFormaPago.DisplayMember = "descripcion";
            cmbFormaPago.ValueMember = "codFormaPago";
            cmbFormaPago.SelectedIndex = 0;

            cmbFormaPago_SelectionChangeCommitted(new object(), new EventArgs());
            cmbFormaPago.Visible = true;
            dtpFechaPago.Visible = true;
        }

        private Boolean BuscaTipoDocumento()
        {
            doc = Admdoc.BuscaTipoDocumento(txtDocRef.Text);
            if (doc != null)
            {
                CodDocumento = doc.CodTipoDocumento;
                lbDocumento.Text = doc.Descripcion;
                lbDocumento.Visible = true;
                label1.Visible = true;
                return true;
            }
            else
            {
                CodDocumento = 0;
                return false;
            }

        }

        private void frmOrdenVenta_Shown(object sender, EventArgs e)
        {
            CargaFormaPagos();
            if (Proceso == 1)
            {
                txtDocRef.Text = "OV";
                txtCodCliente.Text = "C000001";
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                txtDocRef_KeyPress(txtDocRef, ee);
                BuscaCliente();

            }
            else if (Proceso == 3) { btnSalir.Visible = true; btnSalir.Enabled = true; btnImprimirTicket.Enabled = true; }
        }

        private void txtDocRef_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtDocRef.Text != "")
                {
                    if (BuscaTipoDocumento())
                    {
                        //ProcessTabKey(true);
                    }
                    else
                    {
                        MessageBox.Show("Codigo de Documento no existe, Presione F1 para consultar la tabla de ayuda", "NOTA DE SALIDA", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void cmbFormaPago_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cmbFormaPago.SelectedIndex != -1)
            {
                fpago = AdmPago.CargaFormaPago(Convert.ToInt32(cmbFormaPago.SelectedValue));
                dtpFechaPago.Value = dtpFecha.Value.AddDays(fpago.Dias);
            }
        }

        private void txtCodCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            ok.enteros(e);
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                try
                {
                    //Cursor = Cursors.WaitCursor;
                    switch (this.txtCodCliente.Text.Length)
                    {
                        case 1:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo un digito Ingresado",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 2:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo dos digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 3:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo tres digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 4:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo cuatro digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 5:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo cinco digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 6:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo seis digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 7:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo siete digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 8:
                            cli = AdmCli.ConsultaCliente(txtCodCliente.Text);
                            if (cli != null)
                            {
                                CodCliente = cli.CodCliente;
                                txtNombreCliente.Text = cli.RazonSocial;
                                txtDireccion.Text = cli.DireccionLegal;

                                if (cli.Moneda == 1)
                                {
                                    txtLineaCredito.Text = String.Format("{0:#,##0.00}", cli.LineaCredito);
                                    txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoDisponible);
                                    txtLineaCreditoUso.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoUsado);
                                    txttasa.Text = String.Format("{0:#,##0.00}", cli.Tasa);
                                    lbLineaCredito.Text = "Línea de Crédito (S/.):";
                                    label23.Text = "Línea Disponible (S/.):";
                                    label25.Text = "Línea C. en Uso (S/.):";
                                    if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                                }
                                else
                                {
                                    txtLineaCredito.Text = String.Format("{0:#,##0.00}", (cli.LineaCredito / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                    txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoDisponible / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                    txtLineaCreditoUso.Text = Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoUsado / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                    lbLineaCredito.Text = "Línea de Crédito ($.):";
                                    label23.Text = "Línea Disponible ($.):";
                                    label25.Text = "Línea C. en Uso ($.):";
                                    if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                                }
                                if (cli.FormaPago != 0)
                                {
                                    cmbFormaPago.SelectedValue = 6; //cli.FormaPago  --   6 Contado
                                    EventArgs ee = new EventArgs();
                                    cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
                                }
                                else
                                {
                                    dtpFechaPago.Value = DateTime.Today;
                                }
                            }
                            else
                            {
                                CargarImagenReniec();
                                CargaDNI();
                                CodCliente = 0;
                            }
                            chkBoleta.Checked = true;
                            break;

                        case 9:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ingreso nueve digitos ",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            break;

                        case 10:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ingreso diez digitos ",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 11:
                            cli = AdmCli.ConsultaCliente(txtCodCliente.Text);
                            if (cli != null)
                            {
                                CodCliente = cli.CodCliente;
                                txtNombreCliente.Text = cli.RazonSocial;
                                txtDireccion.Text = cli.DireccionLegal;

                                if (cli.Moneda == 1)
                                {
                                    txtLineaCredito.Text = String.Format("{0:#,##0.00}", cli.LineaCredito);
                                    txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoDisponible);
                                    txtLineaCreditoUso.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoUsado);
                                    txttasa.Text = String.Format("{0:#,##0.00}", cli.Tasa);
                                    lbLineaCredito.Text = "Línea de Crédito (S/.):";
                                    label23.Text = "Línea Disponible (S/.):";
                                    label25.Text = "Línea C. en Uso (S/.):";
                                    if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                                }
                                else
                                {
                                    txtLineaCredito.Text = String.Format("{0:#,##0.00}", (cli.LineaCredito / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                    txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoDisponible / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                    txtLineaCreditoUso.Text = Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoUsado / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                    lbLineaCredito.Text = "Línea de Crédito ($.):";
                                    label23.Text = "Línea Disponible ($.):";
                                    label25.Text = "Línea C. en Uso ($.):";
                                    if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                                }
                                if (cli.FormaPago != 0)
                                {
                                    cmbFormaPago.SelectedValue = cli.FormaPago;
                                    EventArgs ee = new EventArgs();
                                    cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
                                }
                                else
                                {
                                    dtpFechaPago.Value = DateTime.Today;
                                }
                            }
                            else
                            {
                                CargarImagenSunat();
                                CargaRUC();
                                CodCliente = 0;
                            }
                            chkFactura.Checked = true;
                            break;

                        default:
                            ValidaLongitud();
                            break;
                    }

                    //cbFamilia.Select();                    

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    CargarImagenSunat();
                }
            }
        }

        private void ValidaLongitud()
        {
            if (txtCodCliente.Text.Length == 0)
            {
                MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ningun digito Ingresado",
                               "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
            }
            else if (txtCodCliente.Text.Length > 11)
            {
                MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ha Ingresado " + txtCodCliente.Text.Length + " Digitos",
                               "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                txtCodCliente.SelectAll();
                txtCodCliente.Focus();
            }
        }

        private void CargaDNI()
        {
            MyInfoReniec.GetInfo(this.txtCodCliente.Text, this.txtSunat_Capchat.Text);
            switch (MyInfoReniec.GetResul)
            {
                case Reniec.Resul.Ok:
                    limpiarSunat();
                    txtCodCliente.Text = MyInfoReniec.Dni;
                    String apellidos = MyInfoReniec.ApePaterno + " " + MyInfoReniec.ApeMaterno;
                    txtNombreCliente.Text = MyInfoReniec.Nombres + " " + apellidos;
                    break;
                case Reniec.Resul.NoResul:
                    limpiarSunat();
                    MessageBox.Show("No Existe DNI");
                    break;
                case Reniec.Resul.ErrorCapcha:
                    limpiarSunat();
                    MessageBox.Show("Ingrese imagen correctamente");
                    break;
                default:
                    MessageBox.Show("Error Desconocido");
                    break;
            }
            //Comentar esta linea para consultar multiples DNI usando un solo captcha.
            CargarImagenReniec();
        }

        #region RENIEC

        private void AplicacionFiltros()
        {
            Bitmap bmp = new Bitmap(pbCapchatS.Image);
            FiltroInvertir(bmp);
            ColorFiltros();
            Bitmap bmp1 = new Bitmap(pbCapchatS.Image);
            FiltroInvertir(bmp1);
            Bitmap bmp2 = new Bitmap(pbCapchatS.Image);
            FiltroSharpen(bmp2);
        }

        private void FiltroInvertir(Bitmap bmp)
        {
            IFilter Filtro = new Invert();
            Bitmap XImage = Filtro.Apply(bmp);
            pbCapchatS.Image = XImage;
        }

        private void ColorFiltros()
        {
            //Red Min - MAX
            red.Min = Math.Min(red.Max, byte.Parse("229"));
            red.Max = Math.Max(red.Min, byte.Parse("255"));
            //Verde Min - MAX
            green.Min = Math.Min(green.Max, byte.Parse("0"));
            green.Max = Math.Max(green.Min, byte.Parse("255"));
            //Azul Min - MAX
            blue.Min = Math.Min(blue.Max, byte.Parse("0"));
            blue.Max = Math.Max(blue.Min, byte.Parse("130"));
            ActualizarFiltro();
        }

        private void ActualizarFiltro()
        {
            ColorFiltering FiltroColor = new ColorFiltering();
            FiltroColor.Red = red;
            FiltroColor.Green = green;
            FiltroColor.Blue = blue;
            IFilter Filtro = FiltroColor;
            Bitmap bmp = new Bitmap(pbCapchatS.Image);
            Bitmap XImage = Filtro.Apply(bmp);
            pbCapchatS.Image = XImage;
        }

        private void FiltroSharpen(Bitmap bmp)
        {
            IFilter Filtro = new Sharpen();
            Bitmap XImage = Filtro.Apply(bmp);
            pbCapchatS.Image = XImage;
        }
        private void CargarImagenReniec()
        {
            try
            {
                if (MyInfoReniec == null)
                    MyInfoReniec = new Reniec();
                this.pbCapchatS.Image = MyInfoReniec.GetCapcha;
                AplicacionFiltros();
                LeerCaptchaReniec();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LeerCaptchaReniec()
        {
            using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
            {
                using (var image = new System.Drawing.Bitmap(pbCapchatS.Image))
                {
                    using (var pix = PixConverter.ToPix(image))
                    {
                        using (var page = engine.Process(pix))
                        {
                            var Porcentaje = String.Format("{0:P}", page.GetMeanConfidence());
                            string CaptchaTexto = page.GetText();
                            char[] eliminarChars = { '\n', ' ' };
                            CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars);
                            CaptchaTexto = CaptchaTexto.Replace(" ", string.Empty);
                            CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z0-9]+", string.Empty);
                            if (CaptchaTexto != string.Empty & CaptchaTexto.Length == 4)
                                txtSunat_Capchat.Text = CaptchaTexto.ToUpper();
                            //else
                            //    CargarImagenReniec();
                        }
                    }
                }
            }
        }

        #endregion

        #region metodos Sunat

        private void CargaRUC()
        {
            if (this.txtCodCliente.Text.Length == 11)
            {
                LeerDatos();
            }
        }
        private void LeerDatos()
        {
            //llamamos a los metodos de la libreria ConsultaReniec...
            MyInfoSunat.GetInfo(this.txtCodCliente.Text, this.txtSunat_Capchat.Text);
            switch (MyInfoSunat.GetResul)
            {
                case Sunat.Resul.Ok:
                    limpiarSunat();
                    txtCodCliente.Text = MyInfoSunat.Ruc;
                    txtDireccion.Text = MyInfoSunat.Direcion;
                    txtNombreCliente.Text = MyInfoSunat.RazonSocial;
                    Ciudad(MyInfoSunat.Direcion);
                    BloqueaDatos();
                    break;
                case Sunat.Resul.NoResul:
                    limpiarSunat();
                    MessageBox.Show("No Existe RUC");
                    break;
                case Sunat.Resul.ErrorCapcha:
                    limpiarSunat();
                    MessageBox.Show("Ingrese imagen correctamente");
                    break;
                default:
                    MessageBox.Show("Error Desconocido");
                    break;
            }
            //CargarImagenSunat();
        }
        private void CargarImagenSunat()
        {
            try
            {
                if (MyInfoSunat == null)
                    MyInfoSunat = new Sunat();
                this.pbCapchatS.Image = MyInfoSunat.GetCapcha;
                LeerCaptchaSunat();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LeerCaptchaSunat()
        {
            //string ruta = Directory.GetCurrentDirectory()+"\\tessdata";
            //string RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tessdata\\");
            try
            {
                using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
                {
                    using (var image = new System.Drawing.Bitmap(pbCapchatS.Image))
                    {
                        using (var pix = PixConverter.ToPix(image))
                        {
                            using (var page = engine.Process(pix))
                            {
                                var Porcentaje = String.Format("{0:P}", page.GetMeanConfidence());
                                string CaptchaTexto = page.GetText();
                                char[] eliminarChars = { '\n', ' ' };
                                CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars);
                                CaptchaTexto = CaptchaTexto.Replace(" ", string.Empty);
                                CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z]+", string.Empty);
                                if (CaptchaTexto != string.Empty & CaptchaTexto.Length == 4)
                                    txtSunat_Capchat.Text = CaptchaTexto.ToUpper();
                                else
                                    CargarImagenSunat();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Ciudad(string Direccion)
        {
            String[] array = Direccion.Split('-');
            if (array.Length > 1)
            {
                int a = array.Length;
                String DirTemp = array[a - 3].Trim();
                DirTemp = DirTemp.TrimEnd(' ');
                String[] ArrayDir = DirTemp.Split(' ');
                int i = ArrayDir.Length;
                //cbDepartamento.Text = ArrayDir[i - 1].Trim();
                //cbProvincia.Text = array[a - 2].Trim();
                //cbDistrito.Text = array[a - 1].Trim();
            }
        }
        private void limpiarSunat()
        {
            txtNombreCliente.Text = "";
            txtSunat_Capchat.Text = string.Empty;
        }
        private void BloqueaDatos()
        {
            /*txtRUC.ReadOnly = true; */
            txtDireccion.ReadOnly = true; txtNombreCliente.ReadOnly = true;
        }

        #endregion

        private void dgvDetalle_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (Proceso == 1 || Proceso == 3 || Proceso == 2)
            {
                Recalcular();
            }
        }

        private void calculatotales()
        {
            Double bruto = 0; Double descuen = 0; Double valor = 0;

            foreach (DataGridViewRow row in dgvDetalle.Rows)
            {
                bruto = bruto + Convert.ToDouble(row.Cells[importe.Name].Value);
                descuen = descuen + Convert.ToDouble(row.Cells[montodscto.Name].Value);
                valor = valor + Convert.ToDouble(row.Cells[valorventa.Name].Value);

                if (row.Index == dgvDetalle.CurrentRow.Index)
                {
                    if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "21" && banderadelete) // gratuitas
                    {
                        montogratuitas = montogratuitas - (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value));
                        montosventa();
                        banderadelete = false;
                    }

                    if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "10" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "11" && banderadelete ||
                        Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "12" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "13" && banderadelete ||
                        Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "14" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "15" && banderadelete ||
                        Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "16" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "17" && banderadelete)   // gravadas
                    {
                        montogravadas = montogravadas - (Convert.ToDecimal(row.Cells[valorventa.Name].Value));
                        montosventa();
                        banderadelete = false;
                    }

                    if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "20" && banderadelete) // exoneradas
                    {
                        montoexoneradas = montoexoneradas - (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value));
                        montosventa();
                        banderadelete = false;
                    }

                    if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "30" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "31" && banderadelete ||
                        Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "32" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "33" && banderadelete ||
                        Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "34" && banderadelete || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "35" && banderadelete ||
                        Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "36" && banderadelete) // inafectas
                    {
                        montoinafectas = montoinafectas - (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value));
                        montosventa();
                        banderadelete = false;
                    }
                }
            }
            txtBruto.Text = String.Format("{0:#,##0.00}", bruto);
            txtValorVenta.Text = String.Format("{0:#,##0.00}", valor);
            txtIGV.Text = String.Format("{0:#,##0.00}", bruto - descuen - valor);
            txtPrecioVenta.Text = String.Format("{0:#,##0.00}", bruto - descuen);
            txtPrecioVenta.Text = txtBruto.Text;
            label11.Text = txtBruto.Text;
            montosventa();
        }

        public void montosventa()
        {
            if (Proceso != 0)
            {
                if (montogravadas > 0)
                {
                    txtgravadas.Clear();
                    txtgravadas.Text = String.Format("{0:#,##0.00}", montogravadas);
                }
                else { txtgravadas.Text = String.Format("{0:#,##0.00}", 0); }

                if (montogratuitas > 0)
                {
                    txtgratuitas.Clear();
                    txtgratuitas.Text = String.Format("{0:#,##0.00}", montogratuitas);
                }
                else { txtgratuitas.Text = String.Format("{0:#,##0.00}", 0); }

                if (montoexoneradas > 0)
                {
                    txtexoneradas.Clear();
                    txtexoneradas.Text = String.Format("{0:#,##0.00}", montoexoneradas);
                }
                else { txtexoneradas.Text = String.Format("{0:#,##0.00}", 0); }

                if (montoinafectas > 0)
                {
                    txtinafectas.Clear();
                    txtinafectas.Text = String.Format("{0:#,##0.00}", montoinafectas);
                }
                else { txtinafectas.Text = String.Format("{0:#,##0.00}", 0); }
            }
        }

        private void Recalcular()
        {
            Double bruto = 0;
            Double descuen = 0;
            Double valor = 0;
            Double figv = 0;
            Double pven = 0;
            foreach (DataGridViewRow row in dgvDetalle.Rows)
            {
                bruto = bruto + Convert.ToDouble(row.Cells[importe.Name].Value);
                descuen = descuen + Convert.ToDouble(row.Cells[montodscto.Name].Value);
                valor = valor + Convert.ToDouble(row.Cells[valorventa.Name].Value);
                figv = figv + Convert.ToDouble(row.Cells[igv.Name].Value);
                pven = pven + Convert.ToDouble(row.Cells[precioventa.Name].Value);
            }
            txtBruto.Text = String.Format("{0:#,##0.00}", bruto);
            txtDscto.Text = String.Format("{0:#,##0.00}", descuen);
            txtValorVenta.Text = String.Format("{0:#,##0.00}", valor);
            txtIGV.Text = String.Format("{0:#,##0.00}", figv);
            txtPrecioVenta.Text = String.Format("{0:#,##0.00}", pven);
            label11.Text = txtBruto.Text;
            montosventa();
        }

        private void txtCantidadPago_KeyPress(object sender, KeyPressEventArgs e)
        {
            ok.SOLONumeros(sender, e);
        }

        private void dgvDetalle_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            //btnDeleteItem_Click(sender, new EventArgs());
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (bandera) { LlamarAddDetalle(); }
            }
            catch (Exception a) { MessageBox.Show(a.Message); }
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvDetalle.Rows.Count > 0 && dgvDetalle.CurrentCell.RowIndex > -1)
                {
                    if (Convert.ToInt32(dgvDetalle.CurrentRow.Cells[coddetalle.Name].Value) == 0)
                    {
                        banderadelete = true;
                        //calculatotales();
                        dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow);
                        //dgvDetalle.Rows.RemoveAt(dgvDetalle.CurrentCell.RowIndex);
                        calculatotales();
                    }
                    else
                    {
                        if (MessageBox.Show("¿Realmente desea eliminar el item seleccionado?", "Pedido Venta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {

                            if (AdmPedido.deletedetalle(Convert.ToInt32(dgvDetalle.CurrentRow.Cells[coddetalle.Name].Value)))
                            {
                                MessageBox.Show("El detalle se eliminó correctamente", "Pedido Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                banderadelete = true;
                                calculatotales();
                                dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow);
                                calculatotales();
                            }
                        }
                    }
                    lblCantidadProductos.Text = "Productos Agregados: " + dgvDetalle.RowCount;
                }
            }
            catch (Exception a) { MessageBox.Show(a.Message); }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            CerrarOV();
        }

        private void txtSerie_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Application.OpenForms["frmSerie"] != null)
                {
                    Application.OpenForms["frmSerie"].Activate();
                }
                else
                {
                    frmSerie form = new frmSerie();
                    form.Proceso = 3;
                    form.DocSeleccionado = CodDocumento;
                    form.ShowDialog();
                    ser = form.ser;
                    CodSerie = ser.CodSerie;
                    manual = Convert.ToInt32(ser.PreImpreso);
                    if (CodSerie != 0)
                    {
                        txtSerie.Text = ser.Serie;
                    }
                    if (CodSerie != 0) { }
                }
            }
        }

        private void btnGuardaOV_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (verificarPrecioVenta())
                {
                    if (dgvDetalle.RowCount == 0)
                    {
                        MessageBox.Show("No se puedo Guardar, no existen productos en el pedido!",
                                        "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnAddItem.Focus();
                    }
                    else if (superValidator1.Validate())
                    {
                        if (Proceso != 0)
                        {
                            
                            if (chkBoleta.Checked)
                            {

                                /**
                                 *  si el monto de la venta es mayor a 700
                                 *  y codigo de cliente es de clientes varios(publico general)
                                 */

                                if ((Convert.ToDouble(txtPrecioVenta.Text) > Convert.ToDouble(parametroMontoMáximoBoleta)) &&
                                    (cli.CodCliente == Convert.ToInt32(parametroCodigoClienteGeneral)))
                                {
                                    MessageBox.Show("EL MONTO DE LA VENTA SUPERA LOS " + parametroMontoMáximoBoleta +
                                                    " SOLES, " + "DEBE REGISTRAR LOS DATOS DE SU CLIENTE PARA " +
                                                    "GUARDAR EL COMPROBANTE", "VALIDACIÓN", MessageBoxButtons.OK,
                                                    MessageBoxIcon.Error);
                                    return;
                                }
                            }

                            pedido.CodAlmacen = frmLogin.iCodAlmacen;
                            if (CodCliente == 0)
                            {
                                ValidaCliente(txtCodCliente.Text);
                            }
                            pedido.CodCliente = CodCliente;
                            pedido.CodTipoDocumento = doc.CodTipoDocumento;

                            if (txtCodCliente.Text.ToString() == "00000000") { pedido.Nombrecliente = txtNombreCliente.Text; } else { pedido.Nombrecliente = txtNombreCliente.Text; }

                            pedido.Moneda = 1;//Convert.ToInt32(cmbMoneda.SelectedValue);
                            if (mdi_Menu.tc_hoy > 0)
                            {
                                pedido.TipoCambio = mdi_Menu.tc_hoy;
                            }
                            pedido.FechaPedido = dtpFecha.Value.Date;
                            pedido.FechaEntrega = dtpFecha.Value.Date;
                            pedido.FormaPago = Convert.ToInt32(cmbFormaPago.SelectedValue);
                            pedido.FechaPago = dtpFecha.Value.AddDays(fpago.Dias);
                            pedido.CodSerie = CodSerie;
                            pedido.Comentario = "";
                            pedido.SerieDoc = txtSerie.Text;
                            pedido.MontoBruto = Convert.ToDouble(txtBruto.Text);
                            pedido.MontoDscto = Convert.ToDouble(txtDscto.Text);
                            pedido.Igv = Convert.ToDouble(txtIGV.Text);
                            pedido.Total = Convert.ToDouble(txtPrecioVenta.Text);
                            /*
							 * cambiar el uso de este parámetro
							 * en lugar de usar el codigo del usuario autenticado
							 * tomar de una variable local
							 */
                            pedido.CodVendedor = vendedor.CodUsuario;//frmLogin.iCodUser;
                            pedido.CodUser = vendedor.CodUsuario;//frmLogin.iCodUser;
                            pedido.Estado = 1;

                            pedido.Gratuitas = montogratuitas;
                            pedido.Exoneradas = montoexoneradas;
                            pedido.Gravadas = montogravadas;
                            pedido.Inafectas = montoinafectas;
                            if (chkBoleta.Checked)
                            {
                                pedido.Boletafactura = 1;
                            }
                            else if (chkFactura.Checked)
                            {
                                pedido.Boletafactura = 2;
                            }
                            pedido.CodEmpresa = frmLogin.iCodEmpresa;
                            pedido.CodigoBarras = "";
                            pedido.CodigoBarrasCifrado = "";
                            // 1 venta grabada   2 venta exonerada    3 venta inafecta   4 venta gratuita    5 venta con descuento 
                            // 6 venta grabada + exonerada   7 venta grabada + inafecta
                            banderagrabada = false; banderaexonerada = false; banderainafecta = false;
                            foreach (DataGridViewRow row in dgvDetalle.Rows)
                            {
                                switch (Convert.ToInt32(row.Cells[Tipoimpuesto.Name].Value))
                                {
                                    case 10: banderagrabada = true; break;

                                    case 11: banderagrabada = true; break;

                                    case 12: banderagrabada = true; break;

                                    case 13: banderagrabada = true; break;

                                    case 14: banderagrabada = true; break;

                                    case 15: banderagrabada = true; break;

                                    case 16: banderagrabada = true; break;

                                    case 17: banderagrabada = true; break;

                                    case 20: banderaexonerada = true; break;

                                    case 30: banderainafecta = true; break;

                                    case 31: banderainafecta = true; break;

                                    case 32: banderainafecta = true; break;

                                    case 33: banderainafecta = true; break;

                                    case 34: banderainafecta = true; break;

                                    case 35: banderainafecta = true; break;

                                    case 36: banderainafecta = true; break;

                                }
                            }

                            if (chkVentaGratuita.Checked)
                            {
                                pedido.Tipoventa = 4; // venta gratuita
                            }
                            else if (chkVentaDsctoGlobal.Checked)
                            {
                                pedido.Tipoventa = 5; // venta con descuento Global
                            }
                            else if (banderagrabada == true && banderaexonerada == false && banderainafecta == false)
                            {
                                pedido.Tipoventa = 1; // venta gravada
                            }
                            else if (banderagrabada == false && banderaexonerada == true && banderainafecta == false)
                            {
                                pedido.Tipoventa = 2; // venta exonerada
                            }
                            else if (banderagrabada == false && banderaexonerada == false && banderainafecta == true)
                            {
                                pedido.Tipoventa = 3; // venta inafecta
                            }
                            else if (banderagrabada == true && banderaexonerada == true && banderainafecta == false)
                            {
                                pedido.Tipoventa = 6; // 
                            }
                            else if (banderagrabada == false && banderaexonerada == true && banderainafecta == true)
                            {
                                pedido.Tipoventa = 7; // 
                            }

                            // Para saber si la nota esta activa o anulada. El estado se podra cambiar en una ventana especifica para anular notas
                            if (Proceso == 1)
                            {
                                /*
								 * Insercion de cabecera y detalles en un solo 
								 * metodo
								 */
                                RecorreDetalle();
                                pedido.Detalle = detalle;

                                if (AdmPedido.insertarOrdenVenta(pedido))
                                {
                                    CodPedido = pedido.CodPedido;

                                    if (CodPedido != "")
                                    {
                                        Proceso = 3; frmOrdenVenta_Load(new object(), new EventArgs());
                                    }
                                    MessageBox.Show("Los datos se guardaron correctamente!\nPedido: " + Convert.ToInt32(pedido.Numeracion) + "\nTotal: " + pedido.Total + " Soles.", "Pedido Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    btnGuardaOV.Enabled = false;
                                    btnAddItem.Enabled = false;
                                    btnEditaOV.Enabled = false;
                                    btnDeleteItem.Enabled = false;
                                    txtBruto.Enabled = false;
                                    txtDscto.Enabled = false;
                                    sololectura(true);
                                    this.Close();
                                    menu.biOrdenVenta_Click(sender, e);
                                }
                                else
                                {
                                    MessageBox.Show("Ocurrió un error al registrar la operación",
                                                    "Orden de Venta", MessageBoxButtons.OK,
                                                    MessageBoxIcon.Error);
                                    return;
                                }
                                
                            }
                            else if (Proceso == 2)
                            {
                                if (AdmPedido.update(pedido))
                                {
                                    RecorreDetalle();
                                    foreach (clsDetallePedido det in detalle)
                                    {
                                        foreach (clsDetallePedido det1 in detalle)
                                        {
                                            if (det.CodDetallePedido == det1.CodDetallePedido && det1.CodDetallePedido != 0)
                                            {
                                                AdmPedido.updatedetalle(det1);
                                            }
                                        }
                                    }
                                    foreach (clsDetallePedido deta in detalle)
                                    {
                                        if (deta.CodDetallePedido == 0)
                                        {
                                            AdmPedido.insertdetalle(deta);
                                        }
                                    }
                                    CodPedido = pedido.CodPedido;
                                    CargaPedido();
                                    MessageBox.Show(
                                        "Los datos se guardaron correctamente!\nPedido: " +
                                        Convert.ToInt32(pedido.Numeracion) + "\nTotal: " + pedido.Total + " Soles.",
                                        "Pedido Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    sololectura(true);
                                    frmPedidosPendientes form =
                                        (frmPedidosPendientes)Application.OpenForms["frmPedidosPendientes"];
                                    if (form != null) form.CargaLista();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message.ToString());
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ValidaCliente(String rucdni)
        {
            cli = AdmCli.ConsultaCliente(rucdni);
            if (cli == null)
            {
                cli = new clsCliente();
                Int32 id = AdmCli.GetUltimoId() + 1;
                if (rucdni.Length == 8) { cli.Dni = rucdni; cli.DireccionEntrega = "S/D"; cli.DireccionLegal = "S/D"; }
                else if (rucdni.Length == 11) { cli.Ruc = rucdni; cli.DireccionEntrega = txtDireccion.Text; cli.DireccionLegal = txtDireccion.Text; }
                cli.Nombre = txtNombreCliente.Text;
                cli.RazonSocial = txtNombreCliente.Text;
                cli.CodigoPersonalizado = "C" + id.ToString().PadLeft(6, '0').Trim();
                cli.FormaPago = 6; //pago ala contado
                cli.Moneda = 1;
                cli.LineaCredito = 0;
                cli.LineaCreditoDisponible = 0;
                cli.Habilitado = true;
                cli.CodUser = frmLogin.iCodUser;

                if (AdmCli.insert(cli)) { CodCliente = cli.CodClienteNuevo; }
            }
        }

        private void CargaPedido()
        {
            try
            {
                pedido = AdmPedido.CargaPedido(Convert.ToInt32(CodPedido));
                if (pedido != null)
                {
                    //txtPedido.Text = pedido.Numeracion.ToString("0000");
                    doc.CodTipoDocumento = pedido.CodTipoDocumento;
                    pedido.CodCotizacion = 0;
                    txtSerie.Text = pedido.SerieDoc;
                    CodSerie = pedido.CodSerie;
                    txtPedido.Text = pedido.Numeracion.ToString().PadLeft(8, '0');
                    txtCodigoBarras.Text = pedido.CodigoBarrasCifrado;

                    if (pedido.Boletafactura == 1) // 1 dni
                    {
                        CodCliente = pedido.CodCliente;
                        cli.CodCliente = CodCliente;
                        txtCodCliente.Text = pedido.DNI;
                        if (pedido.Nombrecliente != "") { txtNombreCliente.Text = pedido.Nombrecliente; } else { txtNombreCliente.Text = pedido.Nombre; }
                        // txtNombreCliente.Enabled = false;
                        txtDireccion.Text = pedido.Direccion;

                    }
                    else
                    {
                        CodCliente = pedido.CodCliente;
                        cli.CodCliente = CodCliente;
                        txtCodCliente.Text = pedido.RUCCliente;
                        if (pedido.RazonSocialCliente != "") txtNombreCliente.Text = pedido.RazonSocialCliente;
                        else txtNombreCliente.Text = pedido.Nombre;
                        txtDireccion.Text = pedido.Direccion;
                    }

                    dtpFecha.Value = pedido.FechaPedido;

                    if (txtDocRef.Enabled)
                    {
                        CodDocumento = pedido.CodTipoDocumento;
                        txtDocRef.Text = pedido.SiglaDocumento;
                        lbDocumento.Text = pedido.DescripcionDocumento;
                    }

                    txtBruto.Text = String.Format("{0:#,##0.00}", pedido.MontoBruto);
                    txtDscto.Text = String.Format("{0:#,##0.00}", pedido.MontoDscto);
                    txtValorVenta.Text = String.Format("{0:#,##0.00}", pedido.Total - pedido.Igv);
                    txtIGV.Text = String.Format("{0:#,##0.00}", pedido.Igv);
                    txtPrecioVenta.Text = String.Format("{0:#,##0.00}", pedido.Total);
                    montogravadas = pedido.Gravadas;
                    montoexoneradas = pedido.Exoneradas;
                    montoinafectas = pedido.Inafectas;
                    montogratuitas = pedido.Gratuitas;
                    txtPedido.Text = pedido.Numeracion.ToString().PadLeft(8, '0');
                    label11.Text = txtPrecioVenta.Text;
                    montosventa();
                    CargaDetalle();
                    if (Proceso == 2)
                    {
                        pedido.Detalle = new List<clsDetallePedido>();
                        RecorreDetallePedido();

                    }

                    /*
					 * Cargar datos del vendedor
					 */
                    txtCodigoVendedor.Text = pedido.CodUser.ToString();
                    txtCodigoVendedor.Focus();
                    KeyEventArgs arg = new KeyEventArgs(Keys.Enter);
                    txtCodigoVendedor_KeyDown(new object(), arg);

                    /*
					 * Cargar cantidad de datos
					 */
                    lblCantidadProductos.Text = "Productos Agregados: " + dgvDetalle.RowCount;
                }
                else
                {
                    MessageBox.Show("El documento solicitado no existe", "Nota de Ingreso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RecorreDetallePedido()
        {
            pedido.Detalle.Clear();
            if (dgvDetalle.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvDetalle.Rows)
                {
                    añadedetallePedido(row);
                }
            }
        }

        private void añadedetallePedido(DataGridViewRow fila)
        {
            clsDetallePedido deta = new clsDetallePedido();
            deta.CodDetallePedido = Convert.ToInt32(fila.Cells[coddetalle.Name].Value);
            deta.CodProducto = Convert.ToInt32(fila.Cells[codproducto.Name].Value);
            deta.CodPedido = Convert.ToInt32(pedido.CodPedido);
            deta.CodAlmacen = frmLogin.iCodAlmacen;
            deta.UnidadIngresada = Convert.ToInt32(fila.Cells[codunidad.Name].Value);

            deta.Cantidad = Convert.ToDouble(fila.Cells[cantidad.Name].Value);

            deta.PrecioUnitario = Convert.ToDouble(fila.Cells[preciounit.Name].Value);
            deta.Subtotal = Convert.ToDouble(fila.Cells[importe.Name].Value);
            deta.Descuento1 = Convert.ToDouble(fila.Cells[dscto1.Name].Value);
            deta.Descuento2 = Convert.ToDouble(fila.Cells[dscto2.Name].Value);
            deta.Descuento3 = Convert.ToDouble(fila.Cells[dscto3.Name].Value);
            deta.Descuento3 = Convert.ToDouble(fila.Cells[dscto3.Name].Value);
            deta.Igv = Convert.ToDouble(fila.Cells[igv.Name].Value);
            deta.Importe = Convert.ToDouble(fila.Cells[precioventa.Name].Value);
            deta.PrecioReal = Convert.ToDouble(fila.Cells[precioreal.Name].Value);
            deta.ValoReal = Convert.ToDouble(fila.Cells[valoreal.Name].Value);
            deta.Precioigv = Convert.ToBoolean(Convert.ToInt32(fila.Cells[precioconigv.Name].Value));
            deta.Valorpromedio = Convert.ToDecimal(fila.Cells[valorpromedio.Name].Value);
            deta.CodUser = frmLogin.iCodUser;

            pedido.Detalle.Add(deta);
        }

        private void CargaDetalle()
        {
            DataTable newData = new DataTable();
            dgvDetalle.Rows.Clear();
            try
            {
                newData = AdmPedido.CargaDetalle(Convert.ToInt32(pedido.CodPedido));
                foreach (DataRow row in newData.Rows)
                {
                    if (Convert.ToInt32(row[25].ToString()) == 1 || Convert.ToInt32(row[25].ToString()) == 0)
                    {
                        dgvDetalle.Rows.Add(row[0].ToString(), row[1].ToString(), row[2].ToString(), row[3].ToString(), row[4].ToString(),
                        row[5].ToString(), row[6].ToString(), row[7].ToString(), row[8].ToString(), row[9].ToString(),
                        row[10].ToString(), row[11].ToString(), row[12].ToString(), row[13].ToString(), row[14].ToString(),
                        row[15].ToString(), row[16].ToString(), row[17].ToString(), row[18].ToString(),
                        row[19].ToString(), row[20].ToString(), row[21].ToString(), row[22].ToString(), row[23].ToString(), row[24].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RecorreDetalle()
        {
            detalle.Clear();
            if (dgvDetalle.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvDetalle.Rows)
                {
                    añadedetalle(row);
                }
            }
        }
        private void añadedetalle(DataGridViewRow fila)
        {
            clsDetallePedido deta = new clsDetallePedido();
            deta.CodDetallePedido = Convert.ToInt32(fila.Cells[coddetalle.Name].Value);
            deta.CodProducto = Convert.ToInt32(fila.Cells[codproducto.Name].Value);
            deta.CodPedido = Convert.ToInt32(pedido.CodPedido);
            deta.CodAlmacen = Convert.ToInt32(fila.Cells[codalmacen.Name].Value);
            deta.UnidadIngresada = Convert.ToInt32(fila.Cells[codunidad.Name].Value);
            deta.Cantidad = Convert.ToDouble(fila.Cells[cantidad.Name].Value);
            deta.PrecioUnitario = Convert.ToDouble(fila.Cells[preciounit.Name].Value);
            deta.Subtotal = Convert.ToDouble(fila.Cells[importe.Name].Value);
            deta.Descuento1 = Convert.ToDouble(fila.Cells[dscto1.Name].Value);
            deta.Descuento2 = 0;
            deta.Descuento3 = 0;
            deta.MontoDescuento = Convert.ToDouble(fila.Cells[montodscto.Name].Value);
            deta.Igv = Convert.ToDouble(fila.Cells[igv.Name].Value);
            deta.Importe = Convert.ToDouble(fila.Cells[precioventa.Name].Value);
            deta.PrecioReal = Convert.ToDouble(fila.Cells[precioreal.Name].Value);
            deta.ValoReal = Convert.ToDouble(fila.Cells[valoreal.Name].Value);
            deta.Precioigv = Convert.ToBoolean(Convert.ToInt32(fila.Cells[precioconigv.Name].Value));
            deta.Valorpromedio = Convert.ToDecimal(fila.Cells[valorpromedio.Name].Value);
            deta.CodUser = frmLogin.iCodUser;
            deta.Tipoimpuesto = fila.Cells[Tipoimpuesto.Name].Value.ToString();
            /*deta.CodEmpresa = Convert.ToInt32(fila.Cells[CodEmpresa.Name].Value);//desde esto toma valor nulo
            deta.Tipoimpuesto = fila.Cells[Tipoimpuesto.Name].Value.ToString();
            deta.TipoUnidad = Convert.ToInt32(fila.Cells[TipoUnidad.Name].Value);*/
            //deta.
            detalle.Add(deta);
        }
        public Boolean verificarPrecioVenta()
        {
            Boolean valor = false;
            if (mdi_Menu.MontoTopeBoleta > 0)
            {
                if (Convert.ToDecimal(txtPrecioVenta.Text) >= mdi_Menu.MontoTopeBoleta && CodCliente == 1)
                {
                    MessageBox.Show("Precio venta > S/. 700, registrar(DNI, datos completos del cliente) o seleccionar cliente para guardar pedido", "Advertencia",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    if (Application.OpenForms["frmClientesLista"] != null)
                    {
                        Application.OpenForms["frmClientesLista"].Activate();
                    }
                    else
                    {
                        frmClientesLista form = new frmClientesLista();
                        form.Proceso = 3;
                        form.ShowDialog();
                        txtCodCliente.Text = "";
                        txtDireccion.Text = "";
                        txtNombreCliente.Text = "";
                        if (form.exit)
                        {
                            cli = form.cli;
                            CodCliente = cli.CodCliente;
                            if (CodCliente != 0)
                            {
                                NombreCliente = cli.Nombre;
                                CargaCliente();
                                valor = true;
                                ProcessTabKey(true);
                            }
                        }
                        else
                        {
                            txtCodCliente.Focus();
                            valor = false;
                        }
                    }
                }
                else
                {
                    valor = true;
                }
            }
            else
            {
                valor = true;
            }
            return valor;
        }

        #region super valideitor
        private void customValidator1_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
        {
            if (Proceso != 0)
                if (e.ControlToValidate.Text != "")
                    e.IsValid = true;
                else
                    e.IsValid = false;
            else
                e.IsValid = true;
        }

        private void customValidator2_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
        {
            if (Proceso != 0)
                if (e.ControlToValidate.Text != "")
                    e.IsValid = true;
                else
                    e.IsValid = false;
            else
                e.IsValid = true;
        }

        private void customValidator3_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
        {
            if (Proceso != 0)
                if (e.ControlToValidate.Text != "")
                    e.IsValid = true;
                else
                    e.IsValid = false;
            else
                e.IsValid = true;
        }

        private void customValidator4_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
        {
            if (Proceso != 0)
                if (e.ControlToValidate.Text != "")
                    e.IsValid = true;
                else
                    e.IsValid = false;
            else
                e.IsValid = true;
        }

        private void customValidator5_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
        {
            if (Proceso != 0)
                if (e.ControlToValidate.Text != "")
                    e.IsValid = true;
                else
                    e.IsValid = false;
            else
                e.IsValid = true;
        }

        private void customValidator6_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
        {
            if (Proceso != 0)
                if (e.ControlToValidate.Text != "")
                    e.IsValid = true;
                else
                    e.IsValid = false;
            else
                e.IsValid = true;
        }

        private void customValidator7_ValidateValue(object sender, DevComponents.DotNetBar.Validator.ValidateValueEventArgs e)
        {
            if (Proceso != 0)
                if (e.ControlToValidate.Text != "")
                    e.IsValid = true;
                else
                    e.IsValid = false;
            else
                e.IsValid = true;
        }

        #endregion super valideitor

        private void chkBoleta_Click(object sender, EventArgs e)
        {
            chkBoleta.Checked = true;
            chkFactura.Checked = false;
        }

        private void chkFactura_Click(object sender, EventArgs e)
        {
            chkBoleta.Checked = false;
            chkFactura.Checked = true;
        }

        private void frmOrdenVenta_Load(object sender, EventArgs e)
        {
            if (Proceso == 3)
            {
                CargaPedido();
                sololectura(true);
                label1.Visible = true;
            }
            if (Proceso == 2)
            {
                CargaPedido();
                sololectura2(true);
                label1.Visible = true;
            }
            CargarParametros();
        }

        private void CargarParametros()
        {
            parametroMontoMáximoBoleta = admParametro.ObtenerParametroPorNombre("MONTO_MÁXIMO_BOLETA_CLIENTES_VARIOS");
            parametroCodigoClienteGeneral = admParametro.ObtenerParametroPorNombre("ID_CLIENTES_VARIOS");
        }

        private void sololectura(Boolean estado)
        {
            dtpFecha.Enabled = !estado;

            txtCodCliente.ReadOnly = estado;
            txtDocRef.ReadOnly = estado;
            txtPedido.ReadOnly = estado;
            txtBruto.ReadOnly = estado;
            txtDscto.ReadOnly = estado;
            txtValorVenta.ReadOnly = estado;
            txtIGV.ReadOnly = estado;
            txtPrecioVenta.ReadOnly = estado;
            btnInicioOV.Enabled = estado; //!estado
            btnAddItem.Enabled = !estado;
            btnDeleteItem.Enabled = !estado;
            btnGuardaOV.Enabled = !estado;
            btnEditaOV.Enabled = estado;
            btnAnulaOV.Enabled = estado;
            lbDocumento.Visible = estado;
            btnSalir.Visible = estado;
            btnSalir.Enabled = estado;
            //btnImprimirTicket.Visible = estado;
            //groupBox4.Enabled = true;

        }
        private void sololectura2(Boolean estado)//Creado para modificar OV
        {
            dtpFecha.Enabled = !estado;

            txtCodCliente.ReadOnly = estado;
            txtDocRef.ReadOnly = estado;
            txtPedido.ReadOnly = estado;
            txtBruto.ReadOnly = estado;
            txtDscto.ReadOnly = estado;
            txtValorVenta.ReadOnly = estado;
            txtIGV.ReadOnly = estado;
            txtPrecioVenta.ReadOnly = estado;
            btnInicioOV.Enabled = false; //estado;
            btnAddItem.Enabled = !estado;
            btnDeleteItem.Enabled = !estado;
            btnGuardaOV.Enabled = !estado;
            btnEditaOV.Enabled = estado;
            btnAnulaOV.Enabled = estado;
            lbDocumento.Visible = estado;
            btnSalir.Visible = estado;
            btnSalir.Enabled = estado;
            btnImprimirTicket.Visible = estado;
            btnCancelarOV.Visible = !estado;
            btnCancelarOV.Enabled = !estado;
            //groupBox4.Enabled = true;

        }

        private void txtNombreCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if ((int)e.KeyChar == (int)Keys.Enter)
                {
                    if (Application.OpenForms["frmClientesLista"] != null)
                    {
                        Application.OpenForms["frmClientesLista"].Activate();
                    }
                    else
                    {
                        frmClientesLista form = new frmClientesLista();
                        form.Proceso = 7;
                        form.filtrocliente = txtNombreCliente.Text;
                        if (form.ShowDialog() == DialogResult.OK)
                        {
                            CodCliente = form.GetCodigoCliente();
                            CargaCliente();

                            if (cli != null)
                            {
                                CodCliente = cli.CodCliente;
                                txtNombreCliente.Text = cli.RazonSocial;
                                txtDireccion.Text = cli.DireccionLegal;
                                txtCodCliente.Text = cli.RucDni;
                                cli = AdmCli.ConsultaCliente(cli.RucDni);
                                if (cli.Moneda == 1)
                                {
                                    txtLineaCredito.Text = String.Format("{0:#,##0.00}", cli.LineaCredito);
                                    txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoDisponible);
                                    txtLineaCreditoUso.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoUsado);
                                    txttasa.Text = String.Format("{0:#,##0.00}", cli.Tasa);
                                    lbLineaCredito.Text = "Línea de Crédito (S/.):";
                                    label23.Text = "Línea Disponible (S/.):";
                                    label25.Text = "Línea C. en Uso (S/.):";
                                    if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                                }
                                else
                                {
                                    txtLineaCredito.Text = String.Format("{0:#,##0.00}", (cli.LineaCredito / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                    txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoDisponible / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                    txtLineaCreditoUso.Text = Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoUsado / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                    lbLineaCredito.Text = "Línea de Crédito ($.):";
                                    label23.Text = "Línea Disponible ($.):";
                                    label25.Text = "Línea C. en Uso ($.):";
                                    if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                                }
                                if (cli.FormaPago != 0)
                                {
                                    cmbFormaPago.SelectedValue = 6; //cli.FormaPago  --   6 Contado
                                    EventArgs ee = new EventArgs();
                                    cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
                                }
                                else
                                {
                                    dtpFechaPago.Value = DateTime.Today;
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void txtPDescuento_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCodigoVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtCodigoVendedor.Text.Trim() != "")
                    {
                        Int32 codigoUsuario = Convert.ToInt32(txtCodigoVendedor.Text);
                        /*
						 * consultar usuario sin considerar usuario
						 * con nombre admin
						 */
                        vendedor = admUsuario.MuestraUsuarioSinAdmin(codigoUsuario);
                        if (vendedor != null)
                        {
                            txtNombreVendedor.Text = vendedor.Nombre + " " + vendedor.Apellido;
                        }
                        else
                        {
                            txtCodigoVendedor.Text = "";
                            txtNombreVendedor.Text = "";
                            MessageBox.Show("No se encontró ningún vendedor con el código ingresado",
                                            "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error: " + ex.Message,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCodigoVendedor_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    frmVendedoresLista frmDialog = new frmVendedoresLista();
                    frmDialog.ShowDialog();
                    if (vendedor != null)
                    {
                        txtCodigoVendedor.Text = vendedor.CodUsuario.ToString();
                        txtNombreVendedor.Text = vendedor.Nombre + " " + vendedor.Apellido;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        /*
		 * se obtiene la cantidad original
		 */
        private void dgvDetalle_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvDetalle.Rows.Count >= 1)
            {
                cantidadOriginalProducto = Convert.ToDecimal(dgvDetalle.CurrentRow.Cells[6].Value);
            }
        }

        private void btnInicioOV_Click(object sender, EventArgs e)
        {
            this.activaPaneles(true);
            CargaNumeracionOV();
            //txtSerie.Focus();
            //txtSerie.SelectAll();   
        }

        private void btnImprimirTicket_Click(object sender, EventArgs e)
        {
            try
            {
                CRCodigodeBarras rpt = new CRCodigodeBarras();
                frmCodigobarra frm = new frmCodigobarra();
                rpt.SetDataSource(ds.CodigoBarras(Convert.ToInt32(CodPedido), frmLogin.iCodAlmacen));
                frm.crystalReportViewer1.ReportSource = rpt;
                frm.Show();
            }
            catch (Exception a) { MessageBox.Show(a.Message); }
        }

        private void btnEditaOV_Click(object sender, EventArgs e)
        {
            sololectura2(false);
            /*
			 * Bandera a true para dejar agregar detalles al editar 
			 * una orden de venta
			 */
            bandera = true;
            calculatotales();
        }

        private void txtCodCliente_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    if (Application.OpenForms["frmClientesLista"] != null)
                    {
                        Application.OpenForms["frmClientesLista"].Activate();
                    }
                    else
                    {
                        frmClientesLista form = new frmClientesLista();
                        form.Proceso = 3;
                        form.ShowDialog();
                        cli = form.cli;
                        CodCliente = cli.CodCliente;
                        if (CodCliente != 0)
                        {
                            txtCodCliente.Text = "";
                            txtDireccion.Text = "";
                            txtNombreCliente.Text = "";
                            NombreCliente = cli.Nombre; CargaCliente();

                            if (cli.Moneda == 1)
                            {
                                txtLineaCredito.Text = String.Format("{0:#,##0.00}", cli.LineaCredito);
                                txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoDisponible);
                                txtLineaCreditoUso.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoUsado);
                                txttasa.Text = String.Format("{0:#,##0.00}", cli.Tasa);
                                lbLineaCredito.Text = "Línea de Crédito (S/.):";
                                label23.Text = "Línea Disponible (S/.):";
                                label25.Text = "Línea C. en Uso (S/.):";
                                if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                            }
                            else
                            {
                                txtLineaCredito.Text = String.Format("{0:#,##0.00}", (cli.LineaCredito / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoDisponible / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                txtLineaCreditoUso.Text = Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoUsado / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                lbLineaCredito.Text = "Línea de Crédito ($.):";
                                label23.Text = "Línea Disponible ($.):";
                                label25.Text = "Línea C. en Uso ($.):";
                                if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                            }
                            if (cli.FormaPago != 0)
                            {
                                cmbFormaPago.SelectedValue = 6; //cli.FormaPago  --   6 Contado
                                EventArgs ee = new EventArgs();
                                cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
                            }
                            else
                            {
                                dtpFechaPago.Value = DateTime.Today;
                            }
                        }
                    }
                }
            }
            catch (Exception a) { MessageBox.Show(a.Message); }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        { }

        private void chkVentaGratuita_Click(object sender, EventArgs e)
        { }

        private void chkVentaDsctoGlobal_Click(object sender, EventArgs e)
        { }

        private void chkVentaGratuita_CheckedChanged(object sender, EventArgs e)
        {
            if (chkVentaDsctoGlobal.Checked) chkVentaDsctoGlobal.Checked = false;
        }

        private void chkVentaDsctoGlobal_CheckedChanged(object sender, EventArgs e)
        {
            if (chkVentaGratuita.Checked) chkVentaGratuita.Checked = false;
        }

        Decimal cantidadanterior, cantidadnueva = 0;
        private void dgvDetalle_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dgvDetalle.RowCount > 0)
            {
                //foreach (DataGridViewRow row in dgvDetalle.Rows)
                //{
                //    row.Cells[0].ReadOnly = true;
                //}                
                dgvDetalle.CurrentRow.Cells[cantidad.Name].ReadOnly = false;
                dgvDetalle.CurrentRow.Cells[cantidad.Name].Style.BackColor = Color.PeachPuff;
                dgvDetalle.CurrentRow.Cells[cantidad.Name].Selected = true;
                cantidadanterior = Convert.ToDecimal(dgvDetalle.CurrentRow.Cells[cantidad.Name].Value);
            }
        }

        private void dgvDetalle_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            if (dgvDetalle.RowCount > 0)
            {
                if (e.ColumnIndex == 6)
                {
                    MessageBox.Show("cantidad original" + cantidadOriginalProducto, "asd");
                    cantidadnueva = Convert.ToDecimal(dgvDetalle.CurrentRow.Cells[cantidad.Name].Value);

                    /*
					 * busca el producto de acuerdo a la fila seleccionada
					 */
                    clsProducto productoBuscado = admProducto.CargaProducto(Convert.ToInt32(dgvDetalle.CurrentRow.Cells[codproducto.Name].Value),
                                                                            frmLogin.iCodAlmacen);

                    /*
					 * validacion de stock disponible de acuerdo a la fila donde modifica la cantidad
					 */
                    if (Convert.ToDouble(cantidadnueva) <= productoBuscado.StockActual)
                    {
                        if (cantidadnueva > 0 && cantidadanterior > 0 && cantidadnueva != cantidadanterior)
                        {
                            dgvDetalle.CurrentRow.Cells[precioventa.Name].Value = String.Format("{0:#,##0.00}", (cantidadnueva * Convert.ToDecimal(dgvDetalle.CurrentRow.Cells[preciounit.Name].Value)));
                            dgvDetalle.CurrentRow.Cells[valorventa.Name].Value = String.Format("{0:#,##0.00}", (Convert.ToDouble(dgvDetalle.CurrentRow.Cells[precioventa.Name].Value) *
                                                                            (1 - (frmLogin.Configuracion.IGV / 100))));
                            dgvDetalle.CurrentRow.Cells[igv.Name].Value = String.Format("{0:#,##0.00}", (Convert.ToDouble(dgvDetalle.CurrentRow.Cells[precioventa.Name].Value) -
                                                                            Convert.ToDouble(dgvDetalle.CurrentRow.Cells[valorventa.Name].Value)));
                            dgvDetalle.CurrentRow.Cells[importe.Name].Value = String.Format("{0:#,##0.00}", (Convert.ToDouble(dgvDetalle.CurrentRow.Cells[precioventa.Name].Value)));
                            Recalcular();
                        }
                        else
                        {
                            Int32 codigoDetallePedido = Convert.ToInt32(dgvDetalle.CurrentRow.Cells[coddetalle.Name].Value);
                            decimal cantidadOriginal = 0;
                            foreach (clsDetallePedido detalle_ in pedido.Detalle)
                            {
                                if (detalle_.CodDetallePedido == codigoDetallePedido)
                                {
                                    cantidadOriginal = Convert.ToDecimal(detalle_.Cantidad);
                                    break;
                                }
                            }

                            dgvDetalle.CurrentRow.Cells[cantidad.Name].Value = String.Format("{0:#,##0.00}", (Convert.ToDouble(cantidadOriginal)));
                            dgvDetalle.CurrentRow.Cells[precioventa.Name].Value = String.Format("{0:#,##0.00}", (cantidadOriginal * Convert.ToDecimal(dgvDetalle.CurrentRow.Cells[preciounit.Name].Value)));
                            dgvDetalle.CurrentRow.Cells[valorventa.Name].Value = String.Format("{0:#,##0.00}", (Convert.ToDouble(dgvDetalle.CurrentRow.Cells[precioventa.Name].Value) *
                                                                            (1 - (frmLogin.Configuracion.IGV / 100))));
                            dgvDetalle.CurrentRow.Cells[igv.Name].Value = String.Format("{0:#,##0.00}", (Convert.ToDouble(dgvDetalle.CurrentRow.Cells[precioventa.Name].Value) -
                                                                            Convert.ToDouble(dgvDetalle.CurrentRow.Cells[valorventa.Name].Value)));
                            dgvDetalle.CurrentRow.Cells[importe.Name].Value = String.Format("{0:#,##0.00}", (Convert.ToDouble(dgvDetalle.CurrentRow.Cells[precioventa.Name].Value)));
                            Recalcular();

                            MessageBox.Show("Cantidad Inválida!! Se tomará la cantidad anterior para el producto editado!",
                                            "Orden de venta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {

                        MessageBox.Show("Cantidad Inválida!! SOBREPASA EL STOCK ACTUAL DEL PRODUCTO!," +
                                        " SE TOMARÁ LA CANTIDAD ANTERIOR PARA EL PRODUCTO EDITADO",
                                        "Orden de venta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        dgvDetalle.CurrentRow.Cells[cantidad.Name].Value = String.Format("{0:#,##0.00}",
                                                                                (Convert.ToDouble(cantidadOriginalProducto)));

                    }

                }
            }
        }
    }
}
