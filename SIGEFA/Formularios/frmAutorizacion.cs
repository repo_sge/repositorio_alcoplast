﻿using SIGEFA.Administradores;
using SIGEFA.Entidades;
using System;
using System.Windows.Forms;

namespace SIGEFA.Formularios
{
	public partial class frmAutorizacion : DevComponents.DotNetBar.Office2007Form
	{

		clsAdmUsuario AdmUser = new clsAdmUsuario();

		/**
         * campo agregado para trabajar de manera global con
         * esta variable
         */
		public Boolean autorizacion = false;

		public frmAutorizacion()
		{
			InitializeComponent();
		}

		private void frmAutorizacion_Load(object sender, EventArgs e)
		{

		}

		private void btnAceptar_Click(object sender, EventArgs e)
		{
			try
			{
				if (txtUsuario.Text.Length > 0 && txtPassword.Text.Length > 0)
				{
					var usuarioSeguridad = new clsUsuario
					{
						Usuario = txtUsuario.Text,
						Contraseña = txtPassword.Text
					};

					if (AdmUser.LoginSeguridad(usuarioSeguridad))
					{
						autorizacion = true;
						MessageBox.Show("Acceso a la operación concedido", "Autorización",
										MessageBoxButtons.OK, MessageBoxIcon.Information);
						this.Close();
					}
					else
					{
						MessageBox.Show("Usuario y contraseña errados", "Error",
										MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Ocurrió un error al realizar la operación: " + ex.Message, 
								"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void txtUsuario_KeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Enter)
			{
				btnAceptar.PerformClick();
			}
		}

		private void txtPassword_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				btnAceptar.PerformClick();
			}
		}
	}
}
