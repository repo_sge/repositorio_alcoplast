﻿using DevComponents.DotNetBar.Validator;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SIGEFA.Formularios
{
	public partial class frmModificarPrecioVentaProducto : DevComponents.DotNetBar.Office2007Form
	{

		private Int32 codigoProducto;
		private Boolean tipoModificacionPrecio;//true: precio aumento - false: precio disminuyo
		private Int32 unidadIngresada;
		private Int32 moneda;
		private Double tipoCambio;
		clsValidar ok = new clsValidar();
		private clsProducto producto;
		private clsUnidadMedida unidad;
		clsAdmProducto AdmPro = new clsAdmProducto();
		clsAdmUnidad AdmUni = new clsAdmUnidad();

		public frmModificarPrecioVentaProducto(Int32 codigoProducto, Boolean tipoModificacionPrecio, Int32 unidadIngresada, Int32 moneda, Double tipoCambio)
		{
			this.tipoModificacionPrecio = tipoModificacionPrecio;
			this.codigoProducto = codigoProducto;
			this.unidadIngresada = unidadIngresada;
			this.moneda = moneda;
			this.tipoCambio = tipoCambio;
			InitializeComponent();
		}

		private void txtPrecioVentaNuevo_KeyPress(object sender, KeyPressEventArgs e)
		{
			ok.SOLONumeros(sender, e);
		}

		private void frmModificarPrecioVentaProducto_Load(object sender, EventArgs e)
		{
			/*
			 * consulta para obtener informacion del producto por su codigo
			 * y de la unidad seleccionada
			 */
			producto = AdmPro.CargaProductoDetalle(codigoProducto, frmLogin.iCodAlmacen, 1, 0);
			unidad = AdmUni.CargaUnidad(unidadIngresada);
			txtDescripcion.Text = producto.Descripcion;
			txtPrecioVentaActual.Text = AdmPro.UltimoPrecioVentaProducto(codigoProducto, unidadIngresada).ToString();
			txtUnidad.Text = unidad.Descripcion;

			txtUltPrecioCompra.Text = AdmPro.UltimoPrecioCompraProducto(codigoProducto,unidadIngresada, 0).ToString();

			if (tipoModificacionPrecio)
			{
				lblMensaje.Text = "Se recomienda AUMENTAR el precio de venta del producto.";
			}
			else
			{
				lblMensaje.Text = "Se recomienda DISMINUIR el precio de venta del producto.";
			}

			this.ActiveControl = txtPrecioVentaSoles;		
		}

		private void btnGuardar_Click(object sender, EventArgs e)
		{
			try
			{
				/*
				 * Nuevo precio de venta en soles
				 */
				Decimal nuevoPrecioVenta = Convert.ToDecimal(txtPrecioVentaSoles.Text.Trim());
				if(nuevoPrecioVenta < 0)
				{
					MessageBox.Show("Nuevo Precio de Venta debe ser mayor a 0","Validación",
									MessageBoxButtons.OK,MessageBoxIcon.Error);
					return;
				}

				/* 
				 * llamar a sp para actualizar precio de venta de producto
				 * en tabla unidad equivalente
				 */
				if (AdmPro.ActualizarPrecioVentaProductoPorUnidad(codigoProducto, unidadIngresada,
															  frmLogin.iCodAlmacen, nuevoPrecioVenta))
				{
					MessageBox.Show("Precio de venta actualizado correctamente", "Producto", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Close();
				}
				else
				{
					MessageBox.Show("Ocurrió un problema al realizar la operación", "Producto", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}

			}
			catch (Exception ex)
			{
				MessageBox.Show("Error: " + ex.Message.ToString());
			}
		}
		
		private void btnCancelar_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void txtPrecioVentaSoles_Validating(object sender, CancelEventArgs e)
		{
			if (String.IsNullOrEmpty(txtPrecioVentaSoles.Text))
			{
				e.Cancel = true;
				errorProvider1.SetError(txtPrecioVentaSoles, "Este campo es requerido");
				highlighter1.SetHighlightColor(txtPrecioVentaSoles, eHighlightColor.Red);
			}
			else
			{
				errorProvider1.SetError(txtPrecioVentaSoles, "");
			}
		}
	}
}
