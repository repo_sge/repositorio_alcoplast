﻿using DevComponents.DotNetBar.Controls;
using DevComponents.DotNetBar;
namespace SIGEFA.Formularios
{
    partial class frmEnvioSunat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEnvioSunat));
            this.cb_estado = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dg_documentos = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Repoid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipodoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fechaemision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Correlativo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Monto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estadosunat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mensajesunat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombredoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_envio = new DevComponents.DotNetBar.ButtonX();
            this.btnSalir = new DevComponents.DotNetBar.ButtonX();
            this.lblTotalDocumentos = new DevComponents.DotNetBar.LabelX();
            this.totaldocs = new DevComponents.DotNetBar.LabelX();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.dtpHasta = new System.Windows.Forms.DateTimePicker();
            this.dtpDesde = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_documentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cb_estado
            // 
            this.cb_estado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_estado.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_estado.FormattingEnabled = true;
            this.cb_estado.Items.AddRange(new object[] {
            "NO ENVIADOS",
            "ENVIADOS"});
            this.cb_estado.Location = new System.Drawing.Point(1050, 57);
            this.cb_estado.Name = "cb_estado";
            this.cb_estado.Size = new System.Drawing.Size(121, 25);
            this.cb_estado.TabIndex = 3;
            this.cb_estado.Visible = false;
            this.cb_estado.SelectedIndexChanged += new System.EventHandler(this.cb_estado_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(965, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "ESTADO: ";
            this.label2.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BackColor = System.Drawing.Color.FloralWhite;
            this.groupBox2.Controls.Add(this.dg_documentos);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(9, 112);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1191, 309);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Boletas y Notas de Crédito / Débito asociadas";
            // 
            // dg_documentos
            // 
            this.dg_documentos.AllowUserToAddRows = false;
            this.dg_documentos.AllowUserToDeleteRows = false;
            this.dg_documentos.AllowUserToOrderColumns = true;
            this.dg_documentos.AllowUserToResizeRows = false;
            this.dg_documentos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dg_documentos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dg_documentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_documentos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Repoid,
            this.Tipodoc,
            this.Fechaemision,
            this.Serie,
            this.Correlativo,
            this.Monto,
            this.Estadosunat,
            this.Mensajesunat,
            this.Nombredoc});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_documentos.DefaultCellStyle = dataGridViewCellStyle9;
            this.dg_documentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_documentos.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.dg_documentos.Location = new System.Drawing.Point(3, 19);
            this.dg_documentos.MultiSelect = false;
            this.dg_documentos.Name = "dg_documentos";
            this.dg_documentos.ReadOnly = true;
            this.dg_documentos.RowHeadersVisible = false;
            this.dg_documentos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_documentos.Size = new System.Drawing.Size(1185, 287);
            this.dg_documentos.TabIndex = 0;
            // 
            // Repoid
            // 
            this.Repoid.DataPropertyName = "Repoid";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Repoid.DefaultCellStyle = dataGridViewCellStyle1;
            this.Repoid.HeaderText = "ID";
            this.Repoid.Name = "Repoid";
            this.Repoid.ReadOnly = true;
            this.Repoid.Visible = false;
            this.Repoid.Width = 40;
            // 
            // Tipodoc
            // 
            this.Tipodoc.DataPropertyName = "Tipodoc";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Tipodoc.DefaultCellStyle = dataGridViewCellStyle2;
            this.Tipodoc.HeaderText = "T. DOC";
            this.Tipodoc.Name = "Tipodoc";
            this.Tipodoc.ReadOnly = true;
            this.Tipodoc.Visible = false;
            this.Tipodoc.Width = 50;
            // 
            // Fechaemision
            // 
            this.Fechaemision.DataPropertyName = "Fechaemision";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Fechaemision.DefaultCellStyle = dataGridViewCellStyle3;
            this.Fechaemision.HeaderText = "F. EMISION";
            this.Fechaemision.Name = "Fechaemision";
            this.Fechaemision.ReadOnly = true;
            this.Fechaemision.Width = 150;
            // 
            // Serie
            // 
            this.Serie.DataPropertyName = "Serie";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Serie.DefaultCellStyle = dataGridViewCellStyle4;
            this.Serie.HeaderText = "SERIE";
            this.Serie.Name = "Serie";
            this.Serie.ReadOnly = true;
            // 
            // Correlativo
            // 
            this.Correlativo.DataPropertyName = "Correlativo";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Correlativo.DefaultCellStyle = dataGridViewCellStyle5;
            this.Correlativo.HeaderText = "CORRELATIVO";
            this.Correlativo.Name = "Correlativo";
            this.Correlativo.ReadOnly = true;
            this.Correlativo.Width = 130;
            // 
            // Monto
            // 
            this.Monto.DataPropertyName = "Monto";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Monto.DefaultCellStyle = dataGridViewCellStyle6;
            this.Monto.HeaderText = "MONTO";
            this.Monto.Name = "Monto";
            this.Monto.ReadOnly = true;
            this.Monto.Width = 70;
            // 
            // Estadosunat
            // 
            this.Estadosunat.DataPropertyName = "Estadosunat";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Estadosunat.DefaultCellStyle = dataGridViewCellStyle7;
            this.Estadosunat.HeaderText = "EST. SUNAT";
            this.Estadosunat.Name = "Estadosunat";
            this.Estadosunat.ReadOnly = true;
            // 
            // Mensajesunat
            // 
            this.Mensajesunat.DataPropertyName = "Mensajesunat";
            this.Mensajesunat.HeaderText = "MENSAJE SUNAT";
            this.Mensajesunat.Name = "Mensajesunat";
            this.Mensajesunat.ReadOnly = true;
            this.Mensajesunat.Width = 400;
            // 
            // Nombredoc
            // 
            this.Nombredoc.DataPropertyName = "Nombredoc";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Nombredoc.DefaultCellStyle = dataGridViewCellStyle8;
            this.Nombredoc.HeaderText = "NOMBRE DOCUMENTO";
            this.Nombredoc.Name = "Nombredoc";
            this.Nombredoc.ReadOnly = true;
            this.Nombredoc.Width = 230;
            // 
            // btn_envio
            // 
            this.btn_envio.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btn_envio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_envio.Enabled = false;
            this.btn_envio.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_envio.Image = ((System.Drawing.Image)(resources.GetObject("btn_envio.Image")));
            this.btn_envio.Location = new System.Drawing.Point(1091, 427);
            this.btn_envio.Name = "btn_envio";
            this.btn_envio.Size = new System.Drawing.Size(106, 37);
            this.btn_envio.TabIndex = 2;
            this.btn_envio.Text = "ENVIAR";
            this.btn_envio.Click += new System.EventHandler(this.btn_envio_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSalir.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground;
            this.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(12, 427);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(100, 37);
            this.btnSalir.TabIndex = 3;
            this.btnSalir.Text = "SALIR";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblTotalDocumentos
            // 
            this.lblTotalDocumentos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.lblTotalDocumentos.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTotalDocumentos.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDocumentos.Location = new System.Drawing.Point(411, 441);
            this.lblTotalDocumentos.Name = "lblTotalDocumentos";
            this.lblTotalDocumentos.Size = new System.Drawing.Size(132, 23);
            this.lblTotalDocumentos.TabIndex = 4;
            this.lblTotalDocumentos.Text = "Total Documentos: ";
            this.lblTotalDocumentos.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // totaldocs
            // 
            this.totaldocs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.totaldocs.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.totaldocs.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totaldocs.Location = new System.Drawing.Point(540, 441);
            this.totaldocs.Name = "totaldocs";
            this.totaldocs.Size = new System.Drawing.Size(63, 23);
            this.totaldocs.TabIndex = 5;
            this.totaldocs.Text = ".";
            this.totaldocs.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.btnBuscar);
            this.groupControl1.Controls.Add(this.dtpHasta);
            this.groupControl1.Controls.Add(this.dtpDesde);
            this.groupControl1.Controls.Add(this.cb_estado);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Location = new System.Drawing.Point(9, 6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1188, 100);
            this.groupControl1.TabIndex = 6;
            this.groupControl1.Text = "BÚSQUEDA DE COMPROBANTES POR ENVIAR A SUNAT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(465, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(385, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "** LA CARGA DE DOCUMENTOS SE REALIZA EN GRUPOS DE 50";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(172, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "HASTA :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "DESDE :";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.Location = new System.Drawing.Point(325, 49);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(133, 34);
            this.btnBuscar.TabIndex = 6;
            this.btnBuscar.Text = "BUSCAR";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // dtpHasta
            // 
            this.dtpHasta.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHasta.Location = new System.Drawing.Point(175, 58);
            this.dtpHasta.Name = "dtpHasta";
            this.dtpHasta.Size = new System.Drawing.Size(144, 25);
            this.dtpHasta.TabIndex = 5;
            // 
            // dtpDesde
            // 
            this.dtpDesde.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDesde.Location = new System.Drawing.Point(18, 58);
            this.dtpDesde.Name = "dtpDesde";
            this.dtpDesde.Size = new System.Drawing.Size(151, 25);
            this.dtpDesde.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(465, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(567, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "** PLAZO MÁXIMO DE ENVÍO: 7 DÍAS A PARTIR DE LA FECHA DE EMISIÓN DEL DOCUMENTO";
            // 
            // frmEnvioSunat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.CancelButton = this.btnSalir;
            this.ClientSize = new System.Drawing.Size(1212, 476);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.totaldocs);
            this.Controls.Add(this.lblTotalDocumentos);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btn_envio);
            this.Controls.Add(this.groupBox2);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmEnvioSunat";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Envío de documentos";
            this.Load += new System.EventHandler(this.frmEnvioSunat_Load);
            this.Shown += new System.EventHandler(this.frmEnvioSunat_Shown);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_documentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private DataGridViewX dg_documentos;
        private ButtonX btn_envio;
        private ButtonX btnSalir;
        private LabelX lblTotalDocumentos;
        private System.Windows.Forms.ComboBox cb_estado;
        private System.Windows.Forms.Label label2;
        private LabelX totaldocs;
		private System.Windows.Forms.DataGridViewTextBoxColumn Repoid;
		private System.Windows.Forms.DataGridViewTextBoxColumn Tipodoc;
		private System.Windows.Forms.DataGridViewTextBoxColumn Fechaemision;
		private System.Windows.Forms.DataGridViewTextBoxColumn Serie;
		private System.Windows.Forms.DataGridViewTextBoxColumn Correlativo;
		private System.Windows.Forms.DataGridViewTextBoxColumn Monto;
		private System.Windows.Forms.DataGridViewTextBoxColumn Estadosunat;
		private System.Windows.Forms.DataGridViewTextBoxColumn Mensajesunat;
		private System.Windows.Forms.DataGridViewTextBoxColumn Nombredoc;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.DateTimePicker dtpHasta;
        private System.Windows.Forms.DateTimePicker dtpDesde;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}