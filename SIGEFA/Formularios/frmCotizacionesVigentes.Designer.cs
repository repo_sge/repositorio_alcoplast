﻿namespace SIGEFA.Formularios
{
    partial class frmCotizacionesVigentes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCotizacionesVigentes));
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.dgvCotizaciones = new System.Windows.Forms.DataGridView();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnAnular = new System.Windows.Forms.Button();
			this.btGenVenta = new System.Windows.Forms.Button();
			this.btnIrCotizacion = new System.Windows.Forms.Button();
			this.btnSalir = new System.Windows.Forms.Button();
			this.imageList2 = new System.Windows.Forms.ImageList(this.components);
			this.imageList3 = new System.Windows.Forms.ImageList(this.components);
			this.imageList4 = new System.Windows.Forms.ImageList(this.components);
			this.btnConsultar = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.dtpDesde = new System.Windows.Forms.DateTimePicker();
			this.dtpHasta = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.txtFiltro = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.cmbVigente = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this.btnEnviar = new System.Windows.Forms.Button();
			this.codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.documento = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.responsable = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fechavence = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.aprob = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvCotizaciones)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.dgvCotizaciones);
			this.groupBox1.Location = new System.Drawing.Point(8, 61);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(1458, 421);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "VIGENTES";
			// 
			// dgvCotizaciones
			// 
			this.dgvCotizaciones.AllowUserToAddRows = false;
			this.dgvCotizaciones.AllowUserToDeleteRows = false;
			this.dgvCotizaciones.AllowUserToOrderColumns = true;
			this.dgvCotizaciones.AllowUserToResizeRows = false;
			this.dgvCotizaciones.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
			dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle19.BackColor = System.Drawing.Color.CadetBlue;
			dataGridViewCellStyle19.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle19.ForeColor = System.Drawing.Color.White;
			dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvCotizaciones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
			this.dgvCotizaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvCotizaciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codigo,
            this.cliente,
            this.importe,
            this.fecha,
            this.documento,
            this.responsable,
            this.fechavence,
            this.aprob});
			dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle24.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvCotizaciones.DefaultCellStyle = dataGridViewCellStyle24;
			this.dgvCotizaciones.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvCotizaciones.EnableHeadersVisualStyles = false;
			this.dgvCotizaciones.Location = new System.Drawing.Point(3, 19);
			this.dgvCotizaciones.MultiSelect = false;
			this.dgvCotizaciones.Name = "dgvCotizaciones";
			this.dgvCotizaciones.ReadOnly = true;
			this.dgvCotizaciones.RowHeadersVisible = false;
			this.dgvCotizaciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvCotizaciones.Size = new System.Drawing.Size(1452, 399);
			this.dgvCotizaciones.TabIndex = 0;
			this.dgvCotizaciones.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCotizaciones_CellDoubleClick);
			this.dgvCotizaciones.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCotizaciones_ColumnHeaderMouseClick);
			this.dgvCotizaciones.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgvCotizaciones_RowStateChanged);
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "exit.png");
			this.imageList1.Images.SetKeyName(1, "pedido.png");
			this.imageList1.Images.SetKeyName(2, "carrito.png");
			this.imageList1.Images.SetKeyName(3, "delete-file-icon.png");
			this.imageList1.Images.SetKeyName(4, "DeleteRed.png");
			this.imageList1.Images.SetKeyName(5, "document_delete.png");
			// 
			// btnAnular
			// 
			this.btnAnular.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAnular.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAnular.Image = ((System.Drawing.Image)(resources.GetObject("btnAnular.Image")));
			this.btnAnular.Location = new System.Drawing.Point(992, 12);
			this.btnAnular.Name = "btnAnular";
			this.btnAnular.Size = new System.Drawing.Size(117, 43);
			this.btnAnular.TabIndex = 4;
			this.btnAnular.Text = "ANULAR COTIZACIÓN";
			this.btnAnular.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnAnular.UseVisualStyleBackColor = true;
			this.btnAnular.Click += new System.EventHandler(this.btnAnular_Click);
			// 
			// btGenVenta
			// 
			this.btGenVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btGenVenta.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btGenVenta.Image = ((System.Drawing.Image)(resources.GetObject("btGenVenta.Image")));
			this.btGenVenta.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btGenVenta.Location = new System.Drawing.Point(1115, 12);
			this.btGenVenta.Name = "btGenVenta";
			this.btGenVenta.Size = new System.Drawing.Size(104, 43);
			this.btGenVenta.TabIndex = 3;
			this.btGenVenta.Text = "GENERAR VENTA";
			this.btGenVenta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btGenVenta.UseVisualStyleBackColor = true;
			this.btGenVenta.Click += new System.EventHandler(this.btGenVenta_Click);
			// 
			// btnIrCotizacion
			// 
			this.btnIrCotizacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnIrCotizacion.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnIrCotizacion.Image = ((System.Drawing.Image)(resources.GetObject("btnIrCotizacion.Image")));
			this.btnIrCotizacion.Location = new System.Drawing.Point(1225, 12);
			this.btnIrCotizacion.Name = "btnIrCotizacion";
			this.btnIrCotizacion.Size = new System.Drawing.Size(119, 43);
			this.btnIrCotizacion.TabIndex = 2;
			this.btnIrCotizacion.Text = "VER COTIZACIÓN";
			this.btnIrCotizacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnIrCotizacion.UseVisualStyleBackColor = true;
			this.btnIrCotizacion.Click += new System.EventHandler(this.btnIrCotizacion_Click);
			// 
			// btnSalir
			// 
			this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSalir.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
			this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnSalir.Location = new System.Drawing.Point(1350, 12);
			this.btnSalir.Name = "btnSalir";
			this.btnSalir.Size = new System.Drawing.Size(110, 43);
			this.btnSalir.TabIndex = 1;
			this.btnSalir.Text = "CANCELAR";
			this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnSalir.UseVisualStyleBackColor = true;
			this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
			// 
			// imageList2
			// 
			this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
			this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList2.Images.SetKeyName(0, "Add Green Button.png");
			this.imageList2.Images.SetKeyName(1, "Add.png");
			this.imageList2.Images.SetKeyName(2, "Remove.png");
			this.imageList2.Images.SetKeyName(3, "Write Document.png");
			this.imageList2.Images.SetKeyName(4, "New Document.png");
			this.imageList2.Images.SetKeyName(5, "Remove Document.png");
			this.imageList2.Images.SetKeyName(6, "1328102023_Copy.png");
			this.imageList2.Images.SetKeyName(7, "document-print.png");
			this.imageList2.Images.SetKeyName(8, "g-icon-new-update.png");
			this.imageList2.Images.SetKeyName(9, "refresh_256.png");
			this.imageList2.Images.SetKeyName(10, "Refresh-icon.png");
			this.imageList2.Images.SetKeyName(11, "search (1).png");
			this.imageList2.Images.SetKeyName(12, "search (5).png");
			this.imageList2.Images.SetKeyName(13, "search (6).png");
			this.imageList2.Images.SetKeyName(14, "search (8).png");
			this.imageList2.Images.SetKeyName(15, "search_top.png");
			this.imageList2.Images.SetKeyName(16, "icon-47203_640.png");
			this.imageList2.Images.SetKeyName(17, "Folder open.png");
			// 
			// imageList3
			// 
			this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
			this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList3.Images.SetKeyName(0, "Write Document.png");
			this.imageList3.Images.SetKeyName(1, "New Document.png");
			this.imageList3.Images.SetKeyName(2, "Remove Document.png");
			this.imageList3.Images.SetKeyName(3, "document-print.png");
			this.imageList3.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
			this.imageList3.Images.SetKeyName(5, "exit.png");
			this.imageList3.Images.SetKeyName(6, "OK_Verde.png");
			// 
			// imageList4
			// 
			this.imageList4.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList4.ImageStream")));
			this.imageList4.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList4.Images.SetKeyName(0, "Write Document.png");
			this.imageList4.Images.SetKeyName(1, "New Document.png");
			this.imageList4.Images.SetKeyName(2, "Remove Document.png");
			this.imageList4.Images.SetKeyName(3, "document-print.png");
			this.imageList4.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
			this.imageList4.Images.SetKeyName(5, "exit.png");
			this.imageList4.Images.SetKeyName(6, "search (1).png");
			this.imageList4.Images.SetKeyName(7, "Glossy-Open-icon.png");
			this.imageList4.Images.SetKeyName(8, "folder-open-icon (1).png");
			this.imageList4.Images.SetKeyName(9, "document_delete.png");
			this.imageList4.Images.SetKeyName(10, "DeleteRed.png");
			this.imageList4.Images.SetKeyName(11, "OK_Verde.png");
			// 
			// btnConsultar
			// 
			this.btnConsultar.BackColor = System.Drawing.SystemColors.ControlLight;
			this.btnConsultar.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnConsultar.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultar.Image")));
			this.btnConsultar.Location = new System.Drawing.Point(744, 12);
			this.btnConsultar.Name = "btnConsultar";
			this.btnConsultar.Size = new System.Drawing.Size(122, 43);
			this.btnConsultar.TabIndex = 29;
			this.btnConsultar.Text = "CONSULTAR";
			this.btnConsultar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnConsultar.UseVisualStyleBackColor = false;
			this.btnConsultar.Visible = false;
			this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(132, 12);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(51, 15);
			this.label6.TabIndex = 28;
			this.label6.Text = "HASTA :";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(10, 12);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(50, 15);
			this.label5.TabIndex = 27;
			this.label5.Text = "DESDE :";
			// 
			// dtpDesde
			// 
			this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpDesde.Location = new System.Drawing.Point(13, 30);
			this.dtpDesde.Name = "dtpDesde";
			this.dtpDesde.Size = new System.Drawing.Size(116, 23);
			this.dtpDesde.TabIndex = 26;
			this.dtpDesde.ValueChanged += new System.EventHandler(this.dtpDesde_ValueChanged);
			// 
			// dtpHasta
			// 
			this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpHasta.Location = new System.Drawing.Point(135, 30);
			this.dtpHasta.Name = "dtpHasta";
			this.dtpHasta.Size = new System.Drawing.Size(116, 23);
			this.dtpHasta.TabIndex = 25;
			this.dtpHasta.ValueChanged += new System.EventHandler(this.dtpHasta_ValueChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.label1.Location = new System.Drawing.Point(643, 10);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(12, 15);
			this.label1.TabIndex = 42;
			this.label1.Text = "x";
			this.label1.Visible = false;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label2.Location = new System.Drawing.Point(451, 10);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(36, 15);
			this.label2.TabIndex = 41;
			this.label2.Text = "POR :";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.BackColor = System.Drawing.Color.Transparent;
			this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label10.Location = new System.Drawing.Point(400, 10);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(45, 15);
			this.label10.TabIndex = 39;
			this.label10.Text = "FILTRO";
			// 
			// txtFiltro
			// 
			this.txtFiltro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtFiltro.Location = new System.Drawing.Point(403, 30);
			this.txtFiltro.Name = "txtFiltro";
			this.txtFiltro.Size = new System.Drawing.Size(241, 23);
			this.txtFiltro.TabIndex = 38;
			this.txtFiltro.TextChanged += new System.EventHandler(this.txtFiltro_TextChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(254, 12);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(58, 15);
			this.label3.TabIndex = 43;
			this.label3.Text = "ESTADO :";
			// 
			// cmbVigente
			// 
			this.cmbVigente.FormattingEnabled = true;
			this.cmbVigente.Items.AddRange(new object[] {
            "TODOS",
            "VIGENTE",
            "PARCIAL",
            "ATENDIDA",
            "VENCIDA"});
			this.cmbVigente.Location = new System.Drawing.Point(257, 30);
			this.cmbVigente.Name = "cmbVigente";
			this.cmbVigente.Size = new System.Drawing.Size(140, 23);
			this.cmbVigente.TabIndex = 44;
			this.cmbVigente.SelectionChangeCommitted += new System.EventHandler(this.cmbVigente_SelectionChangeCommitted);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(493, 11);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(14, 15);
			this.label7.TabIndex = 45;
			this.label7.Text = "X";
			// 
			// btnEnviar
			// 
			this.btnEnviar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnEnviar.Enabled = false;
			this.btnEnviar.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnEnviar.ImageIndex = 17;
			this.btnEnviar.ImageList = this.imageList2;
			this.btnEnviar.Location = new System.Drawing.Point(872, 12);
			this.btnEnviar.Name = "btnEnviar";
			this.btnEnviar.Size = new System.Drawing.Size(23, 43);
			this.btnEnviar.TabIndex = 46;
			this.btnEnviar.Text = "Enviar";
			this.btnEnviar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnEnviar.UseVisualStyleBackColor = true;
			this.btnEnviar.Visible = false;
			this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
			// 
			// codigo
			// 
			this.codigo.DataPropertyName = "codCotizacion";
			this.codigo.HeaderText = "Código";
			this.codigo.Name = "codigo";
			this.codigo.ReadOnly = true;
			this.codigo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.codigo.Width = 120;
			// 
			// cliente
			// 
			this.cliente.DataPropertyName = "cliente";
			this.cliente.HeaderText = "Cliente";
			this.cliente.Name = "cliente";
			this.cliente.ReadOnly = true;
			this.cliente.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.cliente.Width = 450;
			// 
			// importe
			// 
			this.importe.DataPropertyName = "total";
			dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			this.importe.DefaultCellStyle = dataGridViewCellStyle20;
			this.importe.HeaderText = "Importe";
			this.importe.Name = "importe";
			this.importe.ReadOnly = true;
			this.importe.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			// 
			// fecha
			// 
			this.fecha.DataPropertyName = "fecha";
			dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			this.fecha.DefaultCellStyle = dataGridViewCellStyle21;
			this.fecha.HeaderText = "Fecha";
			this.fecha.Name = "fecha";
			this.fecha.ReadOnly = true;
			this.fecha.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.fecha.Width = 90;
			// 
			// documento
			// 
			this.documento.DataPropertyName = "documento";
			dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			this.documento.DefaultCellStyle = dataGridViewCellStyle22;
			this.documento.HeaderText = "T. doc.";
			this.documento.Name = "documento";
			this.documento.ReadOnly = true;
			this.documento.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.documento.Visible = false;
			this.documento.Width = 60;
			// 
			// responsable
			// 
			this.responsable.DataPropertyName = "responsable";
			this.responsable.HeaderText = "Responsable";
			this.responsable.Name = "responsable";
			this.responsable.ReadOnly = true;
			this.responsable.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.responsable.Width = 350;
			// 
			// fechavence
			// 
			this.fechavence.DataPropertyName = "fechavigencia";
			dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle23.Format = "d";
			this.fechavence.DefaultCellStyle = dataGridViewCellStyle23;
			this.fechavence.HeaderText = "Vig. Hasta";
			this.fechavence.Name = "fechavence";
			this.fechavence.ReadOnly = true;
			this.fechavence.Width = 120;
			// 
			// aprob
			// 
			this.aprob.DataPropertyName = "aprobado";
			this.aprob.HeaderText = "Aprobado";
			this.aprob.Name = "aprob";
			this.aprob.ReadOnly = true;
			this.aprob.Visible = false;
			// 
			// frmCotizacionesVigentes
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1476, 496);
			this.Controls.Add(this.btnEnviar);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.cmbVigente);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.txtFiltro);
			this.Controls.Add(this.btnConsultar);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.dtpDesde);
			this.Controls.Add(this.dtpHasta);
			this.Controls.Add(this.btnAnular);
			this.Controls.Add(this.btGenVenta);
			this.Controls.Add(this.btnIrCotizacion);
			this.Controls.Add(this.btnSalir);
			this.Controls.Add(this.groupBox1);
			this.DoubleBuffered = true;
			this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmCotizacionesVigentes";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Historial de Cotizaciones";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.frmCotizacionesVigentes_Load);
			this.Shown += new System.EventHandler(this.frmCotizacionesVigentes_Shown);
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvCotizaciones)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvCotizaciones;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnIrCotizacion;
        private System.Windows.Forms.Button btGenVenta;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnAnular;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.ImageList imageList4;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpDesde;
        private System.Windows.Forms.DateTimePicker dtpHasta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbVigente;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnEnviar;
		private System.Windows.Forms.DataGridViewTextBoxColumn codigo;
		private System.Windows.Forms.DataGridViewTextBoxColumn cliente;
		private System.Windows.Forms.DataGridViewTextBoxColumn importe;
		private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
		private System.Windows.Forms.DataGridViewTextBoxColumn documento;
		private System.Windows.Forms.DataGridViewTextBoxColumn responsable;
		private System.Windows.Forms.DataGridViewTextBoxColumn fechavence;
		private System.Windows.Forms.DataGridViewTextBoxColumn aprob;
	}
}